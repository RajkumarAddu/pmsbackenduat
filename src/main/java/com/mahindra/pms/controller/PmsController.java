package com.mahindra.pms.controller;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mahindra.pms.model.EmployeeVehicleMaster;
import com.mahindra.pms.model.FuelType;
import com.mahindra.pms.model.GateMaster;
import com.mahindra.pms.model.NoOfWheelsMaster;
import com.mahindra.pms.model.Notify;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.SecurityGaurdMaster;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VehicleTypeMaster;
import com.mahindra.pms.model.VisitorVehicleDetails;
import com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO;
import com.mahindra.pms.model.dto.ChangeParkingSlotDTO;
import com.mahindra.pms.model.dto.CheckLocationResponseDTO;
import com.mahindra.pms.model.dto.EmployeeFetchDetailsDTO;
import com.mahindra.pms.model.dto.EmployeeVRegNoFetchDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleListDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleMasterDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleRegNoDTO;
import com.mahindra.pms.model.dto.NotifyDTO;
import com.mahindra.pms.model.dto.ParkingStationMasterResponseDTO;
import com.mahindra.pms.model.dto.Report1DTO;
import com.mahindra.pms.model.dto.Report2DTO;
import com.mahindra.pms.model.dto.Report2ResponseDTO;
import com.mahindra.pms.model.dto.Report3DTO;
import com.mahindra.pms.model.dto.ResponseBaseLocationAndBuildingMaster;
import com.mahindra.pms.model.dto.ResponseDTO;
import com.mahindra.pms.model.dto.ResponseEmployeeVRegNoFetchDTO;
import com.mahindra.pms.model.dto.ResponseOfEmpVRegistration;
import com.mahindra.pms.model.dto.SecurityGaurdMasterDTO;
import com.mahindra.pms.model.dto.SecurityGaurdTokenDTO;
import com.mahindra.pms.model.dto.VehicleEntryDTO;
import com.mahindra.pms.model.dto.VehicleExitDTO;
import com.mahindra.pms.model.dto.VisitorVehicleEntryDTO;
import com.mahindra.pms.model.dto.VisitorVehicleExitDTO;
import com.mahindra.pms.security.JwtUtil;
import com.mahindra.pms.service.BaseLocationService;
import com.mahindra.pms.service.EmployeeVehicleMasterService;
import com.mahindra.pms.service.GateMasterService;
import com.mahindra.pms.service.NoOfWheelsService;
import com.mahindra.pms.service.NotifyService;
import com.mahindra.pms.service.ParkingStationMasterService;
import com.mahindra.pms.service.ReportService;
import com.mahindra.pms.service.SecurityGaurdMasterService;
import com.mahindra.pms.service.VehicleInOutService;
import com.mahindra.pms.service.VisitorVehicleDetailsService;
import com.mahindra.pms.serviceImpl.EmployeeVehicleDetailsExcelExporter;
import com.mahindra.pms.serviceImpl.VehicleExcelExporter;
import com.mahindra.pms.validation.EmployeeVehicleMasterValidation;

@RestController
@RequestMapping("/pms/")
public class PmsController {

	Logger log = LoggerFactory.getLogger(PmsController.class);

	EmployeeVehicleMasterService employeeVehicleMasterService;

	public PmsController(EmployeeVehicleMasterService employeeVehicleMasterService) {
		this.employeeVehicleMasterService = employeeVehicleMasterService;
	}

	@Autowired
	ParkingStationMasterService parkingStationMasterService;

	@Autowired
	GateMasterService gateMasteService;

	@Autowired
	VehicleInOutService vehicleInOutService;

	@Autowired
	VisitorVehicleDetailsService visitorVehicleDetailsService;

	@Autowired
	BaseLocationService baseLocationService;

	@Autowired
	NotifyService notifyService;

	@Autowired
	NoOfWheelsService noOfWheelsService;

	@Autowired
	SecurityGaurdMasterService securityGaurdMasterService;

	@Autowired
	JwtUtil jwtUtil;

	@Autowired
	ReportService reportService;

	@Autowired
	EmployeeVehicleMasterValidation validationService;

	/*
	 * @Autowired(required = true) private AuthenticationManager manager;
	 */
	/**
	 * registerEmpVeDetails for Register Emp Vehicle Details
	 * 
	 * @param MultiPartFile       pucFile,insuranceFile,licenseFile,rcBook
	 * @param empVehicleMasterDTO object
	 * @return ResponseEntity<EmployeeVehicleMaster>
	 * @throws Exception
	 */
	@PostMapping(value = "register/EmpVehicleMaster")
	public ResponseDTO registerEmpVeDetails(@Valid @RequestBody EmployeeVehicleMasterDTO empVehicleMasterDTO)
			throws Exception {
		log.info("Processing request registerEmpVeDetails id: {}");
		ResponseDTO res = new ResponseDTO();
		validationService.checkValidateEmpVMasterDTO(empVehicleMasterDTO);
		ResponseOfEmpVRegistration saveEmpVM = employeeVehicleMasterService.saveEmpVM(empVehicleMasterDTO);
		res.setData(saveEmpVM);
		res.setMessage("Employee vehicle registered successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/**
	 * getEmpVeDetailsById Get Emp Vehicle Details By RegNo. and EmpId
	 * 
	 * @param EmployeeFetchDetailsDTO dto object
	 * @return ResponseEntity<EmployeeVehicleMaster>
	 */
	@PostMapping(value = "getEmpVehicleDetails")
	public ResponseDTO getEmpVeDetailsById(@Valid @RequestBody EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Processing request getEmpVeDetailsById EmpId and VehicleRegNo id: {}", dto);
		ResponseDTO res = new ResponseDTO();
		EmployeeVehicleMaster empVMById = employeeVehicleMasterService.getEmpVMById(dto);
		res.setData(empVMById);
		res.setMessage("Employee vehicle details fetched successfully. ");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/**
	 * updateEmpVeDetailsById Update Emp Vehicle Details
	 * 
	 * @param EmployeeVehicleMasterDTO employeeVehicleMasterDTO
	 * @param id
	 * @return ResponseEntity<EmployeeVehicleMaster>
	 */
	@PostMapping(value = "updateEmpVehicleDetails")
	public ResponseDTO updateEmpVeDetailsById(@RequestBody EmployeeVehicleMasterDTO employeeVehicleMasterDTO)
			throws Exception {
		log.info("Processing request updateEmpVeDetailsById  id: {}");
		ResponseDTO res = new ResponseDTO();
		EmployeeVehicleMaster empVMById = employeeVehicleMasterService.updateEmpVM(employeeVehicleMasterDTO);
		res.setData(empVMById);
		res.setMessage("Employee vehicle details updated successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/**
	 * deleteEmpVeDetailsById Delete Emp Vehicle Details
	 * 
	 * @param id
	 * @return
	 */
	@PostMapping(value = "deleteEmpVehicleDetails")
	public ResponseDTO deleteEmpVeDetailsById(@RequestBody EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Processing request deleteEmpVeDetailsById  id: {}", dto.getEmpId());
		ResponseDTO res = new ResponseDTO();
		employeeVehicleMasterService.deleteEmpVM(dto);
		res.setMessage("Employee vehicle details deleted successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/**
	 * getAllGates()
	 * 
	 * @return list of Gates
	 */
	@GetMapping(value = "getAllGates")
	public ResponseDTO getAllGates() throws Exception {
		log.info("Processing request getAllGates ");
		ResponseDTO res = new ResponseDTO();
		List<GateMaster> listOfGates = gateMasteService.getAllGates();
		res.setMessage("Gates fetched successfully.");
		res.setStatus(200);
		res.setData(listOfGates);
		res.setSuccess(true);
		return res;
	}

	/**
	 * getGatesByGateNo
	 * 
	 * @param gateNo
	 * @return gate Details
	 */
	@PostMapping(value = "getGatesByLocationId")
	public ResponseDTO getGatesByLocationId(@RequestBody BaseLocationFetchResponseDTO baseLocationIdDTO)
			throws Exception {
		log.info("Processing request getGatesByLocationId");
		List<GateMaster> list = gateMasteService.getGatesByLocationId(baseLocationIdDTO);
		ResponseDTO res = new ResponseDTO();
		res.setMessage("Gates by location fetched successfully.");
		res.setStatus(200);
		res.setData(list);
		res.setSuccess(true);
		return res;
	}

	/**
	 * @return list of Gates
	 */
	@PostMapping(value = "getPsAndSlots")
	public ResponseDTO fetchAvlParkingStationAndSlots(@RequestBody BaseLocationFetchResponseDTO dto) throws Exception {
		log.info("Processing request fetchAvlParkingStationAndSlots ");
		ResponseDTO res = new ResponseDTO();
		ParkingStationMasterResponseDTO parkingData = parkingStationMasterService.fetchParkingStationAndSlots(dto);
		res.setData(parkingData);
		res.setMessage("Parking stations fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/**
	 * @return list of Gates
	 */
	@PostMapping(value = "getAllPsAndSlots")
	public ResponseDTO fetchAllParkingStationAndSlots(@RequestBody BaseLocationFetchResponseDTO dto) throws Exception {
		log.info("Processing request fetchAllParkingStationAndSlots ");
		ResponseDTO res = new ResponseDTO();
		List<ParkingStationMaster> list = parkingStationMasterService.fetchAllParkingStationAndSlots(dto);
		res.setData(list);
		res.setMessage("Parking stations fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@PostMapping(value = "vehicleEntry")
	public ResponseDTO vehicleEntry(@Valid @RequestBody VehicleEntryDTO vehicleEntryDTO) throws Exception {
		log.info("Processing request vehicleEntry");
		ResponseDTO res = new ResponseDTO();
		VehicleInOut vehicleInEntry = vehicleInOutService.vehicleInEntry(vehicleEntryDTO);
		res.setData(vehicleInEntry);
		res.setStatus(200);
		res.setMessage(vehicleEntryDTO.getVehicleRegNo() + " Vehicle entered successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@PostMapping(value = "vehicleExit")
	public ResponseDTO vehicleExit(@Valid @RequestBody VehicleExitDTO vehicleExitDTO) throws Exception {
		log.info("Processing request vehicleExit");
		ResponseDTO res = new ResponseDTO();
		VehicleInOut vehicleExit = vehicleInOutService.vehicleExit(vehicleExitDTO);
		res.setData(vehicleExit);
		res.setStatus(200);
		res.setMessage(vehicleExitDTO.getVehicleRegNo() + " Vehicle exited successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	*/
	@PostMapping(value = "visitorEntry")
	public ResponseDTO visitorEntry(@Valid @RequestBody VisitorVehicleEntryDTO visitorVehicleDetailsDTO)
			throws Exception {
		log.info("Processing request visitor Entry :"+visitorVehicleDetailsDTO);
		validationService.checkValidateVisitorEntryDTO(visitorVehicleDetailsDTO);
		ResponseDTO res = new ResponseDTO();
		VisitorVehicleDetails saveVisitor = visitorVehicleDetailsService.visitorEntry(visitorVehicleDetailsDTO);
		res.setData(saveVisitor);
		res.setStatus(200);
		res.setMessage("Visitor entered successfully.");
		res.setSuccess(true);
		return res;

	}

	/*
	 * 
	 */
	@PostMapping(value = "visitorExit")
	public ResponseDTO visitorExit(@Valid @RequestBody VisitorVehicleExitDTO visitorVehicleExitDTO) throws Exception {
		log.info("Processing request visitor Exit");
		ResponseDTO res = new ResponseDTO();
		VisitorVehicleDetails visitorExit = visitorVehicleDetailsService.visitorExit(visitorVehicleExitDTO);
		res.setData(visitorExit);
		res.setStatus(200);
		res.setMessage("Visitor exited successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@PostMapping(value = "visitorList")
	public ResponseDTO visitorList(@RequestBody BaseLocationFetchResponseDTO dto) throws Exception {
		log.info("Processing request visitorList");
		ResponseDTO res = new ResponseDTO();
		List<VisitorVehicleDetails> visitorList = visitorVehicleDetailsService.visitorsListIsExitFalse(dto);
		res.setData(visitorList);
		res.setStatus(200);
		res.setMessage("Visitors fetched successfully.");
		res.setSuccess(true);
		return res;

	}

	/*
	 * 
	 */
	@PostMapping(value = "getVisitorByVRegNo")
	public ResponseDTO getVisitorByVRegNo(@RequestBody EmployeeVehicleRegNoDTO dto) throws Exception {
		log.info("Processing request visitorList");
		ResponseDTO res = new ResponseDTO();
		VisitorVehicleDetails visitorData = visitorVehicleDetailsService.getVisitorbyVRegNo(dto);
		res.setData(visitorData);
		res.setStatus(200);
		res.setMessage("Visitor fetched successfully.");
		res.setSuccess(true);
		return res;

	}

	/*
	 * 
	 */
	@PostMapping("notify")
	public ResponseDTO notifyIssue(@RequestBody NotifyDTO dto) throws Exception {
		log.info("request Processing of notify Issue");
		Notify ntfData = notifyService.observedIssue(dto);
		ResponseDTO res = new ResponseDTO();
		res.setData(ntfData);
		res.setStatus(200);
		res.setMessage("Employee notified successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@PostMapping("getEmpVPresentToday")
	public ResponseDTO getEmpVehiclePresentToday(@RequestBody EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("request Processing of getEmpVehiclePresentToday ");
		List<EmployeeVehicleListDTO> empVehiclePresentToday = employeeVehicleMasterService
				.getEmpVehiclePresentToday(dto);
		ResponseDTO res = new ResponseDTO();
		res.setData(empVehiclePresentToday);
		res.setStatus(200);
		res.setMessage("Employee vehicle details for today fetched successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@GetMapping(value = "getFuelData")
	public ResponseDTO getFuleData() throws Exception {
		log.info("Processing request getFuleData");
		ResponseDTO res = new ResponseDTO();
		List<FuelType> list = employeeVehicleMasterService.getAllFuelTypeData();
		res.setData(list);
		res.setStatus(200);
		res.setMessage("Fuel types fetched successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@GetMapping(value = "getVehicleTypeData")
	public ResponseDTO vehicleTypeData() throws Exception {
		log.info("Processing request vehicleTypeData");
		ResponseDTO res = new ResponseDTO();
		List<VehicleTypeMaster> list = employeeVehicleMasterService.getAllVehicleTypeMaster();
		res.setData(list);
		res.setStatus(200);
		res.setMessage("Vehicle types fetched successfully.");
		res.setSuccess(true);
		return res;

	}

	/*
	 * 
	 */
	@PostMapping(value = "getVehicleRegNoOfEmp")
	public ResponseDTO getVehicleRegNoOfEmp(@RequestBody EmployeeVRegNoFetchDTO dto) throws Exception {
		log.info("Processing request  getVehicleRegNoOfEmp");
		ResponseDTO res = new ResponseDTO();
		List<ResponseEmployeeVRegNoFetchDTO> list = employeeVehicleMasterService.getAllVehicleRegNo(dto);
		res.setData(list);
		res.setStatus(200);
		res.setMessage("Vehicle registration no fetched successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@GetMapping(value = "getAllBaseLocation")
	public ResponseDTO getAllBaseLocation() throws Exception {
		log.info("Processing request vehicleTypeData");
		ResponseDTO res = new ResponseDTO();
		// List<BaseLocation> list = baseLocationService.getAllBaseLocation();
		List<ResponseBaseLocationAndBuildingMaster> resBM = baseLocationService.getAllBuildingsAndBaseLocations();
		res.setData(resBM);
		res.setStatus(200);
		res.setMessage("Base locations fetched successfully.");
		res.setSuccess(true);
		return res;
	}

	/*
	 * checkIsLocationAvl And Send AccessToken 
	 */
	@PostMapping(value = "checkIsLocationAvl")
	public ResponseDTO checkIsLocationAvl(@RequestBody CheckLocationResponseDTO dto) throws Exception {
		log.info("Processing request  getVehicleRegNoOfEmp");
		ResponseDTO res = new ResponseDTO();
		boolean response = securityGaurdMasterService.checkIsLocationAvl(dto);
		if (response == true) {
			String generateToken = jwtUtil.generateToken(dto.getEmpId());
			res.setData(generateToken);
			res.setMessage("Access approved");
			res.setSuccess(true);
		} else {
			res.setMessage("Access denied");
			res.setSuccess(false);
		}
		//res.setData(response);
		res.setStatus(200);
		return res;
	}

	/*
	 * 
	 */
	@PostMapping(value = "givenReservedParking")
	public ResponseDTO givenReservedParking(@Valid @RequestBody EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Processing request givenReservedParking ", dto);
		ResponseDTO res = new ResponseDTO();
		EmployeeVehicleMaster empVMById = employeeVehicleMasterService.givenReservedParking(dto);
		res.setData(empVMById);
		res.setMessage("Reserved parking has been assigned to " + empVMById.getVehicleRegNo());
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * 
	 */
	@GetMapping(value = "getAllNoOfWheels")
	public ResponseDTO getAllNoOfWheels() throws Exception {
		log.info("Processing request getAllNoOfWheels ");
		ResponseDTO res = new ResponseDTO();
		List<NoOfWheelsMaster> list = noOfWheelsService.getAllWheelsData();
		res.setData(list);
		res.setMessage("No of wheels fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Generate Qr Code
	 */
	@PostMapping(value = "generateQrCode")
	public ResponseDTO generateQrCode(@Valid @RequestBody EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Processing request generateQrCode ", dto);
		ResponseDTO res = new ResponseDTO();
		EmployeeVehicleMasterDTO emp = new EmployeeVehicleMasterDTO();
		emp.setEmpId(dto.getEmpId());
		emp.setVehicleRegNo(dto.getVehicleRegNo());
		ResponseOfEmpVRegistration generateQRCodeImage = employeeVehicleMasterService.generateOnlyQRCodeImage(emp);
		res.setData(generateQRCodeImage);
		res.setMessage("QR code fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Get Report1 DateWise Data
	 */
	@PostMapping(value = "dateWise/VehicleInOutData")
	public ResponseDTO dateWiseVehicleInOutData(@Valid @RequestBody Report1DTO dto, HttpServletResponse response)
			throws Exception {
		log.info("Processing request dateWiseVehicleInOutData ", dto);
		// response.setContentType("application/octet-stream");
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=EmployeeVehicleDetails_" + LocalDate.now() + ".xlsx";
		response.setHeader(headerKey, headerValue);
		ResponseDTO res = new ResponseDTO();
		List<VehicleInOut> list = reportService.getDateWiseVehicleInOutData(dto);

		// EmployeeVehicleDetailsExcelExporter exporter = new
		// EmployeeVehicleDetailsExcelExporter(list);
		// exporter.export(response);
		res.setData(list);
		res.setMessage("Employee parking details fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Get Report1 DateWise Data in Excel Sheet
	 */
	@PostMapping(value = "dateWise/VehicleInOutData/excel")
	public ResponseDTO dateWiseVehicleInOutDataExcel(@Valid @RequestBody Report1DTO dto, HttpServletResponse response)
			throws Exception {
		log.info("Processing request dateWiseVehicleInOutData ", dto);
		// response.setContentType("application/octet-stream");
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=EmployeeVehicleDetails_" + LocalDate.now() + ".xlsx";
		response.setHeader(headerKey, headerValue);
		ResponseDTO res = new ResponseDTO();
		List<VehicleInOut> list = reportService.getDateWiseVehicleInOutData(dto);
		EmployeeVehicleDetailsExcelExporter exporter = new EmployeeVehicleDetailsExcelExporter(list);
		exporter.export(response);
		res.setData(list);
		res.setMessage("Employee parking details fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Get Report1 DateWise Data
	 * 
	 * @GetMapping(value = "report1") public ResponseDTO
	 * report1OutData(HttpServletResponse response) throws Exception {
	 * log.info("Processing request dateWiseVehicleInOutData "); Report1DTO dto= new
	 * Report1DTO(); dto.setEmpId("203442"); dto.setEndDate(LocalDate.now());
	 * dto.setStartDate(LocalDate.now());
	 * //response.setContentType("application/octet-stream"); String headerKey =
	 * "Content-Disposition"; String headerValue =
	 * "attachment; filename=EmployeeVehicleDetails_" + LocalDate.now() + ".xlsx";
	 * response.setHeader(headerKey, headerValue); ResponseDTO res = new
	 * ResponseDTO(); List<VehicleInOut> list =
	 * reportService.getDateWiseVehicleInOutData(dto);
	 * 
	 * EmployeeVehicleDetailsExcelExporter exporter= new
	 * EmployeeVehicleDetailsExcelExporter(list); exporter.export(response);
	 * res.setData(list);
	 * res.setMessage("Employee parking details fetched successfully.");
	 * res.setStatus(200); res.setSuccess(true); return res; }
	 */

	/*
	 * Get Report2 DateWise Data
	 */
	@PostMapping(value = "dateWise/EmpAndVisitorData")
	public ResponseDTO dateWiseEmpAndVisitorData(@Valid @RequestBody Report2DTO dto, HttpServletResponse response)
			throws Exception {
		log.info("Processing request dateWiseEmpAndVisitorData ", dto);
		// response.setContentType("application/octet-stream");
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=VehicleDetails_" + LocalDate.now() + ".xlsx";
		response.setHeader(headerKey, headerValue);
		ResponseDTO res = new ResponseDTO();
		Report2ResponseDTO empAndVisitorData = reportService.getEmpAndVisitorData(dto);

		// VehicleExcelExporter excelExporter = new
		// VehicleExcelExporter(empAndVisitorData);

		// excelExporter.export(response);
		res.setData(empAndVisitorData);
		res.setMessage("Datewise parking details fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Get Report2 DateWise Data in excel sheet
	 */
	@PostMapping(value = "dateWise/EmpAndVisitorData/excel")
	public ResponseDTO dateWiseEmpAndVisitorDataExcel(@Valid @RequestBody Report2DTO dto, HttpServletResponse response)
			throws Exception {
		log.info("Processing request dateWiseEmpAndVisitorDataExcel ", dto);
		// response.setContentType("application/octet-stream");
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=VehicleDetails_" + LocalDate.now() + ".xlsx";
		response.setHeader(headerKey, headerValue);
		ResponseDTO res = new ResponseDTO();
		Report2ResponseDTO empAndVisitorData = reportService.getEmpAndVisitorData(dto);

		VehicleExcelExporter excelExporter = new VehicleExcelExporter(empAndVisitorData);

		excelExporter.export(response);
		res.setData(empAndVisitorData);
		res.setMessage("Datewise parking details fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Get Report1 DateWise Data
	 */
	@PostMapping(value = "previewImage")
	public ResponseDTO previewImage(@Valid @RequestBody EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Processing request previewImage ", dto);
		ResponseDTO res = new ResponseDTO();
		EmployeeVehicleMaster previewImage = employeeVehicleMasterService.previewImage(dto);
		res.setData(previewImage);
		res.setMessage("Preview image successful.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Register Security Gaurd Data
	 */
	@PostMapping(value = "registerSecurityGuard")
	public ResponseDTO registerSecurityGuard(@Valid @RequestBody SecurityGaurdMasterDTO dto) throws Exception {
		log.info("Processing request registerSecurityGuard ", dto);
		ResponseDTO res = new ResponseDTO();
		SecurityGaurdMaster securityG = securityGaurdMasterService.registerSecurityG(dto);
		res.setData(securityG);
		res.setMessage("Security guard registered successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Register Security Gaurd Data
	 */
	@PostMapping(value = "changePsSlot")
	public ResponseDTO changePsSlot(@Valid @RequestBody ChangeParkingSlotDTO dto) throws Exception {
		log.info("Processing request changePsSlot ", dto);
		ResponseDTO res = new ResponseDTO();
		String changePsSlot = parkingStationMasterService.changePsSlot(dto);
		res.setData(changePsSlot);
		res.setMessage("Parking station updated successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Register Security Gaurd Data
	 */
	@PostMapping(value = "parkingBasedReport")
	public ResponseDTO parkingBasedReport(@Valid @RequestBody Report3DTO dto) throws Exception {
		log.info("Processing request parkingBasedReport ", dto);
		ResponseDTO res = new ResponseDTO();
		Report2ResponseDTO report3 = reportService.getEmpAndVisitorDataByPsId(dto);
		res.setData(report3);
		res.setMessage("Details fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Register Security Gaurd Data
	 */
	@PostMapping(value = "mailSend")
	public ResponseDTO mailSend(@RequestBody EmployeeVehicleMasterDTO dto) throws Exception {
		log.info("Processing request mail send  ");
		ResponseOfEmpVRegistration qrCodeImageAndSendMail = employeeVehicleMasterService.generateQRCodeImage(dto);
		ResponseDTO res = new ResponseDTO();
		res.setData(qrCodeImageAndSendMail);
		res.setMessage("Mail send successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

	/*
	 * Register Security Gaurd Data
	 */
	@PostMapping(value = "generateAccessToken")
	public ResponseDTO generateAccessToken(@RequestBody SecurityGaurdTokenDTO dto) throws Exception {
		log.info("Processing request mail send  ");
		ResponseDTO res = new ResponseDTO();

		/*
		 * manager.authenticate( new
		 * UsernamePasswordAuthenticationToken(dto.getSecurityGaurdId(),
		 * dto.getSecurityGaurdId()));
		 */
		String generateToken = jwtUtil.generateToken(dto.getSecurityGaurdId());
		res.setData(generateToken);
		res.setMessage("Details fetched successfully.");
		res.setStatus(200);
		res.setSuccess(true);
		return res;
	}

}
