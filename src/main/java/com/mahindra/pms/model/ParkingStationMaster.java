package com.mahindra.pms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ParkingStationMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String psName;
	private Integer capacity;
	private Integer reservedParking;
	private Integer NoReservedParking;
	private Integer NoUnReservedParking;
	private Integer availablilityForReserved;
	private Integer availablilityForUnReserved;
	private Boolean isFullReserved;
	private Boolean isFullUnReserved;
	private String avlNow;
	private Boolean isTwoWheelerPs;
	
	@OneToOne
	private BaseLocation baseLocationId;
	
	@OneToOne
	private BuildingMaster buildingMaster;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPsName() {
		return psName;
	}
	public void setPsName(String psName) {
		this.psName = psName;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	public Integer getReservedParking() {
		return reservedParking;
	}
	public void setReservedParking(Integer reservedParking) {
		this.reservedParking = reservedParking;
	}
	public Integer getNoReservedParking() {
		return NoReservedParking;
	}
	public void setNoReservedParking(Integer noReservedParking) {
		NoReservedParking = noReservedParking;
	}
	public Integer getNoUnReservedParking() {
		return NoUnReservedParking;
	}
	public void setNoUnReservedParking(Integer noUnReservedParking) {
		NoUnReservedParking = noUnReservedParking;
	}
	public Integer getAvailablilityForReserved() {
		return availablilityForReserved;
	}
	public void setAvailablilityForReserved(Integer availablilityForReserved) {
		this.availablilityForReserved = availablilityForReserved;
	}
	public Integer getAvailablilityForUnReserved() {
		return availablilityForUnReserved;
	}
	public void setAvailablilityForUnReserved(Integer availablilityForUnReserved) {
		this.availablilityForUnReserved = availablilityForUnReserved;
	}
	public Boolean getIsFullReserved() {
		return isFullReserved;
	}
	public void setIsFullReserved(Boolean isFullReserved) {
		this.isFullReserved = isFullReserved;
	}
	public Boolean getIsFullUnReserved() {
		return isFullUnReserved;
	}
	public void setIsFullUnReserved(Boolean isFullUnReserved) {
		this.isFullUnReserved = isFullUnReserved;
	}
	public String getAvlNow() {
		return avlNow;
	}
	public void setAvlNow(String avlNow) {
		this.avlNow = avlNow;
	}
	public BaseLocation getBaseLocationId() {
		return baseLocationId;
	}
	public void setBaseLocationId(BaseLocation baseLocationId) {
		this.baseLocationId = baseLocationId;
	}
	public Boolean getIsTwoWheelerPs() {
		return isTwoWheelerPs;
	}
	public void setIsTwoWheelerPs(Boolean isTwoWheelerPs) {
		this.isTwoWheelerPs = isTwoWheelerPs;
	}
	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}
	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}
}
