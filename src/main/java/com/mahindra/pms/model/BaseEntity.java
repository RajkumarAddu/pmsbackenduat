package com.mahindra.pms.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDateTime registered;

	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@JsonIgnore
	public LocalDateTime modified;

	@JsonIgnore
	private boolean status;

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public LocalDateTime getRegistered() {
		return registered;
	}

	public void setRegistered(LocalDateTime registered) {
		if (registered == null) {
			this.registered = LocalDateTime.now();
			this.modified = LocalDateTime.now();
		} else
			this.registered = registered;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified() {
		this.modified = LocalDateTime.now();
	}
}
