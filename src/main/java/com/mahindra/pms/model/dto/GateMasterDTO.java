package com.mahindra.pms.model.dto;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class GateMasterDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer gateNo;
	private String gateName;
	
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDate created;

	//@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@JsonIgnore
	public LocalDate modified;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getGateNo() {
		return gateNo;
	}
	public void setGateNo(Integer gateNo) {
		this.gateNo = gateNo;
	}
	public String getGateName() {
		return gateName;
	}
	public void setGateName(String gateName) {
		this.gateName = gateName;
	}
	
	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public LocalDate getModified() {
		return modified;
	}

	public void setModified(LocalDate modified) {
		this.modified = modified;
	}
}
