package com.mahindra.pms.model.dto;

public class EmployeeVehicleRegNoDTO {

	private String vehicleRegNo;

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	
}
