package com.mahindra.pms.model.dto;

import java.util.List;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;

public class ResponseBaseLocationAndBuildingMaster {

	private BaseLocation baseLocation;
	private List<BuildingMaster> buildingMaster;
	
	public BaseLocation getBaseLocation() {
		return baseLocation;
	}
	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}
	public List<BuildingMaster> getBuildingMaster() {
		return buildingMaster;
	}
	public void setBuildingMaster(List<BuildingMaster> buildingMaster) {
		this.buildingMaster = buildingMaster;
	}
	
}
