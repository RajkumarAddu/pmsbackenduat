package com.mahindra.pms.model.dto;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.FuelType;
import com.mahindra.pms.model.NoOfWheelsMaster;
import com.mahindra.pms.model.VehicleTypeMaster;

public class EmployeeVehicleMasterDTO {

	private Integer id;

	// @NotEmpty(message = "Employee id is required")
	private String empId;

	// @NotEmpty(message = "name is required")
	private String empName;

	// @NotNull(message = "BaseLocation is required")
	private BaseLocation baseLocation;// some issue here according to discussion

	private BuildingMaster buildingMaster;
	
	// @NotNull(message = "mobile no. is required")
	private Long mobile;

	// @Email(message = "please insert Proper email address")
	// @NotNull(message = "email address is required")
	private String email;

	// @NotEmpty(message = "(Vehicle Registration Number is required")
	private String vehicleRegNo;

	// @NotEmpty(message = "(Vehicle Model is required")
	private String model;

	// @NotEmpty(message = "(Vehicle Make is required")
	private String make;

	// @NotNull(message = "(Number Of vehicle Wheels is required")
	private NoOfWheelsMaster noOfWheels;

	private String pucFileName;

	private LocalDate pucValidTo;

	private String insuranceFileName;

	private LocalDate insuranceValidTo;

	private String licenseFileName;

	private LocalDate licenseValidTo;

	private String rcBook;

	// @Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDate created = LocalDate.now();

	// @Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@JsonIgnore
	public LocalDate modified = LocalDate.now();

	@OneToMany
	// @NotNull(message = "vehicleType is required")
	private List<VehicleTypeMaster> vehicleType;

	@OneToMany
	// @NotNull(message = "FuelType is required")
	private List<FuelType> fuelType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public BaseLocation getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public NoOfWheelsMaster getNoOfWheels() {
		return noOfWheels;
	}

	public void setNoOfWheels(NoOfWheelsMaster noOfWheels) {
		this.noOfWheels = noOfWheels;
	}

	public String getPucFileName() {
		return pucFileName;
	}

	public void setPucFileName(String pucFileName) {
		this.pucFileName = pucFileName;
	}

	public LocalDate getPucValidTo() {
		return pucValidTo;
	}

	public void setPucValidTo(LocalDate pucValidTo) {
		this.pucValidTo = pucValidTo;
	}

	public String getInsuranceFileName() {
		return insuranceFileName;
	}

	public void setInsuranceFileName(String insuranceFileName) {
		this.insuranceFileName = insuranceFileName;
	}

	public LocalDate getInsuranceValidTo() {
		return insuranceValidTo;
	}

	public void setInsuranceValidTo(LocalDate insuranceValidTo) {
		this.insuranceValidTo = insuranceValidTo;
	}

	public String getLicenseFileName() {
		return licenseFileName;
	}

	public void setLicenseFileName(String licenseFileName) {
		this.licenseFileName = licenseFileName;
	}

	public LocalDate getLicenseValidTo() {
		return licenseValidTo;
	}

	public void setLicenseValidTo(LocalDate licenseValidTo) {
		this.licenseValidTo = licenseValidTo;
	}

	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public LocalDate getModified() {
		return modified;
	}

	public void setModified(LocalDate modified) {
		this.modified = modified;
	}

	public List<VehicleTypeMaster> getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(List<VehicleTypeMaster> vehicleType) {
		this.vehicleType = vehicleType;
	}

	public List<FuelType> getFuelType() {
		return fuelType;
	}

	public void setFuelType(List<FuelType> fuelType) {
		this.fuelType = fuelType;
	}

	public String getRcBook() {
		return rcBook;
	}

	public void setRcBook(String rcBook) {
		this.rcBook = rcBook;
	}

	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}

	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}

	
}
