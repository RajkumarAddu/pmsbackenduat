package com.mahindra.pms.model.dto;

import com.mahindra.pms.model.BaseLocation;

public class SecurityGaurdMasterDTO {

	private String empId;
	private BaseLocation baseLocatoin;
	private boolean isAdmin;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public BaseLocation getBaseLocatoin() {
		return baseLocatoin;
	}

	public void setBaseLocatoin(BaseLocation baseLocatoin) {
		this.baseLocatoin = baseLocatoin;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

}
