package com.mahindra.pms.model.dto;

import com.mahindra.pms.model.EmployeeVehicleMaster;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.VehicleInOut;

public class EmployeeVehicleListDTO implements Comparable<EmployeeVehicleListDTO>{

	EmployeeVehicleMaster employeeVehicleMaster;
	ParkingStationMaster psMaster;
	VehicleInOut vehicleInOut;

	public ParkingStationMaster getPsMaster() {
		return psMaster;
	}

	public void setPsMaster(ParkingStationMaster psMaster) {
		this.psMaster = psMaster;
	}

	public EmployeeVehicleMaster getEmployeeVehicleMaster() {
		return employeeVehicleMaster;
	}

	public void setEmployeeVehicleMaster(EmployeeVehicleMaster employeeVehicleMaster) {
		this.employeeVehicleMaster = employeeVehicleMaster;
	}

	public VehicleInOut getVehicleInOut() {
		return vehicleInOut;
	}

	public void setVehicleInOut(VehicleInOut vehicleInOut) {
		this.vehicleInOut = vehicleInOut;
	}

	

	public EmployeeVehicleListDTO() {
		super();
	}

	public EmployeeVehicleListDTO(EmployeeVehicleMaster employeeVehicleMaster, ParkingStationMaster psMaster,
			VehicleInOut vehicleInOut) {
		super();
		this.employeeVehicleMaster = employeeVehicleMaster;
		this.psMaster = psMaster;
		this.vehicleInOut = vehicleInOut;
	}

	@Override
	public int compareTo(EmployeeVehicleListDTO e) {
		if (getVehicleInOut() == null || e.getVehicleInOut()== null) {
		      return 0;
		    }
		    return getVehicleInOut().getId().compareTo(e.getVehicleInOut().getId());
	}

}
