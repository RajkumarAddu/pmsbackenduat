package com.mahindra.pms.model.dto;

import java.time.LocalDate;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.ParkingStationMaster;

public class Report3DTO {
	
	private LocalDate currentDate;
	private BaseLocation baseLocation;
	private ParkingStationMaster parkingStation;
	private Boolean isReserved;

	public BaseLocation getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}

	public ParkingStationMaster getParkingStation() {
		return parkingStation;
	}

	public void setParkingStation(ParkingStationMaster parkingStation) {
		this.parkingStation = parkingStation;
	}

	public Boolean getIsReserved() {
		return isReserved;
	}

	public void setIsReserved(Boolean isReserved) {
		this.isReserved = isReserved;
	}

	public LocalDate getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(LocalDate currentDate) {
		this.currentDate = currentDate;
	}
	
}
