package com.mahindra.pms.model.dto;

public class SecurityGaurdTokenDTO {
	
	private String securityGaurdId;

	public String getSecurityGaurdId() {
		return securityGaurdId;
	}

	public void setSecurityGaurdId(String securityGaurdId) {
		this.securityGaurdId = securityGaurdId;
	}
	

}
