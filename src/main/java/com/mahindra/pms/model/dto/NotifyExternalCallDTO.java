package com.mahindra.pms.model.dto;

public class NotifyExternalCallDTO {

	private String devhed;
	private String msg;
	private String title;

	public String getDevhed() {
		return devhed;
	}

	public void setDevhed(String devhed) {
		this.devhed = devhed;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
