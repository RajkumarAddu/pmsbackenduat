package com.mahindra.pms.model.dto;

public class ResponseEmployeeVRegNoFetchDTO {

	private String vehicleRegNo;
	private String vehicleMake;

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vehicleMake == null) ? 0 : vehicleMake.hashCode());
		result = prime * result + ((vehicleRegNo == null) ? 0 : vehicleRegNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseEmployeeVRegNoFetchDTO other = (ResponseEmployeeVRegNoFetchDTO) obj;
		if (vehicleMake == null) {
			if (other.vehicleMake != null)
				return false;
		} else if (!vehicleMake.equals(other.vehicleMake))
			return false;
		if (vehicleRegNo == null) {
			if (other.vehicleRegNo != null)
				return false;
		} else if (!vehicleRegNo.equals(other.vehicleRegNo))
			return false;
		return true;
	}
	
	

}
