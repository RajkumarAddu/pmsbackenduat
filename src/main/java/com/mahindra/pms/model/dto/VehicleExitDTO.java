package com.mahindra.pms.model.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.mahindra.pms.model.GateMaster;

public class VehicleExitDTO {

	//@NotEmpty(message = "empId is required")
	private String empId;
	
	//@NotEmpty(message = "vehicleRegNo is required")
	private String vehicleRegNo;
	
	private LocalDateTime exitDateTime=LocalDateTime.now();
	
	//@NotNull(message = "exitGate is required")
	private GateMaster exitGate;
	
	//@NotEmpty(message = "exitDriverName is required")
	private String exitDriverName;
	
	//@NotNull(message = "exitKm is required")
	private Integer exitKm;
	
	private boolean isReserved;
	
	//@NotEmpty(message = "updatedBy is required")
	private String updatedBy;
	private boolean isExit = true;
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public LocalDateTime getExitDateTime() {
		return exitDateTime;
	}
	public void setExitDateTime(LocalDateTime exitDateTime) {
		this.exitDateTime = exitDateTime;
	}
	public GateMaster getExitGate() {
		return exitGate;
	}
	public void setExitGate(GateMaster exitGate) {
		this.exitGate = exitGate;
	}
	public String getExitDriverName() {
		return exitDriverName;
	}
	public void setExitDriverName(String exitDriverName) {
		this.exitDriverName = exitDriverName;
	}
	public Integer getExitKm() {
		return exitKm;
	}
	public void setExitKm(Integer exitKm) {
		this.exitKm = exitKm;
	}
	public boolean isReserved() {
		return isReserved;
	}
	public void setReserved(boolean isReserved) {
		this.isReserved = isReserved;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public boolean isExit() {
		return isExit;
	}
	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}
	
}
