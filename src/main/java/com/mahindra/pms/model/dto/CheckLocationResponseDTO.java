package com.mahindra.pms.model.dto;

import com.mahindra.pms.model.BaseLocation;

public class CheckLocationResponseDTO {

	private String empId;
	private BaseLocation baseLocationId;
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public BaseLocation getBaseLocationId() {
		return baseLocationId;
	}
	public void setBaseLocationId(BaseLocation baseLocationId) {
		this.baseLocationId = baseLocationId;
	}
	
	
}
