package com.mahindra.pms.model.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.mahindra.pms.model.GateMaster;

public class VisitorVehicleExitDTO {

	//@NotEmpty(message = "Vehicle Registration Number is required")
	private String vehicleRegNo;
	
	//@NotNull(message = "exitGate is required")
	private GateMaster exitGate;
	
	//@NotEmpty(message = "updatedBy is required")
	private String updatedBy;

	public GateMaster getExitGate() {
		return exitGate;
	}

	public void setExitGate(GateMaster exitGate) {
		this.exitGate = exitGate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	

}
