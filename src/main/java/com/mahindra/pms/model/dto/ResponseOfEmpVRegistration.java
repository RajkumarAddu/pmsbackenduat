package com.mahindra.pms.model.dto;

public class ResponseOfEmpVRegistration {

	private String qrCodeImageString;
	private String qrCodePdfString;

	public String getQrCodeImageString() {
		return qrCodeImageString;
	}

	public void setQrCodeImageString(String qrCodeImageString) {
		this.qrCodeImageString = qrCodeImageString;
	}

	public String getQrCodePdfString() {
		return qrCodePdfString;
	}

	public void setQrCodePdfString(String qrCodePdfString) {
		this.qrCodePdfString = qrCodePdfString;
	}

}
