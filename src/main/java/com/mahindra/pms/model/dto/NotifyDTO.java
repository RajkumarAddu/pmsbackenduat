package com.mahindra.pms.model.dto;

import com.mahindra.pms.model.ParkingStationMaster;

public class NotifyDTO {

	private String vehicleRegNo;
	private String obeservedIssue;
	private ParkingStationMaster parkingStation;
	private Boolean notificationSend;

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	
	public String getObeservedIssue() {
		return obeservedIssue;
	}

	public void setObeservedIssue(String obeservedIssue) {
		this.obeservedIssue = obeservedIssue;
	}

	public ParkingStationMaster getParkingStation() {
		return parkingStation;
	}

	public void setParkingStation(ParkingStationMaster parkingStation) {
		this.parkingStation = parkingStation;
	}

	public Boolean getNotificationSend() {
		return notificationSend;
	}

	public void setNotificationSend(Boolean notificationSend) {
		this.notificationSend = notificationSend;
	}

}
