package com.mahindra.pms.model.dto;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;

public class BaseLocationFetchResponseDTO {

	private BaseLocation baseLocation;
	private BuildingMaster buildingMaster;

	public BaseLocation getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}

	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}

	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}

}
