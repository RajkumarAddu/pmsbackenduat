package com.mahindra.pms.model.dto;

public class ParkingStationMasterResponseDTO {
	
	private String unReservedParkingStaitonName;
	private Integer NoUnReservedParkingSlots;
	private String reservedParkingStaitonName;
	private Integer NoReservedParkingSlots;

	public String getUnReservedParkingStaitonName() {
		return unReservedParkingStaitonName;
	}

	public void setUnReservedParkingStaitonName(String unReservedParkingStaitonName) {
		this.unReservedParkingStaitonName = unReservedParkingStaitonName;
	}

	public Integer getNoUnReservedParkingSlots() {
		return NoUnReservedParkingSlots;
	}

	public void setNoUnReservedParkingSlots(Integer noUnReservedParkingSlots) {
		NoUnReservedParkingSlots = noUnReservedParkingSlots;
	}

	public String getReservedParkingStaitonName() {
		return reservedParkingStaitonName;
	}

	public void setReservedParkingStaitonName(String reservedParkingStaitonName) {
		this.reservedParkingStaitonName = reservedParkingStaitonName;
	}

	public Integer getNoReservedParkingSlots() {
		return NoReservedParkingSlots;
	}

	public void setNoReservedParkingSlots(Integer noReservedParkingSlots) {
		NoReservedParkingSlots = noReservedParkingSlots;
	}

}
