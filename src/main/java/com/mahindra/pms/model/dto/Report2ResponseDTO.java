package com.mahindra.pms.model.dto;

import java.util.List;
import java.util.Map;

import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VisitorVehicleDetails;

public class Report2ResponseDTO {

	private List<VehicleInOut> vehicleInOut;
	private List<VisitorVehicleDetails> visitorVehicleDetails;
	private Integer count;
	
	public List<VehicleInOut> getVehicleInOut() {
		return vehicleInOut;
	}

	public void setVehicleInOut(List<VehicleInOut> vehicleInOut) {
		this.vehicleInOut = vehicleInOut;
	}

	public List<VisitorVehicleDetails> getVisitorVehicleDetails() {
		return visitorVehicleDetails;
	}

	public void setVisitorVehicleDetails(List<VisitorVehicleDetails> visitorVehicleDetails) {
		this.visitorVehicleDetails = visitorVehicleDetails;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	
}
