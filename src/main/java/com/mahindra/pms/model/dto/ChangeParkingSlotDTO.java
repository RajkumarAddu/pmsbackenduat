package com.mahindra.pms.model.dto;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.EmployeeVehicleMaster;
import com.mahindra.pms.model.ParkingStationMaster;

public class ChangeParkingSlotDTO {

	private EmployeeVehicleMaster employeeVehicleMaster;
	private ParkingStationMaster oldParkingStation;
	private ParkingStationMaster newParkingStation;
	private BaseLocation baseLocation;
	private BuildingMaster buildingMaster;

	
	public EmployeeVehicleMaster getEmployeeVehicleMaster() {
		return employeeVehicleMaster;
	}

	public void setEmployeeVehicleMaster(EmployeeVehicleMaster employeeVehicleMaster) {
		this.employeeVehicleMaster = employeeVehicleMaster;
	}

	public ParkingStationMaster getOldParkingStation() {
		return oldParkingStation;
	}

	public void setOldParkingStation(ParkingStationMaster oldParkingStation) {
		this.oldParkingStation = oldParkingStation;
	}

	public ParkingStationMaster getNewParkingStation() {
		return newParkingStation;
	}

	public void setNewParkingStation(ParkingStationMaster newParkingStation) {
		this.newParkingStation = newParkingStation;
	}

	public BaseLocation getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}

	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}

	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}
	
}
