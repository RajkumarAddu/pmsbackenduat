package com.mahindra.pms.model.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.GateMaster;

public class VehicleEntryDTO {
	
	//@NotEmpty(message = "empId is required")
	private String empId;
	
	//@NotEmpty(message = "vehicleRegNo is required")
	private String vehicleRegNo;
	
	private LocalDateTime entryDateTime = LocalDateTime.now();
	
	//@NotNull(message = "entryGate is required")
	private GateMaster entryGate;
	
	//@NotEmpty(message = "entryDriverName is required")
	private String entryDriverName;
	
	//@NotNull(message = "entryKm is required")
	private Integer entryKm;
	
	
	private boolean isReserved;
	
	private BuildingMaster buildingMaster;
	
	private BaseLocation baseLocationId;
	
	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDate created = LocalDate.now();
	
	//@NotEmpty(message = "createdBy is required")
	private String createdBy;
	
	private boolean isExit = true;
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	
	public String getEntryDriverName() {
		return entryDriverName;
	}
	public void setEntryDriverName(String entryDriverName) {
		this.entryDriverName = entryDriverName;
	}
	public Integer getEntryKm() {
		return entryKm;
	}
	public void setEntryKm(Integer entryKm) {
		this.entryKm = entryKm;
	}
	
	public boolean isReserved() {
		return isReserved;
	}
	public void setReserved(boolean isReserved) {
		this.isReserved = isReserved;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public boolean isExit() {
		return isExit;
	}
	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}
	public BaseLocation getBaseLocationId() {
		return baseLocationId;
	}
	public void setBaseLocationId(BaseLocation baseLocationId) {
		this.baseLocationId = baseLocationId;
	}
	public GateMaster getEntryGate() {
		return entryGate;
	}
	public void setEntryGate(GateMaster entryGate) {
		this.entryGate = entryGate;
	}
	public LocalDate getCreated() {
		return created;
	}
	public void setCreated(LocalDate created) {
		this.created = created;
	}
	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}
	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}
	
}
