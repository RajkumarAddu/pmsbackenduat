package com.mahindra.pms.model.dto;

import com.mahindra.pms.model.BaseLocation;

public class EmployeeFetchDetailsDTO {

	//@NotNull(message = "Employee Id is Mandatory")
	private String empId;
	
	//@NotBlank(message = "vehicle Registration Number is Mandatory")
	private String vehicleRegNo;
	
	private BaseLocation baseLocation;
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public BaseLocation getBaseLocation() {
		return baseLocation;
	}
	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}
	
	
   
}
