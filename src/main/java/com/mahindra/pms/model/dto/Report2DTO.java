package com.mahindra.pms.model.dto;

import java.time.LocalDate;

import com.mahindra.pms.model.NoOfWheelsMaster;
import com.mahindra.pms.model.ParkingStationMaster;

public class Report2DTO {

	
	private LocalDate startDate;
	private LocalDate endDate;
	private NoOfWheelsMaster noOfWheelsMaster;
	//private Boolean includeVisitor;
	private Boolean presentInPremises;
	private ParkingStationMaster parkingStationMaster;

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public NoOfWheelsMaster getNoOfWheelsMaster() {
		return noOfWheelsMaster;
	}

	public void setNoOfWheelsMaster(NoOfWheelsMaster noOfWheelsMaster) {
		this.noOfWheelsMaster = noOfWheelsMaster;
	}

	public Boolean getPresentInPremises() {
		return presentInPremises;
	}

	public void setPresentInPremises(Boolean presentInPremises) {
		this.presentInPremises = presentInPremises;
	}

	public ParkingStationMaster getParkingStationMaster() {
		return parkingStationMaster;
	}

	public void setParkingStationMaster(ParkingStationMaster parkingStationMaster) {
		this.parkingStationMaster = parkingStationMaster;
	}
	
	
}
