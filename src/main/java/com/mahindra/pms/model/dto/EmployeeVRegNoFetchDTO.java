package com.mahindra.pms.model.dto;

public class EmployeeVRegNoFetchDTO {

	private String empId;
	private String vehicleList;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getVehicleList() {
		return vehicleList;
	}

	public void setVehicleList(String vehicleList) {
		this.vehicleList = vehicleList;
	}

	
}
