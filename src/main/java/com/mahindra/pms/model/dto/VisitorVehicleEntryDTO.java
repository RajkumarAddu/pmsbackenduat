package com.mahindra.pms.model.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.GateMaster;
import com.mahindra.pms.model.NoOfWheelsMaster;
import com.mahindra.pms.model.ParkingStationMaster;

public class VisitorVehicleEntryDTO {

	// @NotEmpty(message = "name is required")
	private String visitorName;

	// @NotNull(message = "Number Of vehicle Wheels is required")
	private NoOfWheelsMaster noOfWheels;

	private BuildingMaster buildingMaster;
	
	private Long mobile;
	
	// @NotEmpty(message = "Vehicle Make is required")
	private String vehicleMake;

	// @NotEmpty(message = "Vehicle Registration Number is required")
	private String vehicleRegNo;

	// @NotEmpty(message = "Visitor visit purpose is required")
	private String visitPurpose;

	// @NotEmpty(message = "Visitor drop point is required")
	private String dropPoint;

	// @NotNull(message = "visitor toBeParked is required")
	private Boolean toBeParked;

	// @NotEmpty(message = "created By is required")
	private String createdBy;

	// @NotNull(message = "entryGate is required")
	private GateMaster entryGate;

	// @NotNull(message = "baseLocation is required")
	private BaseLocation baseLocation;
	
	

	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDate created = LocalDate.now();

	@LastModifiedDate
	@JsonIgnore
	public LocalDate modified = LocalDate.now();

	public String getVisitorName() {
		return visitorName;
	}

	public void setVisitorName(String visitorName) {
		this.visitorName = visitorName;
	}

	public NoOfWheelsMaster getNoOfWheels() {
		return noOfWheels;
	}

	public void setNoOfWheels(NoOfWheelsMaster noOfWheels) {
		this.noOfWheels = noOfWheels;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getVisitPurpose() {
		return visitPurpose;
	}

	public void setVisitPurpose(String visitPurpose) {
		this.visitPurpose = visitPurpose;
	}

	public String getDropPoint() {
		return dropPoint;
	}

	public void setDropPoint(String dropPoint) {
		this.dropPoint = dropPoint;
	}

	public Boolean getToBeParked() {
		return toBeParked;
	}

	public void setToBeParked(Boolean toBeParked) {
		this.toBeParked = toBeParked;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public GateMaster getEntryGate() {
		return entryGate;
	}

	public void setEntryGate(GateMaster entryGate) {
		this.entryGate = entryGate;
	}

	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public LocalDate getModified() {
		return modified;
	}

	public void setModified(LocalDate modified) {
		this.modified = modified;
	}

	public BaseLocation getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}

	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}

	
}
