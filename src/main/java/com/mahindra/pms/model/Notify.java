package com.mahindra.pms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Notify{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String vehicleRegNo;
	@OneToOne
	private ParkingStationMaster parkingStation;//to be changed
	private String obeservedIssue;
	private Boolean notificationSend;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	
	public String getObeservedIssue() {
		return obeservedIssue;
	}
	public void setObeservedIssue(String obeservedIssue) {
		this.obeservedIssue = obeservedIssue;
	}
	public Boolean getNotificationSend() {
		return notificationSend;
	}
	public void setNotificationSend(Boolean notificationSend) {
		this.notificationSend = notificationSend;
	}
	public ParkingStationMaster getParkingStation() {
		return parkingStation;
	}
	public void setParkingStation(ParkingStationMaster parkingStation) {
		this.parkingStation = parkingStation;
	}
	
	
	
}
