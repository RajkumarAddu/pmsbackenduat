package com.mahindra.pms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class BuildingMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String buildingName;
	
	@OneToOne
	private BaseLocation baseLocation;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public BaseLocation getBaseLocation() {
		return baseLocation;
	}
	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}
	
}
