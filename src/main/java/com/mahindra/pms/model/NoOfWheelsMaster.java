package com.mahindra.pms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NoOfWheelsMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer noOfWheels;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNoOfWheels() {
		return noOfWheels;
	}
	public void setNoOfWheels(Integer noOfWheels) {
		this.noOfWheels = noOfWheels;
	}
	
	


}
