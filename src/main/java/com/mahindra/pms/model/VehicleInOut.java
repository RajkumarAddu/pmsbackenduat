package com.mahindra.pms.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class VehicleInOut {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String empId;
	private String vehicleRegNo;
	private LocalDateTime entryDateTime;
	
	@OneToOne
	private GateMaster entryGate;
	
	private LocalDateTime exitDateTime;
	
	@OneToOne
	private GateMaster exitGate;
	
	private String entryDriverName;
	private String exitDriverName;
	private Integer entryKm;
	private Integer exitKm;
	private boolean isReserved;
	private String createdBy;
	private String updatedBy;
	private boolean isExit;
	@OneToOne
	private EmployeeVehicleMaster vehicleId;
	@OneToOne
	private ParkingStationMaster parkingStaionId;//to be changed
	
	@OneToOne
	private BuildingMaster buildingMaster;
	
	@OneToOne
	private BaseLocation baseLocationId;
	
	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDate created = LocalDate.now();
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	
	public LocalDateTime getExitDateTime() {
		return exitDateTime;
	}
	public void setExitDateTime(LocalDateTime exitDateTime) {
		this.exitDateTime = exitDateTime;
	}
	
	public String getEntryDriverName() {
		return entryDriverName;
	}
	public void setEntryDriverName(String entryDriverName) {
		this.entryDriverName = entryDriverName;
	}
	public String getExitDriverName() {
		return exitDriverName;
	}
	public void setExitDriverName(String exitDriverName) {
		this.exitDriverName = exitDriverName;
	}
	public Integer getEntryKm() {
		return entryKm;
	}
	public void setEntryKm(Integer entryKm) {
		this.entryKm = entryKm;
	}
	public Integer getExitKm() {
		return exitKm;
	}
	public void setExitKm(Integer exitKm) {
		this.exitKm = exitKm;
	}
	public boolean isReserved() {
		return isReserved;
	}
	public void setReserved(boolean isReserved) {
		this.isReserved = isReserved;
	}
	public EmployeeVehicleMaster getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(EmployeeVehicleMaster vehicleId) {
		this.vehicleId = vehicleId;
	}
	public ParkingStationMaster getParkingStaionId() {
		return parkingStaionId;
	}
	public void setParkingStaionId(ParkingStationMaster parkingStaionId) {
		this.parkingStaionId = parkingStaionId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public boolean isExit() {
		return isExit;
	}
	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}
	@Override
	public String toString() {
		return "VehicleInOut [id=" + id + ", empId=" + empId + ", vehicleRegNo=" + vehicleRegNo + ", entryDateTime="
				+ entryDateTime + ", entryGate=" + entryGate + ", exitDateTime=" + exitDateTime + ", exitGate="
				+ exitGate + ", entryDriverName=" + entryDriverName + ", exitDriverName=" + exitDriverName
				+ ", entryKm=" + entryKm + ", exitKm=" + exitKm + ", isReserved=" + isReserved + ", createdBy="
				+ createdBy + ", updatedBy=" + updatedBy + ", isExit=" + isExit + ", vehicleId=" + vehicleId
				+ ", parkingStaionId=" + parkingStaionId + "]";
	}
	public BaseLocation getBaseLocationId() {
		return baseLocationId;
	}
	public void setBaseLocationId(BaseLocation baseLocationId) {
		this.baseLocationId = baseLocationId;
	}
	public GateMaster getEntryGate() {
		return entryGate;
	}
	public void setEntryGate(GateMaster entryGate) {
		this.entryGate = entryGate;
	}
	public GateMaster getExitGate() {
		return exitGate;
	}
	public void setExitGate(GateMaster exitGate) {
		this.exitGate = exitGate;
	}
	public LocalDate getCreated() {
		return created;
	}
	public void setCreated(LocalDate created) {
		this.created = created;
	}
	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}
	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}
	
}
