package com.mahindra.pms.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class VisitorVehicleDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String visitorName;

	@OneToOne
	private NoOfWheelsMaster noOfWheels;

	private Long mobile;
	private String vehicleMake;
	private String vehicleRegNo;
	private String visitPurpose;
	private String dropPoint;
	private Boolean toBeParked;
	@OneToOne
	private ParkingStationMaster reservedParkingStation;// to be changed with OneToOne
	private String createdBy;
	private String updatedBy;
	private boolean isExit;
	@OneToOne
	private BaseLocation baseLocation;
	
	@OneToOne
	private BuildingMaster buildingMaster;

	// @Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime entryDateTime;

	@OneToOne
	private GateMaster entryGate;

	// @Temporal(TemporalType.TIMESTAMP)
	private LocalDateTime exitDateTime;

	@OneToOne
	private GateMaster exitGate;

	// @Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDate created;

	// @Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@JsonIgnore
	public LocalDate modified;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVisitorName() {
		return visitorName;
	}

	public void setVisitorName(String visitorName) {
		this.visitorName = visitorName;
	}

	public NoOfWheelsMaster getNoOfWheels() {
		return noOfWheels;
	}

	public void setNoOfWheels(NoOfWheelsMaster noOfWheels) {
		this.noOfWheels = noOfWheels;
	}

	public String getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getVisitPurpose() {
		return visitPurpose;
	}

	public void setVisitPurpose(String visitPurpose) {
		this.visitPurpose = visitPurpose;
	}

	public String getDropPoint() {
		return dropPoint;
	}

	public void setDropPoint(String dropPoint) {
		this.dropPoint = dropPoint;
	}

	public Boolean getToBeParked() {
		return toBeParked;
	}

	public void setToBeParked(Boolean toBeParked) {
		this.toBeParked = toBeParked;
	}

	public ParkingStationMaster getReservedParkingStation() {
		return reservedParkingStation;
	}

	public void setReservedParkingStation(ParkingStationMaster reservedParkingStation) {
		this.reservedParkingStation = reservedParkingStation;
	}

	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public GateMaster getEntryGate() {
		return entryGate;
	}

	public void setEntryGate(GateMaster entryGate) {
		this.entryGate = entryGate;
	}

	public GateMaster getExitGate() {
		return exitGate;
	}

	public void setExitGate(GateMaster exitGate) {
		this.exitGate = exitGate;
	}

	public LocalDateTime getExitDateTime() {
		return exitDateTime;
	}

	public void setExitDateTime(LocalDateTime exitDateTime) {
		this.exitDateTime = exitDateTime;
	}

	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public LocalDate getModified() {
		return modified;
	}

	public void setModified(LocalDate modified) {
		this.modified = modified;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public boolean isExit() {
		return isExit;
	}

	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}

	@Override
	public String toString() {
		return "VisitorVehicleDetails [id=" + id + ", visitorName=" + visitorName + ", NoOfWheels=" + noOfWheels
				+ ", vehicleMake=" + vehicleMake + ", vehicleRegNo=" + vehicleRegNo + ", visitPurpose=" + visitPurpose
				+ ", dropPoint=" + dropPoint + ", toBeParked=" + toBeParked + ", reservedParkingStation="
				+ reservedParkingStation + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + ", isExit="
				+ isExit + ", entryDateTime=" + entryDateTime + ", entryGate=" + entryGate + ", exitDateTime="
				+ exitDateTime + ", exitGate=" + exitGate + ", created=" + created + ", modified=" + modified + "]";
	}

	public BaseLocation getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}

	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}
	
}
