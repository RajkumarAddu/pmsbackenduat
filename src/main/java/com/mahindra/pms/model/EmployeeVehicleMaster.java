package com.mahindra.pms.model;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Table(name = "employee_vehicle_master")
public class EmployeeVehicleMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String empId;
	private String empName;

	// changes

	@OneToOne
	private BaseLocation baseLocation;
	
	@OneToOne
	private BuildingMaster buildingMaster;

	private Long mobile;

	private String email;
	private String vehicleRegNo;
	private String model;
	private String make;

	@OneToOne
	private NoOfWheelsMaster noOfWheels;
	
	private boolean reservedParking;

	private String pucFileName;
	private LocalDate pucValidTo;

	private String insuranceFileName;
	private LocalDate insuranceValidTo;

	private String licenseFileName;
	private LocalDate licenseValidTo;
	private String rcBook;

	// @Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	@CreatedDate
	@JsonIgnore
	public LocalDate created;

	// @Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@JsonIgnore
	public LocalDate modified;

	@ManyToMany(targetEntity = VehicleTypeMaster.class)
	@JoinColumn(name = "vehicleTypeId")
	private List<VehicleTypeMaster> vehicleType;

	@ManyToMany(targetEntity = FuelType.class)
	@JoinColumn(name = "fuelTypeId")
	private List<FuelType> fuelType;
	
	private Boolean isDeleted=false;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public BaseLocation getBaseLocation() {
		return baseLocation;
	}

	public void setBaseLocation(BaseLocation baseLocation) {
		this.baseLocation = baseLocation;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public NoOfWheelsMaster getNoOfWheels() {
		return noOfWheels;
	}

	public void setNoOfWheels(NoOfWheelsMaster noOfWheels) {
		this.noOfWheels = noOfWheels;
	}

	public boolean isReservedParking() {
		return reservedParking;
	}

	public void setReservedParking(boolean reservedParking) {
		this.reservedParking = reservedParking;
	}

	public String getPucFileName() {
		return pucFileName;
	}

	public void setPucFileName(String pucFileName) {
		this.pucFileName = pucFileName;
	}

	public LocalDate getPucValidTo() {
		return pucValidTo;
	}

	public void setPucValidTo(LocalDate pucValidTo) {
		this.pucValidTo = pucValidTo;
	}

	public String getInsuranceFileName() {
		return insuranceFileName;
	}

	public void setInsuranceFileName(String insuranceFileName) {
		this.insuranceFileName = insuranceFileName;
	}


	public LocalDate getInsuranceValidTo() {
		return insuranceValidTo;
	}

	public void setInsuranceValidTo(LocalDate insuranceValidTo) {
		this.insuranceValidTo = insuranceValidTo;
	}

	public String getLicenseFileName() {
		return licenseFileName;
	}

	public void setLicenseFileName(String licenseFileName) {
		this.licenseFileName = licenseFileName;
	}

	public LocalDate getLicenseValidTo() {
		return licenseValidTo;
	}

	public void setLicenseValidTo(LocalDate licenseValidTo) {
		this.licenseValidTo = licenseValidTo;
	}

	public LocalDate getCreated() {
		return created;
	}

	public void setCreated(LocalDate created) {
		this.created = created;
	}

	public LocalDate getModified() {
		return modified;
	}

	public void setModified(LocalDate modified) {
		this.modified = modified;
	}

	public List<VehicleTypeMaster> getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(List<VehicleTypeMaster> vehicleType) {
		this.vehicleType = vehicleType;
	}

	public List<FuelType> getFuelType() {
		return fuelType;
	}

	public void setFuelType(List<FuelType> fuelType) {
		this.fuelType = fuelType;
	}

	public String getRcBook() {
		return rcBook;
	}

	public void setRcBook(String rcBook) {
		this.rcBook = rcBook;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public BuildingMaster getBuildingMaster() {
		return buildingMaster;
	}

	public void setBuildingMaster(BuildingMaster buildingMaster) {
		this.buildingMaster = buildingMaster;
	}

	public EmployeeVehicleMaster(Integer id, String empId, String empName, BaseLocation baseLocation,
			BuildingMaster buildingMaster, Long mobile, String email, String vehicleRegNo, String model, String make,
			NoOfWheelsMaster noOfWheels, boolean reservedParking, String pucFileName, LocalDate pucValidTo,
			String insuranceFileName, LocalDate insuranceValidTo, String licenseFileName, LocalDate licenseValidTo,
			String rcBook, LocalDate created, LocalDate modified, List<VehicleTypeMaster> vehicleType,
			List<FuelType> fuelType, Boolean isDeleted) {
		super();
		this.id = id;
		this.empId = empId;
		this.empName = empName;
		this.baseLocation = baseLocation;
		this.buildingMaster = buildingMaster;
		this.mobile = mobile;
		this.email = email;
		this.vehicleRegNo = vehicleRegNo;
		this.model = model;
		this.make = make;
		this.noOfWheels = noOfWheels;
		this.reservedParking = reservedParking;
		this.pucFileName = pucFileName;
		this.pucValidTo = pucValidTo;
		this.insuranceFileName = insuranceFileName;
		this.insuranceValidTo = insuranceValidTo;
		this.licenseFileName = licenseFileName;
		this.licenseValidTo = licenseValidTo;
		this.rcBook = rcBook;
		this.created = created;
		this.modified = modified;
		this.vehicleType = vehicleType;
		this.fuelType = fuelType;
		this.isDeleted = isDeleted;
	}

	public EmployeeVehicleMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
}
