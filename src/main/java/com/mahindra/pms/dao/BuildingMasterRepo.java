package com.mahindra.pms.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;

public interface BuildingMasterRepo extends CrudRepository<BuildingMaster, Integer> {

	@Query
	List<BuildingMaster> findByBaseLocation(BaseLocation baseLocation);
}
