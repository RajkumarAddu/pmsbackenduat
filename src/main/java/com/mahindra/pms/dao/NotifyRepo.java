package com.mahindra.pms.dao;

import org.springframework.data.repository.CrudRepository;
import com.mahindra.pms.model.Notify;

public interface NotifyRepo extends CrudRepository<Notify,Integer> {
}
