package com.mahindra.pms.dao;

import org.springframework.data.repository.CrudRepository;

import com.mahindra.pms.model.BaseLocation;

public interface BaseLocationRepo extends CrudRepository<BaseLocation, Integer> {
}
