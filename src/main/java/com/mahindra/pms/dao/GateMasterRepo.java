package com.mahindra.pms.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.GateMaster;

public interface GateMasterRepo extends CrudRepository<GateMaster, Integer> {

	@Query
	public Optional<GateMaster> findByGateNo(Integer gateNo);
	
	@Query
	public List<GateMaster> findByBaseLocationId(BaseLocation baseLocation);
	
	
}
