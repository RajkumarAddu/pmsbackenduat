package com.mahindra.pms.dao;


import org.springframework.data.repository.CrudRepository;

import com.mahindra.pms.model.VehicleTypeMaster;


public interface VehicleTypeMasterRepo extends CrudRepository<VehicleTypeMaster, Integer> {
}
