package com.mahindra.pms.dao;

import org.springframework.data.repository.CrudRepository;

import com.mahindra.pms.model.NoOfWheelsMaster;

public interface NoOfWheelsMasterRepo extends CrudRepository<NoOfWheelsMaster, Integer> {

}
