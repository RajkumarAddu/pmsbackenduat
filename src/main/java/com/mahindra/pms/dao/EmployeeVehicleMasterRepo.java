package com.mahindra.pms.dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.mahindra.pms.model.EmployeeVehicleMaster;

public interface EmployeeVehicleMasterRepo extends CrudRepository<EmployeeVehicleMaster, Integer> {
	
	
	@Modifying
	@Query(value = "UPDATE EmployeeVehicleMaster e SET  e.isDeleted = true  Where e.empId= :empId and e.vehicleRegNo=:vehicleRegNo")
	@Transactional
	void softDelete(String empId,String vehicleRegNo);

	
	@Query(value="from EmployeeVehicleMaster e where  e.isDeleted = false and  e.vehicleRegNo=:vehicleRegNo and e.empId= :empId ")
	public Optional<EmployeeVehicleMaster> findByVehicleRegNoAndEmpId(String vehicleRegNo,
			                                                          String empId);
	
	@Query(value = "from EmployeeVehicleMaster e where  e.isDeleted = false and e.vehicleRegNo=:vehicleRegNo")
	public EmployeeVehicleMaster findByVehicleRegNo(String vehicleRegNo);
	
	@Query(value = "from EmployeeVehicleMaster e where  e.isDeleted = false and e.empId= :empId")
	List<EmployeeVehicleMaster> findByEmpId(String empId);
	
}
