package com.mahindra.pms.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.NoOfWheelsMaster;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.VisitorVehicleDetails;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface VisitorVehicleDetailsRepo extends CrudRepository<VisitorVehicleDetails, Integer> {

	@Query
	List<VisitorVehicleDetails> findByBaseLocationAndBuildingMasterAndIsExit(BaseLocation baseLocation,
			BuildingMaster buildingMaster, boolean isExit);

	@Query
	List<VisitorVehicleDetails> findByVehicleRegNo(String vehicleRegNo);

	@Query
	List<VisitorVehicleDetails> findByIsExit(boolean isExit);

	@Query("from VisitorVehicleDetails where vehicleRegNo=:vehicleRegNo And isExit=false")
	public Optional<VisitorVehicleDetails> findByVehicleRegNoAndIsExitFalse(String vehicleRegNo);

	@Query("from VisitorVehicleDetails where vehicleRegNo=:vehicleRegNo And isExit=true")
	public Optional<VisitorVehicleDetails> findByVehicleRegNoAndIsExitTrue(String vehicleRegNo);

	@Query("from VisitorVehicleDetails where created=:date")
	public List<VisitorVehicleDetails> findAllTodaysVisitor(LocalDate date);

	@Query("from VisitorVehicleDetails WHERE noOfWheels=:noOfWheels and reservedParkingStation=:reservedParking and created BETWEEN :startDate AND :endDate")
	public List<VisitorVehicleDetails> findAllByStartDateAndEndDate(NoOfWheelsMaster noOfWheels,
			ParkingStationMaster reservedParking, LocalDate startDate, LocalDate endDate);

	@Query("from VisitorVehicleDetails WHERE  isExit=false and noOfWheels=:noOfWheels and reservedParkingStation=:reservedParking and created BETWEEN :startDate AND :endDate")
	public List<VisitorVehicleDetails> findByStartDateAndEndDateAndIsExitFalse(NoOfWheelsMaster noOfWheels,
			ParkingStationMaster reservedParking, LocalDate startDate, LocalDate endDate);

	@Query("from VisitorVehicleDetails where baseLocation=:baseLocation and reservedParkingStation=:psm  and isExit=false")
	public List<VisitorVehicleDetails> findByBaseLocationAndPsIdAndDate(BaseLocation baseLocation,
			ParkingStationMaster psm);
	

	/*
	 * 
	 */
	@Query("from VisitorVehicleDetails WHERE noOfWheels=:noOfWheels and created BETWEEN :startDate AND :endDate")
	public List<VisitorVehicleDetails> findAllByOnlyStartDateAndEndDate(NoOfWheelsMaster noOfWheels,
			LocalDate startDate, LocalDate endDate);

	@Query("from VisitorVehicleDetails WHERE  isExit=false and noOfWheels=:noOfWheels and created BETWEEN :startDate AND :endDate")
	public List<VisitorVehicleDetails> findByOnlyStartDateAndEndDateAndIsExitFalse(NoOfWheelsMaster noOfWheels,
			LocalDate startDate, LocalDate endDate);
	
	/*
	 * For count in premises
	 */
	@Query("SELECT COUNT(u) FROM VisitorVehicleDetails u WHERE  u.isExit=false and u.created BETWEEN :startDate AND :endDate")
    Integer CountOfAllVisitorInPremises(LocalDate startDate,LocalDate endDate);
	
	

}
