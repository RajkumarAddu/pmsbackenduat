package com.mahindra.pms.dao;

import org.springframework.data.repository.CrudRepository;
import com.mahindra.pms.model.FuelType;

public interface FuelTypeRepo extends CrudRepository<FuelType, Integer>{
}
