package com.mahindra.pms.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VisitorVehicleDetails;

public interface VehicleInOutRepo extends CrudRepository<VehicleInOut, Integer> {

	@Query
	public List<VehicleInOut> findByEmpIdAndIsExit(String empId,Boolean isExit);
	
	@Query("from VehicleInOut where empId=:empId And vehicleRegNo=:vehicleRegNo and isExit=false")
	public Optional<VehicleInOut> findByEmpIdAndVehicleRegNoAndIsExitFalse(String empId, String vehicleRegNo);

	@Query("from VehicleInOut where empId=:empId And vehicleRegNo=:vehicleRegNo and isExit=true")
	public Optional<VehicleInOut> findByEmpIdAndVehicleRegNoAndIsExitTrue(String empId, String vehicleRegNo);

	@Query
	public Optional<VehicleInOut> findByVehicleRegNoAndEmpId(String vehicleRegNo, String empId);

	@Query("from VehicleInOut WHERE empId=:empId And created BETWEEN :startDate AND :endDate")
	public List<VehicleInOut> findByEmpIdAndStartDateAndEndDate(String empId, LocalDate startDate, LocalDate endDate);

	@Query("from  VehicleInOut WHERE parkingStaionId=:parkingStation and created BETWEEN :startDate AND :endDate")
	public List<VehicleInOut> findAllByStartDateAndEndDate(ParkingStationMaster parkingStation,LocalDate startDate,LocalDate endDate);

	@Query("from  VehicleInOut WHERE isExit=false and parkingStaionId=:parkingStation and created BETWEEN :startDate AND :endDate")
	public List<VehicleInOut> findByStartDateAndEndDateAndIsExitFalse(ParkingStationMaster parkingStation,LocalDate startDate,LocalDate endDate);

	@Query("from VehicleInOut where baseLocationId=:baseLocation and parkingStaionId=:psm  and isExit=false")
	public List<VehicleInOut> findByBaseLocationAndPsIdAndDate(BaseLocation baseLocation,ParkingStationMaster psm);

	
	@Modifying
	@Query(value = "UPDATE VehicleInOut v SET  v.parkingStaionId=:parkingStaionId Where v.id= :id")
	@Transactional
	void updatePsIdOfVehicleEntry(ParkingStationMaster parkingStaionId,Integer id);
	
	/*
	 * 
	 */
	@Query("from  VehicleInOut WHERE created BETWEEN :startDate AND :endDate")
	public List<VehicleInOut> findAllByOnlyStartDateAndEndDate(LocalDate startDate,LocalDate endDate);
	
	
	@Query("from  VehicleInOut WHERE isExit=false and created BETWEEN :startDate AND :endDate")
	public List<VehicleInOut> findByOnlyStartDateAndEndDateAndIsExitFalse(LocalDate startDate,LocalDate endDate);


	/*
	 * For count in premises
	 */
	@Query("SELECT COUNT(u) FROM VehicleInOut u WHERE  u.isExit=false and u.created BETWEEN :startDate AND :endDate")
    Integer CountOfAllVehicleInPremises(LocalDate startDate,LocalDate endDate);
	
}
