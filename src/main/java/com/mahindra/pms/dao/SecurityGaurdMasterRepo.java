package com.mahindra.pms.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.SecurityGaurdMaster;
import java.lang.String;

public interface SecurityGaurdMasterRepo extends CrudRepository<SecurityGaurdMaster,Integer> {

	List<SecurityGaurdMaster> findByEmpIdAndBaseLocatoin(String empId,BaseLocation baseLocation);

	List<SecurityGaurdMaster> findByEmpId(String empid);
}
