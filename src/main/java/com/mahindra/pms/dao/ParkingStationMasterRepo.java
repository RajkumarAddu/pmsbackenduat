package com.mahindra.pms.dao;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;

import java.util.List;

public interface ParkingStationMasterRepo extends CrudRepository<ParkingStationMaster, Integer> {

	@Query()
	Optional<ParkingStationMaster> findByPsName(String psName);

	@Query
	List<ParkingStationMaster> findByBaseLocationIdAndBuildingMaster(BaseLocation baseLocation,
			BuildingMaster buildingMaster);

	@Query
	ParkingStationMaster findByIsTwoWheelerPsAndBuildingMaster(Boolean isTwoWheels,BuildingMaster blm);

	@Query(value = "from ParkingStationMaster where isFullReserved = false")
	ParkingStationMaster findByIsFullUnReserved();

	/*
	 * @Query(value =
	 * "from ParkingStationMaster p where p.id= :id and p.baseLocationId= :baseLocationId"
	 * ) Optional<ParkingStationMaster> findByIdAndBaseLocationId(Integer
	 * id,BaseLocation baseLocationId);
	 */

	@Query(value = "from ParkingStationMaster where isFullReserved = false ")
	ParkingStationMaster findByIsFullReserved();

	@Query(value = "SELECT coalesce(max(id), 0)  from ParkingStationMaster where buildingMaster= :buildingMaster")
	int findMaxId(BuildingMaster buildingMaster);

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.availablilityForUnReserved = :avlUnResPs  Where p.id= :id and buildingMaster= :buildingMaster")
	@Transactional
	void updateUnreservedParking(Integer avlUnResPs, Integer id,BuildingMaster buildingMaster);

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.isFullUnReserved=true  Where p.id= :id")
	@Transactional
	void updateTrueIFFullUnReserved(Integer id);

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.isFullUnReserved=false  Where p.id= :id")
	@Transactional
	void updateFalseIFFullUnReserved(Integer id);

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.isFullUnReserved=true  Where p.id>=2")
	@Transactional
	void updateTrueAfterSecondIdUnReserved();

	/*
	 * after this is for reserved Parking
	 */

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.availablilityForReserved = :avlResPs  Where p.id= :id and buildingMaster= :buildingMaster")
	@Transactional
	void updateReservedParking(Integer avlResPs, Integer id,BuildingMaster buildingMaster);

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.isFullReserved=true  Where p.id= :id")
	@Transactional
	void updateTrueIFFullReserved(Integer id);

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.isFullReserved=false  Where p.id= :id")
	@Transactional
	void updateFalseIFFullReserved(Integer id);

	@Modifying
	@Query(value = "UPDATE ParkingStationMaster p SET  p.isFullReserved=true  Where p.id>=2")
	@Transactional
	void updateTrueAfterSecondIdReserved();

}
