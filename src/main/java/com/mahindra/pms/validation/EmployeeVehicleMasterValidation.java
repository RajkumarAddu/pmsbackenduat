package com.mahindra.pms.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mahindra.pms.exception.CustomValidationException;
import com.mahindra.pms.model.dto.EmployeeVehicleMasterDTO;
import com.mahindra.pms.model.dto.VisitorVehicleEntryDTO;

@Service
public class EmployeeVehicleMasterValidation {

	Logger log = LoggerFactory.getLogger(EmployeeVehicleMasterValidation.class);

	public void checkValidateEmpVMasterDTO(EmployeeVehicleMasterDTO dto) throws Exception {
		log.info("inside checkValidateEmpVMasterDTO validate service");
		if (dto.getEmpId() == null || dto.getEmpId().equals("") || dto.getEmpId().length() == 0) {
			throw new CustomValidationException("Employee id is required");
		}
		if (dto.getVehicleRegNo() == null || dto.getVehicleRegNo().equals("") || dto.getVehicleRegNo().length() == 0) {
			throw new CustomValidationException("vehicle Reg Number is required");
		}
		if (dto.getEmpName() == null || dto.getEmpName().equals("") || dto.getEmpName().length() == 0) {
			throw new CustomValidationException("Employee Name is required");
		}
		if (dto.getBaseLocation() == null || dto.getBaseLocation().getId() == null) {
			throw new CustomValidationException("BaseLocation is required");
		}
		if (dto.getEmail().equals("") || dto.getEmail().length() == 0) {
			throw new CustomValidationException("Email is required");
		}
		if (dto.getMobile() == null) {
			throw new CustomValidationException("Mobile Number is required");
		}
		if (dto.getBuildingMaster() == null || dto.getBuildingMaster().getId() == null) {
			throw new CustomValidationException("BuildingMaster is required");
		}
		if (dto.getNoOfWheels() == null || dto.getNoOfWheels().getId() == null) {
			throw new CustomValidationException("Number of wheels is required");
		}
		if (dto.getInsuranceValidTo() == null) {
			throw new CustomValidationException("Insurance Date is required");
		}

		if (dto.getFuelType() == null || dto.getFuelType().get(0).getId() == null) {
			throw new CustomValidationException("FuelType is required");
		}
		if (dto.getVehicleType() == null || dto.getVehicleType().get(0).getId() == null) {
			throw new CustomValidationException("VehicleType is required");
		}

		if (dto.getMake() == null || dto.getMake().equals("") || dto.getMake().length() == 0) {
			throw new CustomValidationException("Vehicle Make is required");
		}

		if (dto.getModel() == null || dto.getModel().equals("") || dto.getModel().length() == 0) {
			throw new CustomValidationException("Vehicle Model is required");
		}
	}

	public void checkValidateVisitorEntryDTO(VisitorVehicleEntryDTO dto) throws Exception {
		log.info("inside checkValidateVisitorEntryDTO validate service");
		if (dto.getVisitorName() == null || dto.getVisitorName().equals("") || dto.getVisitorName().length() == 0) {
			throw new CustomValidationException("VisitorName is required");
		}
		if (dto.getNoOfWheels() == null || dto.getNoOfWheels().getId() == null) {
			throw new CustomValidationException("Number of wheels is required");
		}
		if (dto.getMobile() == null) {
			throw new CustomValidationException("Mobile Number is required");
		}
		if (dto.getVehicleRegNo() == null || dto.getVehicleRegNo().equals("") || dto.getVehicleRegNo().length() == 0) {
			throw new CustomValidationException("vehicle Reg Number is required");
		}
		if (dto.getVehicleMake() == null || dto.getVehicleMake().equals("") || dto.getVehicleMake().length() == 0) {
			throw new CustomValidationException("Vehicle Make is required");
		}
		if (dto.getVisitPurpose() == null || dto.getVisitPurpose().equals("") || dto.getVisitPurpose().length() == 0) {
			throw new CustomValidationException("Visit Purpose is required");
		}
		if (dto.getDropPoint() == null || dto.getDropPoint().equals("") || dto.getDropPoint().length() == 0) {
			throw new CustomValidationException("Drop point is required");
		}
		if (dto.getBuildingMaster() == null || dto.getBuildingMaster().getId() == null) {
			throw new CustomValidationException("BuildingMaster is required");
		}
		if (dto.getBaseLocation() == null || dto.getBaseLocation().getId() == null) {
			throw new CustomValidationException("BaseLocation is required");
		}
		if (dto.getEntryGate() == null || dto.getEntryGate().getId() == null) {
			throw new CustomValidationException("Entry Gate is required");
		}
		if (dto.getCreatedBy() == null || dto.getCreatedBy().equals("") || dto.getCreatedBy().length() == 0) {
			throw new CustomValidationException("Created By is required");
		}
	}

}
