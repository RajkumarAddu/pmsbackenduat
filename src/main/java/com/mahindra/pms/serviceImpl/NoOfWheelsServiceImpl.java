package com.mahindra.pms.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mahindra.pms.dao.NoOfWheelsMasterRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.NoOfWheelsMaster;
import com.mahindra.pms.service.NoOfWheelsService;

@Service
public class NoOfWheelsServiceImpl implements NoOfWheelsService{

	@Autowired
	NoOfWheelsMasterRepo noOfWheelsMasterRepo;

	@Override
	public List<NoOfWheelsMaster> getAllWheelsData() throws Exception {
		List<NoOfWheelsMaster> list = (List<NoOfWheelsMaster>) noOfWheelsMasterRepo.findAll();
		if (list.size() != 0) {
			return list;
		} else {
			throw new DataNotFoundException("Wheels details are not Available");
		}
	}
}
