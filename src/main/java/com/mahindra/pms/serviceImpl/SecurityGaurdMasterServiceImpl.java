package com.mahindra.pms.serviceImpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mahindra.pms.dao.SecurityGaurdMasterRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.SecurityGaurdMaster;
import com.mahindra.pms.model.dto.CheckLocationResponseDTO;
import com.mahindra.pms.model.dto.SecurityGaurdMasterDTO;
import com.mahindra.pms.service.SecurityGaurdMasterService;

@Service
public class SecurityGaurdMasterServiceImpl implements SecurityGaurdMasterService {

	Logger log = LoggerFactory.getLogger(VehicleInOutServiceImpl.class);

	@Autowired
	SecurityGaurdMasterRepo securityGaurdMasterRepo;

	@Override
	public boolean checkIsLocationAvl(CheckLocationResponseDTO dto) throws Exception {
		log.info("inside checkIsLocationAvl service");
		List<SecurityGaurdMaster> sGaurdDataList = securityGaurdMasterRepo.findByEmpIdAndBaseLocatoin(dto.getEmpId(),
				dto.getBaseLocationId());
		if (sGaurdDataList.size() != 0) {
			for (SecurityGaurdMaster sfm : sGaurdDataList) {
				System.out.println("for loop " + sfm);
				Integer locationIdOfGuard = sfm.getBaseLocatoin().getId();
				if (locationIdOfGuard.equals(dto.getBaseLocationId().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public SecurityGaurdMaster registerSecurityG(SecurityGaurdMasterDTO sgmDTO)throws Exception {
		log.info("inside registerSecurity Gaurd Service");
		SecurityGaurdMaster sgm = new SecurityGaurdMaster();
		if (sgmDTO.getBaseLocatoin() != null && sgmDTO.getEmpId() != null) {
			sgm.setBaseLocatoin(sgmDTO.getBaseLocatoin());
			sgm.setAdmin(sgmDTO.isAdmin());
			sgm.setEmpId(sgmDTO.getEmpId());
			return securityGaurdMasterRepo.save(sgm);
		}else {
		 throw new DataNotFoundException("Security guard registration failed.");	
		}
	}
}