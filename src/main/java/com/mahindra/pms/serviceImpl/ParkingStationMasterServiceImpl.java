package com.mahindra.pms.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mahindra.pms.dao.ParkingStationMasterRepo;
import com.mahindra.pms.dao.VehicleInOutRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO;
import com.mahindra.pms.model.dto.ChangeParkingSlotDTO;
import com.mahindra.pms.model.dto.ParkingStationMasterResponseDTO;
import com.mahindra.pms.service.ParkingStationMasterService;

@Service
public class ParkingStationMasterServiceImpl implements ParkingStationMasterService {

	Logger log = LoggerFactory.getLogger(ParkingStationMasterServiceImpl.class);

	@Autowired
	ParkingStationMasterRepo parkingStationMasterRepo;

	@Autowired
	VehicleInOutRepo vehicleInOutRepo;

	@Override
	public List<ParkingStationMaster> fetchAllParkingStationAndSlots(BaseLocationFetchResponseDTO dto)
			throws Exception {
		log.info("inside fetchAllParkingStationAndSlots service");
		log.debug("inside fetchAllParkingStationAndSlots service");
		/*
		 * BaseLocation baseLocation = new BaseLocation();
		 * baseLocation.setId(dto.getBaseLocation().getId());
		 */
		List<ParkingStationMaster> list = parkingStationMasterRepo
				.findByBaseLocationIdAndBuildingMaster(dto.getBaseLocation(), dto.getBuildingMaster());
		if (list.size() != 0) {
			return list;
		} else {
			throw new DataNotFoundException(
					dto.getBaseLocation().getId() + " Parking stations details are not available.");
		}
	}

	@Override
	public ParkingStationMasterResponseDTO fetchParkingStationAndSlots(BaseLocationFetchResponseDTO bsLDTO)
			throws Exception {
		log.info("inside fetchParkingStationAndSlots service");
		log.debug("inside fetchParkingStationAndSlots service");
		ParkingStationMasterResponseDTO dto = new ParkingStationMasterResponseDTO();
		List<ParkingStationMaster> list = parkingStationMasterRepo
				.findByBaseLocationIdAndBuildingMaster(bsLDTO.getBaseLocation(), bsLDTO.getBuildingMaster());

		if (list.size() != 0) {
			for (ParkingStationMaster ps : list) {
				System.out.println(ps.getPsName() + " --ps details 1--" + ps.getAvailablilityForUnReserved());
				if (ps.getAvailablilityForUnReserved() != 0) {
					dto.setNoUnReservedParkingSlots(ps.getAvailablilityForUnReserved());
					dto.setUnReservedParkingStaitonName(ps.getPsName());
					break;
				}
			}
			for (ParkingStationMaster ps : list) {
				System.out.println(ps.getPsName() + " --ps details 2--" + ps.getAvailablilityForUnReserved());
				if (ps.getAvailablilityForReserved() != 0) {
					dto.setReservedParkingStaitonName(ps.getPsName());
					dto.setNoReservedParkingSlots(ps.getAvailablilityForReserved());
					break;
				}
			}
			return dto;
		} else {
			throw new DataNotFoundException("All parking stations are full.");
		}
	}

	@Override
	public String changePsSlot(ChangeParkingSlotDTO dto) throws Exception {
		log.info("inside change parking Slot service");
		Integer isVehicleTypeId = dto.getEmployeeVehicleMaster().getVehicleType().get(0).getId();
		Integer oldPsId = dto.getOldParkingStation().getId();
		Integer newPsId = dto.getNewParkingStation().getId();
		String empId = dto.getEmployeeVehicleMaster().getEmpId();
		String vehicleRegNo = dto.getEmployeeVehicleMaster().getVehicleRegNo();
		System.out.println(empId + " " + vehicleRegNo);
		Optional<VehicleInOut> vehicleData = vehicleInOutRepo.findByEmpIdAndVehicleRegNoAndIsExitFalse(empId,
				vehicleRegNo);
		Boolean isReserved = vehicleData.get().isReserved();
		System.out.println(vehicleData);
		if (!vehicleData.isPresent()) {
			throw new DataNotFoundException(vehicleData.get().getVehicleRegNo() + "  vehicle is not yet parked. ");
		} else {
			Optional<ParkingStationMaster> oldPs = parkingStationMasterRepo.findById(oldPsId);
			// Optional<ParkingStationMaster> newPs =
			// parkingStationMasterRepo.findByIdAndBaseLocationId(newPsId,dto.getBaseLocation());
			Optional<ParkingStationMaster> newPs = parkingStationMasterRepo.findById(newPsId);

			if (!newPs.isPresent()) {
				throw new DataNotFoundException("Parking station details not found.");
			}
			if (!oldPs.isPresent()) {
				throw new DataNotFoundException("Parking station details not found.");
			} else {
				Integer avlResPsCount = newPs.get().getAvailablilityForReserved();
				Integer avlUnResPsCount = newPs.get().getAvailablilityForUnReserved();
				System.out.println("oldId::" + oldPsId + "  newId:" + newPsId);
				if (isVehicleTypeId == 2 || isReserved == true) {
					log.info("inside reserved parking " + avlUnResPsCount);
					if (avlResPsCount != 0 && avlResPsCount > 0) {
						Integer vehicleInOutId = vehicleData.get().getId();
						Integer oldAvlForReserved = oldPs.get().getAvailablilityForReserved() + 1;
						if (newPs.get().getIsTwoWheelerPs()) {
							throw new DataNotFoundException("Please check this parking for only two wheels");
						} else {
							parkingStationMasterRepo.updateReservedParking(oldAvlForReserved, oldPsId,
									dto.getBuildingMaster());
							Integer newAvlForReserved = newPs.get().getAvailablilityForReserved() - 1;
							parkingStationMasterRepo.updateReservedParking(newAvlForReserved, newPsId,
									dto.getBuildingMaster());
							vehicleInOutRepo.updatePsIdOfVehicleEntry(newPs.get(), vehicleInOutId);
							return "Parking Slot changed ";
						}
					} else {
						throw new DataNotFoundException("There is no reserved parking available.");
					}
				} else {
					if (avlUnResPsCount != 0 && avlUnResPsCount > 0) {
						log.info("inside unReserved Parking " + avlUnResPsCount);
						Integer vehicleInOutId = vehicleData.get().getId();
						Integer oldAvlUnRes = oldPs.get().getAvailablilityForUnReserved() + 1;
						if (newPs.get().getIsTwoWheelerPs()) {
							throw new DataNotFoundException("Please check this parking for only two wheels");
						} else {
							parkingStationMasterRepo.updateUnreservedParking(oldAvlUnRes, oldPsId,
									dto.getBuildingMaster());
							Integer newAvlForUnReserved = newPs.get().getAvailablilityForUnReserved() - 1;
							parkingStationMasterRepo.updateUnreservedParking(newAvlForUnReserved, newPsId,
									dto.getBuildingMaster());
							vehicleInOutRepo.updatePsIdOfVehicleEntry(newPs.get(), vehicleInOutId);
							return "Parking Slot changed ";
						}
					} else {
						throw new DataNotFoundException("There is no reserved parking available.");
					}
				}

			}
		}
	}

}
