package com.mahindra.pms.serviceImpl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mahindra.pms.dao.BaseLocationRepo;
import com.mahindra.pms.dao.EmployeeVehicleMasterRepo;
import com.mahindra.pms.dao.ParkingStationMasterRepo;
import com.mahindra.pms.dao.VehicleInOutRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.exception.ResourceAlreadyExistsException;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.EmployeeVehicleMaster;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VehicleTypeMaster;
import com.mahindra.pms.model.dto.VehicleEntryDTO;
import com.mahindra.pms.model.dto.VehicleExitDTO;
import com.mahindra.pms.service.VehicleInOutService;

@Service
public class VehicleInOutServiceImpl implements VehicleInOutService {

	Logger log = LoggerFactory.getLogger(VehicleInOutServiceImpl.class);

	@Autowired
	EmployeeVehicleMasterRepo employeeVehicleMasterRepo;

	@Autowired
	VehicleInOutRepo vehicleInOutRepo;

	@Autowired
	ParkingStationMasterRepo parkingStationMasterRepo;

	@Autowired
	BaseLocationRepo baseLocationRepo;

	@Override
	public VehicleInOut vehicleInEntry(VehicleEntryDTO dto) throws Exception {
		log.info("inside vehicleEntry service");
		Optional<EmployeeVehicleMaster> empVehicleData = employeeVehicleMasterRepo
				.findByVehicleRegNoAndEmpId(dto.getVehicleRegNo(), dto.getEmpId());
		Optional<VehicleInOut> vehicleEntryData = vehicleInOutRepo
				.findByEmpIdAndVehicleRegNoAndIsExitFalse(dto.getEmpId(), dto.getVehicleRegNo());
		Optional<BaseLocation> baseLocation = baseLocationRepo.findById(dto.getBaseLocationId().getId());

		if (vehicleEntryData.isPresent()) {
			throw new ResourceAlreadyExistsException(dto.getVehicleRegNo() + " This vehicle is already parked.");
		} else {
			if (!empVehicleData.isPresent()) {
				throw new DataNotFoundException("Details for this EmpId:" + dto.getEmpId() + " vehicle are not found.");
			} else {
				// Entry gate Master
				// Integer vehicleEntryBuildingId =
				// dto.getEntryGate().getBuildingMaster().getId();
				Integer vehicleEntryBuildingId = dto.getBuildingMaster().getId();
				Integer vehicleEntryBsLocationId = dto.getBaseLocationId().getId();
				BuildingMaster bldMaster = empVehicleData.get().getBuildingMaster();

				if (bldMaster != null) {
					Integer empBuildingId = empVehicleData.get().getBuildingMaster().getId();
					Integer empBaseLocationId = empVehicleData.get().getBaseLocation().getId();
					System.out.println(vehicleEntryBuildingId + " -building ID- " + empBuildingId);
					if (vehicleEntryBsLocationId == empBaseLocationId) {
						if (vehicleEntryBuildingId == empBuildingId) {
							LocalDate insuranceValidTo = empVehicleData.get().getInsuranceValidTo();
							LocalDate currenDate = LocalDate.now();
							System.out.println("insDate " + insuranceValidTo + " curr " + currenDate);
							if (insuranceValidTo != null) {
								System.out.println("inside insu");
								if (insuranceValidTo.isEqual(currenDate) || insuranceValidTo.isAfter(currenDate)) {
									Integer isTwoWheelsId = empVehicleData.get().getNoOfWheels().getId();
									if (isTwoWheelsId == 1) {
										EmployeeVehicleMaster empVData = empVehicleData.get();
										VehicleInOut vehicleEntry = new VehicleInOut();
										BeanUtils.copyProperties(dto, vehicleEntry);
										vehicleEntry.setEmpId(empVData.getEmpId());
										vehicleEntry.setVehicleRegNo(empVData.getVehicleRegNo());
										vehicleEntry.setVehicleId(empVData);
										vehicleEntry.setExit(false);
										vehicleEntry.setBaseLocationId(baseLocation.get());
										vehicleEntry.setEntryGate(dto.getEntryGate());
										System.out.println("inside two wheels");
										/*
										 * parking booking
										 */
										ParkingStationMaster bookParkingTwoWheelsSlots = bookParkingTwoWheelsSlots(
												dto.getBaseLocationId(), bldMaster);
										vehicleEntry.setParkingStaionId(bookParkingTwoWheelsSlots);
										return vehicleInOutRepo.save(vehicleEntry);
									} else {
										EmployeeVehicleMaster empVData = empVehicleData.get();
										VehicleInOut vehicleEntry = new VehicleInOut();
										BeanUtils.copyProperties(dto, vehicleEntry);
										vehicleEntry.setEmpId(empVData.getEmpId());
										vehicleEntry.setVehicleRegNo(empVData.getVehicleRegNo());
										vehicleEntry.setVehicleId(empVData);
										vehicleEntry.setExit(false);
										vehicleEntry.setBaseLocationId(baseLocation.get());
										vehicleEntry.setEntryGate(dto.getEntryGate());
										/*
										 * check vehicle type
										 */
										List<VehicleTypeMaster> vehicleType = empVData.getVehicleType();
										Integer isVehicleTypeId = vehicleType.get(0).getId();
										ParkingStationMaster bookParkingSlots = new ParkingStationMaster();
										if (isVehicleTypeId == 2 || empVData.isReservedParking() == true) {
											// and is Reserved parking true then
											System.out.println("inside Admin vehicle");
											bookParkingSlots = bookParkingForReservedSlots(dto.getBaseLocationId(),
													bldMaster);
											vehicleEntry.setParkingStaionId(bookParkingSlots);
											vehicleEntry.setReserved(true);
											return vehicleInOutRepo.save(vehicleEntry);
										} else {
											System.out.println("inside Personel vehicle");
											bookParkingSlots = bookParkingForUnReservedSlots(dto.getBaseLocationId(),
													bldMaster);
											vehicleEntry.setParkingStaionId(bookParkingSlots);
											return vehicleInOutRepo.save(vehicleEntry);
										}
									}
								} else {
									throw new DataNotFoundException(
											"Your vehicle insurance is expired On " + insuranceValidTo);

								}
							} else {
								throw new DataNotFoundException("Your vehicle insurance date is not available");

							}
						} else {
							throw new DataNotFoundException("You are only allowed to park in "
									+ empVehicleData.get().getBuildingMaster().getBuildingName() + " building");
						}
					} else {
						throw new DataNotFoundException("Your base location is different.");
					}
				} else {
					throw new DataNotFoundException("Please update your building details from edit vehicles");
				}
			} //
		}
	}

	public ParkingStationMaster bookParkingTwoWheelsSlots(BaseLocation bsl, BuildingMaster bm)
			throws DataNotFoundException {
		log.info("inside bookParkingTwoWheelsSlots service ");
		ParkingStationMaster psData = parkingStationMasterRepo.findByIsTwoWheelerPsAndBuildingMaster(true, bm);
		ParkingStationMaster psmData = new ParkingStationMaster();
		if (psData != null) {
			if (psData.getAvailablilityForUnReserved() != 0 && psData.getBaseLocationId().getId() == bsl.getId()) {
				Integer id = psData.getId();
				Integer avlTwoWheelsPs = psData.getAvailablilityForUnReserved() - 1;
				parkingStationMasterRepo.updateUnreservedParking(avlTwoWheelsPs, id, bm);
				Optional<ParkingStationMaster> psData1 = parkingStationMasterRepo.findById(id);
				psmData = psData1.get();
				return psmData;
			} else {
				throw new DataNotFoundException("Two Wheeler Parking is Full");
			}
		} else {
			throw new DataNotFoundException("Two Wheeler Parking is not Available");
		}
	}

	public ParkingStationMaster bookParkingForUnReservedSlots(BaseLocation bsl, BuildingMaster bm)
			throws DataNotFoundException {
		log.info("inside UnReserved Parking Booking service");
		// Iterable<ParkingStationMaster> psData = parkingStationMasterRepo.findAll();
		List<ParkingStationMaster> psData = parkingStationMasterRepo.findByBaseLocationIdAndBuildingMaster(bsl, bm);
		System.out.println("list::" + psData.size());
		Optional<ParkingStationMaster> getPSData1, getPSData2 = null;
		ParkingStationMaster psmData = new ParkingStationMaster();
		for (ParkingStationMaster psm : psData) {
			System.out.println("ps round 1 " + psm.getPsName());
			if (psm.getIsTwoWheelerPs() == false) {
				Integer id = psm.getId();
				Integer maxId = parkingStationMasterRepo.findMaxId(bm);
				System.out.println("maxId" + maxId);
				if (psm.getAvailablilityForUnReserved() == 0 && id == maxId) {
					throw new DataNotFoundException("parking is full");
				} else if (psm.getAvailablilityForUnReserved() != 0) {
					System.out.println("inside z++");
					getPSData2 = parkingStationMasterRepo.findById(id);
					Integer avlUnResPs = getPSData2.get().getAvailablilityForUnReserved() - 1;
					parkingStationMasterRepo.updateUnreservedParking(avlUnResPs, id, bm);
					Optional<ParkingStationMaster> newPSData = parkingStationMasterRepo.findById(psm.getId());
					psmData = newPSData.get();
					return psmData;
				}

			}
		}
		return psmData;
	}

	public ParkingStationMaster bookParkingForReservedSlots(BaseLocation bsl, BuildingMaster bm)
			throws DataNotFoundException {
		log.info("inside Reserved Parking Booking service");
		List<ParkingStationMaster> psData = parkingStationMasterRepo.findByBaseLocationIdAndBuildingMaster(bsl, bm);
		Optional<ParkingStationMaster> getPSData1, getPSData2 = null;
		ParkingStationMaster psmData = new ParkingStationMaster();
		for (ParkingStationMaster psm : psData) {
			if (psm.getIsTwoWheelerPs() == false) {
				Integer id = psm.getId();
				Integer maxId = parkingStationMasterRepo.findMaxId(bm);
				System.out.println("maxId" + maxId + " ps name " + psm.getPsName());
				System.out.println("round 1 --");
				if (psm.getAvailablilityForReserved() == 0 && id == maxId) {
					throw new DataNotFoundException("parking is full");
				} else if (psm.getAvailablilityForReserved() != 0) {
					System.out.println("inside re++");
					getPSData2 = parkingStationMasterRepo.findById(id);
					Integer avlResPs = getPSData2.get().getAvailablilityForReserved() - 1;
					parkingStationMasterRepo.updateReservedParking(avlResPs, id, bm);
					System.out.println("id " + id + " avl" + avlResPs);
					Optional<ParkingStationMaster> newPSData = parkingStationMasterRepo.findById(psm.getId());
					psmData = newPSData.get();
					return psmData;
				}

			}
		}
		return psmData;
	}

	@Override
	public VehicleInOut vehicleExit(VehicleExitDTO dto) throws Exception {
		log.info("inside vehicleExit service");
		Optional<EmployeeVehicleMaster> empVehicleData = employeeVehicleMasterRepo
				.findByVehicleRegNoAndEmpId(dto.getVehicleRegNo(), dto.getEmpId());
		Optional<VehicleInOut> vehicleExitData = vehicleInOutRepo
				.findByEmpIdAndVehicleRegNoAndIsExitFalse(dto.getEmpId(), dto.getVehicleRegNo());
		if (!vehicleExitData.isPresent()) {
			throw new DataNotFoundException(dto.getVehicleRegNo() + " This vehicle has not yet entered.");
		} else {
			if (!empVehicleData.isPresent()) {
				throw new DataNotFoundException("Details for this EmpId:" + dto.getEmpId() + " vehicle are not found.");
			} else {

				Integer isTwoWheelsId = empVehicleData.get().getNoOfWheels().getId();
				if (isTwoWheelsId == 1) {
					VehicleInOut vehicleExit = vehicleExitData.get();
					vehicleExit.setExitDateTime(dto.getExitDateTime());
					vehicleExit.setExitDriverName(dto.getExitDriverName());
					vehicleExit.setExitGate(dto.getExitGate());
					vehicleExit.setExitKm(dto.getExitKm());
					vehicleExit.setUpdatedBy(dto.getUpdatedBy());
					vehicleExit.setExit(true);
					BuildingMaster bm = vehicleExit.getBuildingMaster();
					// for Parking release
					ParkingStationMaster psData = vehicleExit.getParkingStaionId();
					VehicleInOut exit = new VehicleInOut();
					Integer psId = psData.getId();
					log.info("vehicle Exit for Twoo");
					System.out.println("vehicle Exit for Twoo");
					ParkingStationMaster psForTwo = parkingStationMasterRepo.findByIsTwoWheelerPsAndBuildingMaster(true,
							bm);
					Integer avlUnResPs = psForTwo.getAvailablilityForUnReserved() + 1;
					parkingStationMasterRepo.updateUnreservedParking(avlUnResPs, psId, bm);
					// save exit data
					exit = vehicleInOutRepo.save(vehicleExit);
					Optional<ParkingStationMaster> newPSData = parkingStationMasterRepo.findById(psId);
					exit.setParkingStaionId(newPSData.get());
					return exit;
				} else {
					VehicleInOut vehicleExit = vehicleExitData.get();
					vehicleExit.setExitDateTime(dto.getExitDateTime());
					vehicleExit.setExitDriverName(dto.getExitDriverName());
					vehicleExit.setExitGate(dto.getExitGate());
					vehicleExit.setExitKm(dto.getExitKm());
					vehicleExit.setUpdatedBy(dto.getUpdatedBy());
					vehicleExit.setExit(true);
					BuildingMaster bm = vehicleExit.getBuildingMaster();
					// for Parking release
					ParkingStationMaster parkingData = vehicleExit.getParkingStaionId();
					VehicleInOut exit = new VehicleInOut();
					Integer psId = parkingData.getId();
					Optional<ParkingStationMaster> psData1 = parkingStationMasterRepo.findById(psId);
					ParkingStationMaster psData = psData1.get();
					Integer isVehicleTypeId = empVehicleData.get().getVehicleType().get(0).getId();
					if (isVehicleTypeId == 2 || empVehicleData.get().isReservedParking() == true) {
						log.info("vehicle Exit for Admin");
						Integer avlResPs = psData.getAvailablilityForReserved() + 1;
						parkingStationMasterRepo.updateReservedParking(avlResPs, psId, bm);
						// save exit data
						exit = vehicleInOutRepo.save(vehicleExit);
						Optional<ParkingStationMaster> newPSData = parkingStationMasterRepo.findById(psId);
						// Integer availablilityForReserved =
						// newPSData.get().getAvailablilityForReserved();
						Integer newId = newPSData.get().getId();
						/*
						 * if (availablilityForReserved != 0 && newId == 1) {
						 * parkingStationMasterRepo.updateFalseIFFullReserved(1);
						 * parkingStationMasterRepo.updateTrueAfterSecondIdReserved(); }
						 */
						return exit;

					} else {
						log.info("vehicle Exit for personel");
						Integer avlUnResPs = psData.getAvailablilityForUnReserved() + 1;
						parkingStationMasterRepo.updateUnreservedParking(avlUnResPs, psId, bm);
						// save exit data
						exit = vehicleInOutRepo.save(vehicleExit);
						Optional<ParkingStationMaster> newPSData = parkingStationMasterRepo.findById(psId);
						// Integer availablilityForUnReserved =
						// newPSData.get().getAvailablilityForUnReserved();
						Integer newId = newPSData.get().getId();
						/*
						 * if (availablilityForUnReserved != 0 && newId == 1) {
						 * parkingStationMasterRepo.updateFalseIFFullUnReserved(1);
						 * parkingStationMasterRepo.updateTrueAfterSecondIdUnReserved(); }
						 */
						return exit;
					}
				}

			}
		}
	}

}
