package com.mahindra.pms.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.zxing.pdf417.PDF417Reader;
import com.google.zxing.pdf417.encoder.PDF417;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.mahindra.pms.commons.ZXingHelper;
import com.mahindra.pms.dao.BaseLocationRepo;
import com.mahindra.pms.dao.EmployeeVehicleMasterRepo;
import com.mahindra.pms.dao.FuelTypeRepo;
import com.mahindra.pms.dao.VehicleInOutRepo;
import com.mahindra.pms.dao.VehicleTypeMasterRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.exception.FileStorageException;
import com.mahindra.pms.exception.ResourceAlreadyExistsException;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.EmployeeVehicleMaster;
import com.mahindra.pms.model.FuelType;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VehicleTypeMaster;
import com.mahindra.pms.model.dto.EmployeeFetchDetailsDTO;
import com.mahindra.pms.model.dto.EmployeeVRegNoFetchDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleListDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleMasterDTO;
import com.mahindra.pms.model.dto.ResponseEmployeeVRegNoFetchDTO;
import com.mahindra.pms.model.dto.ResponseOfEmpVRegistration;
import com.mahindra.pms.service.EmployeeVehicleMasterService;

import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;

@Service
public class EmployeeVehicleMasterServiceImpl implements EmployeeVehicleMasterService {

	Logger log = LoggerFactory.getLogger(EmployeeVehicleMasterServiceImpl.class);

	@Autowired
	EmployeeVehicleMasterRepo employeeVehicleMasterRepo;

	@Autowired
	VehicleTypeMasterRepo vehicleTypeMasterRepo;

	@Autowired
	VehicleInOutRepo vehicleInOutRepo;

	@Autowired
	FuelTypeRepo fuelTypeRepo;

	@Autowired
	BaseLocationRepo baseLocationRepo;

	private final Path root = Paths.get("uploads");

	/*
	 * Method for Saving Employee Vehicle Details
	 */
	@Override
	public ResponseOfEmpVRegistration saveEmpVM(EmployeeVehicleMasterDTO dto) throws Exception {
		log.info("Inside save EmployeeVehicle Registration service");
		log.debug("Inside save EmployeeVehicle Registration service");
		System.out.println("d " + dto.getBaseLocation().getId());

		EmployeeVehicleMaster findByEmpId = employeeVehicleMasterRepo.findByVehicleRegNo(dto.getVehicleRegNo());
		if (findByEmpId != null) {
			throw new ResourceAlreadyExistsException(dto.getVehicleRegNo() + " This vehicle is already registered.");
		} else {
			EmployeeVehicleMaster map = new EmployeeVehicleMaster();
			VehicleTypeMaster vehicleTypeMaster = new VehicleTypeMaster();
			VehicleTypeMaster vmt = new VehicleTypeMaster();
			BeanUtils.copyProperties(dto, map);
			Integer isVTypeId = map.getVehicleType().get(0).getId();
			if (isVTypeId == 2) {
				map.setReservedParking(true);
			}
			/*
			 * file names
			 */
			if (dto.getPucFileName() != null) {
				map.setPucFileName(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_PucFile");
				Path targetLocation = this.root.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_PucFile");
				uploadFiles1(map, dto.getPucFileName(), targetLocation);
			}
			if (dto.getInsuranceFileName() != null) {
				map.setInsuranceFileName(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_InsuranceFile");
				Path targetLocation = this.root
						.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_InsuranceFile");
				uploadFiles1(map, dto.getInsuranceFileName(), targetLocation);
			}
			if (dto.getLicenseFileName() != null) {
				map.setLicenseFileName(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_LicenceFile");
				Path targetLocation = this.root.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_LicenceFile");
				uploadFiles1(map, dto.getLicenseFileName(), targetLocation);
			}
			if (dto.getRcBook() != null) {
				map.setRcBook(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_RCBookFile");
				Path targetLocation = this.root.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_RCBookFile");
				uploadFiles1(map, dto.getRcBook(), targetLocation);
			}
			EmployeeVehicleMaster saveData = employeeVehicleMasterRepo.save(map);
			List<VehicleTypeMaster> vehicleType = saveData.getVehicleType();
			List<VehicleTypeMaster> list = new ArrayList<VehicleTypeMaster>();
			for (VehicleTypeMaster vtm : vehicleType) {
				Integer id = vtm.getId();
				Optional<VehicleTypeMaster> opt = vehicleTypeMasterRepo.findById(id);
				vehicleTypeMaster = opt.get();
			}
			vmt.setVehicleType(vehicleTypeMaster.getVehicleType());
			list.add(vehicleTypeMaster);
			saveData.setVehicleType(list);
			// generateBarcode(dto, response);
			// BufferedImage generateQRCodeImage = generateQRCodeImage(dto);
			return generateQRCodeImage(dto);
		}
	}

	/*
	 * Upload File Service
	 */
	public String uploadFiles(MultipartFile file, EmployeeVehicleMaster map, String s) {
		try {
			log.info("inside upload file service");
			log.debug("inside upload file service");
			if (root == null) {
				Files.createDirectory(root);
			}
			Path targetLocation = this.root
					.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_" + file.getOriginalFilename());
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException ex) {
			log.error("Exception occured in File Uploading", ex);
			throw new FileStorageException("Could not store file " + file + ". Please try again!", ex);
		}
		return file.getOriginalFilename();
	}

	/*
	 * Upload File Service
	 */
	public void uploadFiles1(EmployeeVehicleMaster map, String s, Path targetLocation) {
		try {
			log.info("inside upload file service");
			log.debug("inside upload file service");
			if (root == null) {
				Files.createDirectory(root);
			}
			// Path targetLocation = this.root.resolve(map.getEmpId() + "_" +
			// map.getVehicleRegNo() + "_");
			File outputFile = new File(targetLocation + "");
			// decode the string and write to file
			byte[] decodedBytes = Base64.getDecoder().decode(s);
			FileUtils.writeByteArrayToFile(outputFile, decodedBytes);
			// Files.copy(((MultipartFile) outputFile).getInputStream(), targetLocation,
			// StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException ex) {
			log.error("Exception occured in File Uploading", ex);
			throw new FileStorageException("Could not store file " + ". Please try again!", ex);
		}
	}

	/*
	 * preview Image
	 */
	@Override
	public EmployeeVehicleMaster previewImage(EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("inside preview Image service");
		log.debug("inside preview Image service");
		Optional<EmployeeVehicleMaster> empData = employeeVehicleMasterRepo
				.findByVehicleRegNoAndEmpId(dto.getVehicleRegNo(), dto.getEmpId());
		if (!empData.isPresent()) {
			throw new DataNotFoundException(dto.getEmpId() + " this employee data is not available");
		} else {
			EmployeeVehicleMaster empData1 = empData.get();
			if (empData1.getPucFileName() != null) {
				String pucFile = getFileIntoString(empData1.getPucFileName());
				empData1.setPucFileName(pucFile);
			}
			if (empData1.getLicenseFileName() != null) {
				String licenceFile = getFileIntoString(empData1.getLicenseFileName());
				empData1.setLicenseFileName(licenceFile);
			}
			if (empData1.getInsuranceFileName() != null) {
				String insuranceFile = getFileIntoString(empData1.getInsuranceFileName());
				empData1.setInsuranceFileName(insuranceFile);
			}
			if (empData1.getRcBook() != null) {
				String rcFile = getFileIntoString(empData1.getRcBook());
				empData1.setRcBook(rcFile);
			}
			return empData.get();
		}
	}

	/*
	 * get image into String
	 */
	public String getFileIntoString(String fileName) throws Exception {
		log.info("inside readFileService");
		try {
			InputStream input = new FileInputStream("uploads/" + fileName);
			System.out.println("inp str " + input.toString());
			byte[] bytes = IOUtils.toByteArray(input);
			String base64String = Base64.getEncoder().encodeToString(bytes);
			return base64String;
		} catch (Exception e) {
			return null;
		}

	}

	@Bean
	public HttpMessageConverter<BufferedImage> createImageHttpMessageConverter() {
		return new BufferedImageHttpMessageConverter();
	}

	/*
	 * Generate QRCode
	 */
	@SuppressWarnings("resource")
	public ResponseOfEmpVRegistration generateQRCodeImage(EmployeeVehicleMasterDTO dto) throws Exception {
		log.info("inside generateQRCodeImage Service");
		log.debug("inside generateQRCodeImage Service");
		System.out.println("inside Qr Code Generate");
		ResponseOfEmpVRegistration resEVReg = new ResponseOfEmpVRegistration();

		String empId_VRegNo = dto.getEmpId() + "_" + dto.getVehicleRegNo();
		ByteArrayOutputStream stream = QRCode.from(empId_VRegNo).withSize(250, 250).stream();
		String imageToBase64 = Base64.getEncoder().encodeToString(stream.toByteArray());
		resEVReg.setQrCodeImageString(imageToBase64);
		System.out.println(imageToBase64);
		/*
		 * for byteArray to pdf
		 */

		File file = QRCode.from(empId_VRegNo).to(ImageType.JPG).withSize(350, 350).file();
		String outputFile = "QrCode.pdf";
		FileOutputStream fos = new FileOutputStream(outputFile);
		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document, fos);

		Image image = Image.getInstance(file.getAbsolutePath());
		writer.open();
		document.open();
		document.add(image);
		document.close();
		writer.close();
		System.out.println(writer.getPageNumber() + "  --->");

		InputStream input = new FileInputStream(outputFile);
		byte[] bytes = IOUtils.toByteArray(input);
		String pdfTobase64 = Base64.getEncoder().encodeToString(bytes);
		System.out.println(pdfTobase64);
		resEVReg.setQrCodePdfString(pdfTobase64);

		/*
		 * mail sending service call
		 */
		// FileInputStream fileInputStream = new FileInputStream("output.pdf");
		Optional<BaseLocation> bsLocation = baseLocationRepo.findById(dto.getBaseLocation().getId());
		if (!bsLocation.isPresent()) {
			throw new DataNotFoundException("BaseLocation details is not available");
		}
		dto.setBaseLocation(bsLocation.get());
		File pdf = new File("QrCode.pdf");
		CommonMail mail = new CommonMail();
		mail.sendDetails(dto, pdf);

		return resEVReg;
	}

	@Override
	public ResponseOfEmpVRegistration generateOnlyQRCodeImage(EmployeeVehicleMasterDTO dto) throws Exception {
		log.info("inside generateOnlyQRCodeImage Service");
		log.debug("inside generateOnlyQRCodeImage Service");
		System.out.println("inside Qr Code Generate");
		ResponseOfEmpVRegistration resEVReg = new ResponseOfEmpVRegistration();
		if (dto.getEmpId() != null && dto.getVehicleRegNo() != null) {
			String empId_VRegNo = dto.getEmpId() + "_" + dto.getVehicleRegNo();
			ByteArrayOutputStream stream = QRCode.from(empId_VRegNo).withSize(250, 250).stream();
			String imageToBase64 = Base64.getEncoder().encodeToString(stream.toByteArray());
			resEVReg.setQrCodeImageString(imageToBase64);
			System.out.println(imageToBase64);
			/*
			 * for byteArray to pdf
			 */

			File file = QRCode.from(empId_VRegNo).to(ImageType.JPG).withSize(350, 350).file();
			String outputFile = "QrCode.pdf";
			FileOutputStream fos = new FileOutputStream(outputFile);
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document, fos);

			Image image = Image.getInstance(file.getAbsolutePath());
			writer.open();
			document.open();
			document.add(image);
			document.close();
			writer.close();
			System.out.println(writer.getPageNumber() + "  --->");
			InputStream input = new FileInputStream(outputFile);
			byte[] bytes = IOUtils.toByteArray(input);
			String pdfTobase64 = Base64.getEncoder().encodeToString(bytes);
			System.out.println(pdfTobase64);
			resEVReg.setQrCodePdfString(pdfTobase64);
			return resEVReg;
		}else {
			throw new DataNotFoundException("Plese enter proper vehicle Number and employeeId");
		}
	}

	/*
	 * generateBarcode service
	 */
	public void generateBarcode(EmployeeVehicleMasterDTO dto, HttpServletResponse response) throws Exception {
		log.info("inside generateBarcode service ");
		String empId_VRegNo = dto.getEmpId() + "_" + dto.getVehicleRegNo();
		// response.setContentType("image/png");
		OutputStream outputStream = response.getOutputStream();
		outputStream.write(ZXingHelper.getBarCodeImage(empId_VRegNo, 200, 200));
		outputStream.flush();
		outputStream.close();
	}

	/*
	 * Method for get EmployeeVehicleData
	 */
	@Override
	public EmployeeVehicleMaster getEmpVMById(EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Inside get EmployeeVehicle  Service");
		log.debug("Inside get EmployeeVehicle Service");
		EmployeeVehicleMaster employeeVehicleMaster;
		Optional<EmployeeVehicleMaster> empData = employeeVehicleMasterRepo
				.findByVehicleRegNoAndEmpId(dto.getVehicleRegNo(), dto.getEmpId());
		if (!empData.isPresent()) {
			throw new DataNotFoundException(dto.getEmpId() + ": employee vehicle details not found.");
		} else {
			Integer baseLocationId = dto.getBaseLocation().getId();
			Optional<BaseLocation> sGData = baseLocationRepo.findById(baseLocationId);
			String baseLocationOfSG = sGData.get().getLocationName();
			String baseLocationOfEmp = empData.get().getBaseLocation().getLocationName();
			System.out.println(baseLocationOfSG + "------" + baseLocationOfEmp);
			if (baseLocationOfSG.equalsIgnoreCase(baseLocationOfEmp)) {
				employeeVehicleMaster = empData.get();
				return employeeVehicleMaster;
			} else {
				throw new DataNotFoundException(
						baseLocationOfSG + " is not the base location of this employee " + dto.getEmpId());
			}
		}
	}

	/*
	 * upload Employee vehicle Details
	 */
	@Override
	public EmployeeVehicleMaster updateEmpVM(EmployeeVehicleMasterDTO dto) throws Exception {
		log.info("Inside Update EmployeeVehicle Service");
		log.debug("Inside Update Employee Vehicle Service");

		EmployeeVehicleMaster emp, updateData = new EmployeeVehicleMaster();
		Optional<EmployeeVehicleMaster> empData = employeeVehicleMasterRepo
				.findByVehicleRegNoAndEmpId(dto.getVehicleRegNo(), dto.getEmpId());
		if (!empData.isPresent()) {
			throw new DataNotFoundException("Vehicle details for employee:" + dto.getEmpId() + " not found");
		} else {
			emp = empData.get();
			EmployeeVehicleMaster map = new EmployeeVehicleMaster();
			BeanUtils.copyProperties(dto, map);
			map.setId(emp.getId());
			map.setPucValidTo(dto.getPucValidTo());
			map.setLicenseValidTo(dto.getLicenseValidTo());
			map.setInsuranceValidTo(dto.getInsuranceValidTo());
			map.setModified(LocalDate.now());
			System.out.println(map);
			/*
			 * file names
			 */
			if (dto.getPucFileName() != null) {
				map.setPucFileName(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_PucFile");
				Path targetLocation = this.root.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_PucFile");
				uploadFiles1(map, dto.getPucFileName(), targetLocation);
			}
			if (dto.getInsuranceFileName() != null) {
				map.setInsuranceFileName(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_InsuranceFile");
				Path targetLocation = this.root
						.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_InsuranceFile");
				uploadFiles1(map, dto.getInsuranceFileName(), targetLocation);
			}
			if (dto.getLicenseFileName() != null) {
				map.setLicenseFileName(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_LicenceFile");
				Path targetLocation = this.root.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_LicenceFile");
				uploadFiles1(map, dto.getLicenseFileName(), targetLocation);
			}
			if (dto.getRcBook() != null) {
				map.setRcBook(dto.getEmpId() + "_" + dto.getVehicleRegNo() + "_RCBookFile");
				Path targetLocation = this.root.resolve(map.getEmpId() + "_" + map.getVehicleRegNo() + "_RCBookFile");
				uploadFiles1(map, dto.getRcBook(), targetLocation);
			}
			System.out.println("-----");
			System.out.println("-----");
			return employeeVehicleMasterRepo.save(map);
		}
	}

	/*
	 * Delete Employee vehicle Details
	 */
	@Override
	public void deleteEmpVM(EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Inside Delete EmployeeVehicle Service");
		log.debug("Inside Delete EmployeeVehicle Service");
		Optional<EmployeeVehicleMaster> empData = employeeVehicleMasterRepo
				.findByVehicleRegNoAndEmpId(dto.getVehicleRegNo(), dto.getEmpId());
		Optional<VehicleInOut> vehicleInOutData = vehicleInOutRepo
				.findByEmpIdAndVehicleRegNoAndIsExitFalse(dto.getEmpId(), dto.getVehicleRegNo());
		if (!empData.isPresent()) {
			throw new DataNotFoundException(dto.getEmpId() + ": employee vehicle details are already deleted.");
		} else if (vehicleInOutData.isPresent()) {
			if (!vehicleInOutData.get().isExit()) {
				throw new DataNotFoundException(dto.getEmpId() + ": employee vehicle is parked");
			}

		} else {
			employeeVehicleMasterRepo.softDelete(dto.getEmpId(), dto.getVehicleRegNo());
		}
	}

	/*
	 * get employee Vehicle Data By vehicle RegNo
	 */
	@Override
	public EmployeeVehicleMaster getEmpVDetailsByVehicleRegNo(String regNo) throws DataNotFoundException {
		log.info("Inside getEmpVDetailsByVehicleRegNo Service");
		log.debug("Inside getEmpVDetailsByVehicleRegNo Service");
		EmployeeVehicleMaster empData = employeeVehicleMasterRepo.findByVehicleRegNo(regNo);
		if (empData == null) {
			throw new DataNotFoundException(regNo + ": employee vehicle data not found");
		} else {
			return empData;
		}
	}

	/*
	 * getEmpVehiclePresentToday By empId
	 */
	@Override
	public List<EmployeeVehicleListDTO> getEmpVehiclePresentToday(EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Inside getEmpVehiclePresentToday service");
		log.debug("Inside getEmpVehiclePresentToday service");
		List<EmployeeVehicleListDTO> list = new ArrayList<EmployeeVehicleListDTO>();
		List<EmployeeVehicleMaster> empVehiclesList = employeeVehicleMasterRepo.findByEmpId(dto.getEmpId());
		VehicleInOut vehicleInOut = new VehicleInOut();
		for (EmployeeVehicleMaster emp : empVehiclesList) {
			EmployeeVehicleListDTO empVListDTO = new EmployeeVehicleListDTO();
			String vehicleRegNo = emp.getVehicleRegNo();
			System.out.println("vehicleRegNo:" + vehicleRegNo);
			empVListDTO.setEmployeeVehicleMaster(emp);
			String empId = emp.getEmpId();
			Optional<VehicleInOut> vehicleData = vehicleInOutRepo.findByEmpIdAndVehicleRegNoAndIsExitFalse(empId,
					vehicleRegNo);
			if (vehicleData.isPresent()) {
				vehicleInOut = vehicleData.get();
				empVListDTO.setPsMaster(vehicleInOut.getParkingStaionId());
				empVListDTO.setVehicleInOut(vehicleInOut);
			}

			list.add(empVListDTO);
		}
		Collections.sort(list);

		return list;

	}

	/*
	 * For GetFuelTypeMasterData
	 */
	@Override
	public List<FuelType> getAllFuelTypeData() throws Exception {
		log.info("inside getAllFuelTypeData service");
		log.debug("inside getAllFuelTypeData service");
		List<FuelType> list = (List<FuelType>) fuelTypeRepo.findAll();
		if (list.size() == 0) {
			throw new DataNotFoundException("Fuel Data Not Found");
		} else {
			return list;
		}
	}

	/*
	 * For GetVehicleTypeMasterData
	 */
	@Override
	public List<VehicleTypeMaster> getAllVehicleTypeMaster() throws Exception {
		log.info("inside getAllVehicleTypeMaster service");
		log.debug("inside getAllVehicleTypeMaster service");
		List<VehicleTypeMaster> list = (List<VehicleTypeMaster>) vehicleTypeMasterRepo.findAll();
		if (list.size() == 0) {
			throw new DataNotFoundException("Vehicle Data Not Found");
		} else {
			return list;
		}
	}

	/*
	 * 
	 */
	@SuppressWarnings("unlikely-arg-type")
	@Override
	public List<ResponseEmployeeVRegNoFetchDTO> getAllVehicleRegNo(EmployeeVRegNoFetchDTO dto) throws Exception {
		log.info("inside getAllVehicleRegNo service");
		log.debug("inside getAllVehicleRegNo service");
		List<EmployeeVehicleMaster> listAll;
		List<VehicleInOut> vehicleList;
		List<ResponseEmployeeVRegNoFetchDTO> resList = new ArrayList<ResponseEmployeeVRegNoFetchDTO>();
		List<ResponseEmployeeVRegNoFetchDTO> resList1 = new ArrayList<ResponseEmployeeVRegNoFetchDTO>();
		if (dto.getVehicleList().equalsIgnoreCase("All")) {
			listAll = employeeVehicleMasterRepo.findByEmpId(dto.getEmpId());
			resList = new ArrayList<ResponseEmployeeVRegNoFetchDTO>();
			if (listAll.size() != 0) {
				for (EmployeeVehicleMaster emp : listAll) {
					ResponseEmployeeVRegNoFetchDTO res = new ResponseEmployeeVRegNoFetchDTO();
					res.setVehicleRegNo(emp.getVehicleRegNo());
					res.setVehicleMake(emp.getMake());
					resList.add(res);
				}
			} else {
				throw new DataNotFoundException("Vehicle details not available for this employee " + dto.getEmpId());
			}
			System.out.println(resList.size());
			return resList;
		} else if (dto.getVehicleList().equalsIgnoreCase("Exit")) {
			vehicleList = vehicleInOutRepo.findByEmpIdAndIsExit(dto.getEmpId(), false);
			if (vehicleList.size() != 0) {
				for (VehicleInOut v : vehicleList) {
					ResponseEmployeeVRegNoFetchDTO res = new ResponseEmployeeVRegNoFetchDTO();
					if (!v.getVehicleId().getIsDeleted()) {
						res.setVehicleRegNo(v.getVehicleRegNo());
						res.setVehicleMake(v.getVehicleId().getMake());
						System.out.println(v.getVehicleRegNo() + " " + v.getVehicleId().getMake());
						resList1.add(res);
					}
				}
			} else {
				throw new DataNotFoundException("Vehicle details not available for this employee " + dto.getEmpId());
			}
			return resList1;
		} else {
			listAll = employeeVehicleMasterRepo.findByEmpId(dto.getEmpId());
			System.out.println(listAll.size() + " before" + listAll);
			vehicleList = vehicleInOutRepo.findByEmpIdAndIsExit(dto.getEmpId(), false);
			if (listAll.size() != 0) {
				for (EmployeeVehicleMaster emp : listAll) {
					ResponseEmployeeVRegNoFetchDTO res = new ResponseEmployeeVRegNoFetchDTO();
					res.setVehicleRegNo(emp.getVehicleRegNo());
					res.setVehicleMake(emp.getMake());
					resList.add(res);
				}
				for (VehicleInOut v : vehicleList) {
					ResponseEmployeeVRegNoFetchDTO res = new ResponseEmployeeVRegNoFetchDTO();
					if (!v.getVehicleId().getIsDeleted()) {
						res.setVehicleRegNo(v.getVehicleRegNo());
						res.setVehicleMake(v.getVehicleId().getMake());
						System.out.println(v.getVehicleRegNo() + " " + v.getVehicleId().getMake());
						resList1.add(res);
					}
				}
			} else {
				throw new DataNotFoundException("Vehicle details not available for this employee " + dto.getEmpId());
			}
			System.out.println(resList.size());
			resList.removeAll(resList1);
			System.out.println(resList.size());
			return resList;
		}

	}

	/*
	 * given Reserved Parking of the employee
	 */
	@Override
	public EmployeeVehicleMaster givenReservedParking(EmployeeFetchDetailsDTO dto) throws Exception {
		log.info("Inside givenReservedParking  Service");
		log.debug("Inside givenReservedParking Service");
		EmployeeVehicleMaster employeeVehicleMaster = new EmployeeVehicleMaster();
		Optional<EmployeeVehicleMaster> empData = employeeVehicleMasterRepo
				.findByVehicleRegNoAndEmpId(dto.getVehicleRegNo(), dto.getEmpId());
		Optional<VehicleInOut> vehicleEntryData = vehicleInOutRepo
				.findByEmpIdAndVehicleRegNoAndIsExitFalse(dto.getEmpId(), dto.getVehicleRegNo());
		boolean isReservedOfEmp = empData.get().isReservedParking();
		if (isReservedOfEmp == false) {
			if (!empData.isPresent()) {
				throw new DataNotFoundException(dto.getEmpId() + ": employee vehicle data not found.");
			} else {
				if (!vehicleEntryData.isPresent()) {
					Integer baseLocationId = dto.getBaseLocation().getId();
					Optional<BaseLocation> sGData = baseLocationRepo.findById(baseLocationId);
					String baseLocationOfSG = sGData.get().getLocationName();
					String baseLocationOfEmp = empData.get().getBaseLocation().getLocationName();
					System.out.println(baseLocationOfSG + "------" + baseLocationOfEmp);
					if (baseLocationOfSG.equalsIgnoreCase(baseLocationOfEmp)) {
						employeeVehicleMaster = empData.get();
						employeeVehicleMaster.setReservedParking(true);
						return employeeVehicleMasterRepo.save(employeeVehicleMaster);
					} else {
						throw new DataNotFoundException(
								baseLocationOfSG + " is not the base location of this employee  " + dto.getEmpId());
					}
				} else {
					throw new DataNotFoundException(dto.getVehicleRegNo()
							+ " This vehicle is already inside the premises. Please Exit and then it can be marked for Reserved Parking.");
				}
			}
		} else {
			throw new ResourceAlreadyExistsException(
					dto.getVehicleRegNo() + " This vehicle is alredy assigned reserved parking");
		}

	}

}
