package com.mahindra.pms.serviceImpl;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.mahindra.pms.dao.EmployeeVehicleMasterRepo;
import com.mahindra.pms.dao.NotifyRepo;
import com.mahindra.pms.dao.ParkingStationMasterRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.EmployeeVehicleMaster;
import com.mahindra.pms.model.Notify;
import com.mahindra.pms.model.dto.NotifyDTO;
import com.mahindra.pms.service.NotifyService;


@Service
public class NotifyServiceImpl implements NotifyService {

	Logger log = LoggerFactory.getLogger(ParkingStationMasterServiceImpl.class);

	@Autowired
	ParkingStationMasterRepo parkingStationMasterRepo;

	@Autowired
	EmployeeVehicleMasterRepo employeeVehicleMasterRepo;

	@Autowired
	NotifyRepo notifyRepo;

	@Value("${firstApiUrl}")
	String firstUrl;

	@Value("${secondApiUrl}")
	String secondUrl;

	public String notificationSendFirstApi(String empId) {
		log.info("Notification notificationSendFirstApi service");
		RestTemplate restTemplate = new RestTemplate();
		String firstGetUrl = firstUrl;
		
		String token_no = empId;
		HttpHeaders headers = new HttpHeaders();
		headers.add("token_no", token_no);
		HttpEntity<String> request = new HttpEntity<String>(toString(), headers);
		ResponseEntity<String> responseEntity;
		responseEntity = restTemplate.exchange(firstGetUrl, HttpMethod.GET, request, String.class);
		return responseEntity.getBody();
	}

	public HttpStatus notificationSendSecondApi(String token, String msg, String title) {
		log.info("Notification notificationSendSecondApi service");
		String secondPostUrl = secondUrl;
		
		Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress("10.2.152.4",80));
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		requestFactory.setProxy(proxy);
	    
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-type", "application/x-www-form-urlencoded");
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("devhed", token);
		params.add("msg", msg);
		params.add("title", title);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(secondPostUrl, HttpMethod.POST, request,
				String.class);
		HttpStatus statusCode = responseEntity.getStatusCode();
		System.out.println("code " + responseEntity.getStatusCode());
		return responseEntity.getStatusCode();
	}

	@SuppressWarnings("unused")
	@Override
	public Notify observedIssue(NotifyDTO dto) throws Exception {
		log.info("inside Observed Issue");
		log.debug("inside Observed Issue");
		EmployeeVehicleMaster emp = employeeVehicleMasterRepo.findByVehicleRegNo(dto.getVehicleRegNo());
		if (emp != null) {
			Notify notify = new Notify();
			/*
			 * first api  and second call of mahindra predefind api for notification sending
			 */
			String employeeToken = emp.getEmpId();
	        String[] prependValues = new String[] {"0","0","00","000","0000","00000","000000","000000"};
	        if (employeeToken.length() >= 8) {
				employeeToken = emp.getEmpId();
			} else {
				employeeToken = prependValues[8 - employeeToken.length()]+emp.getEmpId();
			}
			String tokenData = notificationSendFirstApi(employeeToken);
			String[] splitTokens = tokenData.split("\\[", 0);
			String tokensArray = splitTokens[splitTokens.length - 1];
			String tokens = tokensArray.substring(0, tokensArray.length() - 1);
			String[] singleTokens = tokens.split(",");
			HttpStatus status = null;
			for (String s : singleTokens) {
				status = notificationSendSecondApi(s, dto.getObeservedIssue(), "PMS");
			}
			if (status.value() == 200) {
				notify.setNotificationSend(true);
			}
			System.out.println("fynal ans " + status.value());
			notify.setParkingStation(dto.getParkingStation());
			notify.setObeservedIssue(dto.getObeservedIssue());
			notify.setVehicleRegNo(dto.getVehicleRegNo());
			return notifyRepo.save(notify);
		}else {
			throw new DataNotFoundException( dto.getVehicleRegNo() + " this vehicle data is not available");
		}

	}

}
