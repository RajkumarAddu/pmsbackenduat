package com.mahindra.pms.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mahindra.pms.dao.BaseLocationRepo;
import com.mahindra.pms.dao.BuildingMasterRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.dto.ResponseBaseLocationAndBuildingMaster;
import com.mahindra.pms.service.BaseLocationService;

@Service
public class BaseLocationImpl implements BaseLocationService {

	Logger log = LoggerFactory.getLogger(BaseLocationImpl.class);

	@Autowired
	BaseLocationRepo baseLocationRepo;

	@Autowired
	BuildingMasterRepo buildingMasterRepo;

	@Override
	public List<BaseLocation> getAllBaseLocation() throws Exception {
		log.info("inside getAllBaseLocation Service");
		log.debug("inside getAllBaseLocation Service");
		Iterable<BaseLocation> locationsData = baseLocationRepo.findAll();
		List<BaseLocation> list = new ArrayList<BaseLocation>();
		list = (List<BaseLocation>) locationsData;
		if (list.size() != 0) {
			return list;
		} else {
			throw new DataNotFoundException("Base locations details not found.");
		}
	}

	@Override
	public List<ResponseBaseLocationAndBuildingMaster> getAllBuildingsAndBaseLocations() throws Exception {
		log.info("inside getAllBuildingsAndBaseLocations service");
		log.debug("inside getAllBuildingsAndBaseLocations service");
		List<ResponseBaseLocationAndBuildingMaster> list = new ArrayList<ResponseBaseLocationAndBuildingMaster>();
		List<BaseLocation> baseLocationList = (List<BaseLocation>) baseLocationRepo.findAll();
		if (baseLocationList.size() != 0) {
			for (BaseLocation bsL : baseLocationList) {
				ResponseBaseLocationAndBuildingMaster resBM = new ResponseBaseLocationAndBuildingMaster();
				List<BuildingMaster> buildingMasterList = buildingMasterRepo.findByBaseLocation(bsL);
				resBM.setBaseLocation(bsL);
				resBM.setBuildingMaster(buildingMasterList);
				list.add(resBM);
			}
			return list;
		} else {
			throw new DataNotFoundException("Base locations And Building Master details not found.");
		}
	}

}
