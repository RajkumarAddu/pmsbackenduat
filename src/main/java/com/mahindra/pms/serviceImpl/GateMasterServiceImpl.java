package com.mahindra.pms.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mahindra.pms.dao.GateMasterRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.GateMaster;
import com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO;
import com.mahindra.pms.service.GateMasterService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GateMasterServiceImpl implements GateMasterService {

	@Autowired
	GateMasterRepo gateMasterRepo;
	
	/*
	 * getAllGates Service
	 */
	@Override
	public List<GateMaster> getAllGates() throws Exception {
		List<GateMaster> listOfGates = (List<GateMaster>) gateMasterRepo.findAll();
		if(listOfGates.size()==0) {
			throw new DataNotFoundException("Gate details not found.");
		}
		return listOfGates;
	}

	/*
	 * getGatesByGateNo By GateNo
	 */
	@Override
	public List<GateMaster> getGatesByLocationId(BaseLocationFetchResponseDTO dto) throws Exception {
		BaseLocation baseLocation = new BaseLocation();
		baseLocation.setId(dto.getBaseLocation().getId());
		List<GateMaster> list = gateMasterRepo.findByBaseLocationId(baseLocation);
		if(list.size()==0) {
			throw new DataNotFoundException("Gates details not found.");
		}else {
	        return list;
		}
	}

}
