package com.mahindra.pms.serviceImpl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mahindra.pms.dao.VehicleInOutRepo;
import com.mahindra.pms.dao.VisitorVehicleDetailsRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.NoOfWheelsMaster;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VisitorVehicleDetails;
import com.mahindra.pms.model.dto.Report1DTO;
import com.mahindra.pms.model.dto.Report2DTO;
import com.mahindra.pms.model.dto.Report2ResponseDTO;
import com.mahindra.pms.model.dto.Report3DTO;
import com.mahindra.pms.service.ReportService;

@Service
public class ReportServiceImpl implements ReportService {

	Logger log = LoggerFactory.getLogger(VehicleInOutServiceImpl.class);

	@Autowired
	VehicleInOutRepo vehicleInOutRepo;

	@Autowired
	VisitorVehicleDetailsRepo visitorVehicleDetailsRepo;

	@Override
	public List<VehicleInOut> getDateWiseVehicleInOutData(Report1DTO dto) throws Exception {
		log.info("inside getDateWiseVehicleInOutData report Service");
		log.debug("inside getDateWiseVehicleInOutData report Service");
		List<VehicleInOut> list = vehicleInOutRepo.findByEmpIdAndStartDateAndEndDate(dto.getEmpId(), dto.getStartDate(),
				dto.getEndDate());
		if (list.size() != 0) {
			return list;
		} else {
			throw new DataNotFoundException("Vehicle entry of this employee:" + dto.getEmpId() + " is not present");
		}
	}

	@Override
	public Report2ResponseDTO getEmpAndVisitorData(Report2DTO dto) throws Exception {
		log.info("inside getEmpAndVisitorData report Service");
		log.debug("inside getEmpAndVisitorData report Service");
		Report2ResponseDTO res = new Report2ResponseDTO();
		if (!dto.getPresentInPremises()) {
			log.info("inside if block ");
			LocalDate startDate = dto.getStartDate();
			LocalDate endDate = dto.getEndDate();
			NoOfWheelsMaster noOfWheelsMaster = dto.getNoOfWheelsMaster();
			if (dto.getParkingStationMaster().getId() != 0) {
				log.info("inside particular parking stations data ");
				List<VehicleInOut> vehicleInOutList = vehicleInOutRepo
						.findAllByStartDateAndEndDate(dto.getParkingStationMaster(), startDate, endDate);
				List<VisitorVehicleDetails> visitorList = visitorVehicleDetailsRepo.findAllByStartDateAndEndDate(
						noOfWheelsMaster, dto.getParkingStationMaster(), startDate, endDate);
				List<VehicleInOut> vehicleList2 = vehicleInOutList.stream()
						.filter(w -> w.getVehicleId().getNoOfWheels().getId() == dto.getNoOfWheelsMaster().getId())
						.collect(Collectors.toList());
				/*
				 * count in premises
				 */
				Integer countVisitor = visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(startDate, endDate);
				Integer countVehicle = vehicleInOutRepo.CountOfAllVehicleInPremises(startDate, endDate);
				res.setCount(countVehicle+countVisitor);
				res.setVisitorVehicleDetails(visitorList);
				res.setVehicleInOut(vehicleList2);
				return res;
			} else {
				log.info("inside all parking stations data");
				List<VehicleInOut> vehicleInOutList = vehicleInOutRepo.findAllByOnlyStartDateAndEndDate(startDate,
						endDate);
				List<VisitorVehicleDetails> visitorList = visitorVehicleDetailsRepo
						.findAllByOnlyStartDateAndEndDate(noOfWheelsMaster, startDate, endDate);
				List<VehicleInOut> vehicleList2 = vehicleInOutList.stream()
						.filter(w -> w.getVehicleId().getNoOfWheels().getId() == dto.getNoOfWheelsMaster().getId())
						.collect(Collectors.toList());

				res.setVisitorVehicleDetails(visitorList);
				res.setVehicleInOut(vehicleList2);
				/*
				 * count in premises
				 */
				Integer countVisitor = visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(startDate, endDate);
				Integer countVehicle = vehicleInOutRepo.CountOfAllVehicleInPremises(startDate, endDate);
				res.setCount(countVehicle+countVisitor);
				return res;
			}
		} else {
			log.info("inside else block ");
			LocalDate startDate = dto.getStartDate();
			LocalDate endDate = dto.getEndDate();
			NoOfWheelsMaster noOfWheelsMaster = dto.getNoOfWheelsMaster();
			if (dto.getParkingStationMaster().getId() != 0) {
				log.info("inside particular parking stations data ");
				List<VehicleInOut> vehicleInOutList = vehicleInOutRepo
						.findByStartDateAndEndDateAndIsExitFalse(dto.getParkingStationMaster(), startDate, endDate);
				List<VisitorVehicleDetails> visitorList = visitorVehicleDetailsRepo
						.findByStartDateAndEndDateAndIsExitFalse(noOfWheelsMaster, dto.getParkingStationMaster(),
								startDate, endDate);
				List<VehicleInOut> vehicleList2 = vehicleInOutList.stream()
						.filter(w -> w.getVehicleId().getNoOfWheels().getId() == dto.getNoOfWheelsMaster().getId())
						.collect(Collectors.toList());
				log.info("inside include visitor if block ");
				res.setVisitorVehicleDetails(visitorList);
				res.setVehicleInOut(vehicleList2);
				/*
				 * count in premises
				 */
				Integer countVisitor = visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(startDate, endDate);
				Integer countVehicle = vehicleInOutRepo.CountOfAllVehicleInPremises(startDate, endDate);
				res.setCount(countVehicle+countVisitor);
				return res;
			} else {
				List<VehicleInOut> vehicleInOutList = vehicleInOutRepo
						.findByOnlyStartDateAndEndDateAndIsExitFalse(startDate, endDate);
				List<VisitorVehicleDetails> visitorList = visitorVehicleDetailsRepo
						.findByOnlyStartDateAndEndDateAndIsExitFalse(noOfWheelsMaster, startDate, endDate);
				List<VehicleInOut> vehicleList2 = vehicleInOutList.stream()
						.filter(w -> w.getVehicleId().getNoOfWheels().getId() == dto.getNoOfWheelsMaster().getId())
						.collect(Collectors.toList());
				res.setVisitorVehicleDetails(visitorList);
				res.setVehicleInOut(vehicleList2);
				/*
				 * count in premises
				 */
				Integer countVisitor = visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(startDate, endDate);
				Integer countVehicle = vehicleInOutRepo.CountOfAllVehicleInPremises(startDate, endDate);
				res.setCount(countVehicle+countVisitor);
				return res;
			}
		}
	}

	@Override
	public Report2ResponseDTO getEmpAndVisitorDataByPsId(Report3DTO dto) throws Exception {
		log.info("inside getEmpAndVisitorDataByPsId service");
		log.debug("inside getEmpAndVisitorDataByPsId service");
		Report2ResponseDTO report3 = new Report2ResponseDTO();
		if (dto.getIsReserved()) {
			log.info("inside Reserved vehicle report ");
			List<VisitorVehicleDetails> visitorList = visitorVehicleDetailsRepo
					.findByBaseLocationAndPsIdAndDate(dto.getBaseLocation(), dto.getParkingStation());
			List<VehicleInOut> vehicleList = vehicleInOutRepo.findByBaseLocationAndPsIdAndDate(dto.getBaseLocation(),
					dto.getParkingStation());

			List<VehicleInOut> resrvedVehicleList = vehicleList.stream()
					.filter(r -> r.isReserved() == dto.getIsReserved()).collect(Collectors.toList());
			report3.setVisitorVehicleDetails(visitorList);
			report3.setVehicleInOut(resrvedVehicleList);
			return report3;
		} else {
			log.info("inside else Reserved vehicle report ");
			List<VehicleInOut> vehicleList = vehicleInOutRepo.findByBaseLocationAndPsIdAndDate(dto.getBaseLocation(),
					dto.getParkingStation());
			report3.setVehicleInOut(vehicleList);
			return report3;
		}
	}

}
