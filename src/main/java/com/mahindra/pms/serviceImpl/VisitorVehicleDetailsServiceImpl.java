package com.mahindra.pms.serviceImpl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mahindra.pms.dao.ParkingStationMasterRepo;
import com.mahindra.pms.dao.VisitorVehicleDetailsRepo;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.exception.ResourceAlreadyExistsException;
import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.BuildingMaster;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VisitorVehicleDetails;
import com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleRegNoDTO;
import com.mahindra.pms.model.dto.VisitorVehicleEntryDTO;
import com.mahindra.pms.model.dto.VisitorVehicleExitDTO;
import com.mahindra.pms.service.VisitorVehicleDetailsService;

@Service
public class VisitorVehicleDetailsServiceImpl implements VisitorVehicleDetailsService {

	Logger log = LoggerFactory.getLogger(VisitorVehicleDetailsServiceImpl.class);
	@Autowired
	VisitorVehicleDetailsRepo visitorVehicleDetailsRepo;

	@Autowired
	ParkingStationMasterRepo parkingStationMasterRepo;

	@Override
	public List<VisitorVehicleDetails> getVisitorsList() throws Exception {
		log.info("Inside getVisitorsList service");
		log.debug("Inside getVisitorsList service");
		List<VisitorVehicleDetails> list = (List<VisitorVehicleDetails>) visitorVehicleDetailsRepo.findAll();
		log.info("list of visitors:" + list);
		if (list.size()==0) {
			throw new DataNotFoundException("visitors data not found");
		} else {
			return list;
		}
	}

	@Override
	public List<VisitorVehicleDetails> visitorsListIsExitFalse(BaseLocationFetchResponseDTO dto) throws Exception {
		log.info("Inside todaysVisitorsList service");
		log.debug("Inside todaysVisitorsList service");
		List<VisitorVehicleDetails> list = 
				visitorVehicleDetailsRepo.findByBaseLocationAndBuildingMasterAndIsExit(dto.getBaseLocation(),dto.getBuildingMaster(),false);
		log.info("list of visitors:" + list);
		if (list.size()==0) {
			throw new DataNotFoundException("There are not visitors.");
		} else {
			return list;
		}
	}

	@Override
	public VisitorVehicleDetails visitorEntry(VisitorVehicleEntryDTO visitorVehicleDetailsDTO) throws Exception {
		log.info("Inside visitorEntry service");
		log.debug("Inside visitorEntry service");
		String vehicleRegNo = visitorVehicleDetailsDTO.getVehicleRegNo();
		Optional<VisitorVehicleDetails> visitorDataByRegNo = visitorVehicleDetailsRepo
				.findByVehicleRegNoAndIsExitFalse(vehicleRegNo);
		if (visitorDataByRegNo.isPresent()) {
			throw new ResourceAlreadyExistsException(vehicleRegNo + " This vehicle is already parked.");
		} else {
			Integer noOfWheelsId = visitorVehicleDetailsDTO.getNoOfWheels().getId();
			if (noOfWheelsId == 1) {
				VisitorVehicleDetails visitorVehicleDetails = new VisitorVehicleDetails();
				BeanUtils.copyProperties(visitorVehicleDetailsDTO, visitorVehicleDetails);
				visitorVehicleDetails.setEntryDateTime(LocalDateTime.now());
				visitorVehicleDetails.setExit(false);
				visitorVehicleDetails.setEntryGate(visitorVehicleDetailsDTO.getEntryGate());
				visitorVehicleDetails.setNoOfWheels(visitorVehicleDetailsDTO.getNoOfWheels());
				System.out.println(visitorVehicleDetailsDTO.getNoOfWheels());
				BuildingMaster bm=visitorVehicleDetailsDTO.getBuildingMaster();
				BaseLocation bsl=visitorVehicleDetailsDTO.getBaseLocation();
				/*
				 * parking booking for twowheels visitor
				 */
				ParkingStationMaster bookParkingTwoWheelsSlots = bookParkingTwoWheelsSlots(bm);
				if (bookParkingTwoWheelsSlots == null) {
					throw new DataNotFoundException("Parking is not yet assigned.");
				}
				visitorVehicleDetails.setReservedParkingStation(bookParkingTwoWheelsSlots);
				return visitorVehicleDetailsRepo.save(visitorVehicleDetails);
			} else {
				VisitorVehicleDetails visitorVehicleDetails = new VisitorVehicleDetails();
				BeanUtils.copyProperties(visitorVehicleDetailsDTO, visitorVehicleDetails);
				visitorVehicleDetails.setEntryDateTime(LocalDateTime.now());
				visitorVehicleDetails.setExit(false);
				visitorVehicleDetails.setEntryGate(visitorVehicleDetailsDTO.getEntryGate());
				visitorVehicleDetails.setNoOfWheels(visitorVehicleDetailsDTO.getNoOfWheels());
				BuildingMaster bm=visitorVehicleDetailsDTO.getBuildingMaster();
				BaseLocation bsl=visitorVehicleDetailsDTO.getBaseLocation();
				visitorVehicleDetails.setBaseLocation(bsl);
				visitorVehicleDetails.setBuildingMaster(bm);
				/*
				 * parking booking for visitor
				 */
				ParkingStationMaster bookParkingForReservedSlots = bookParkingForReservedSlots(bsl,bm);
				if (bookParkingForReservedSlots == null) {
					throw new DataNotFoundException("Parking is not yet assigned.");
				}
				visitorVehicleDetails.setReservedParkingStation(bookParkingForReservedSlots);
				return visitorVehicleDetailsRepo.save(visitorVehicleDetails);
			}
		}
	}

	public ParkingStationMaster bookParkingTwoWheelsSlots(BuildingMaster buildingMaster) throws DataNotFoundException {
		log.info("inside bookParkingTwoWheelsSlots service ");
		ParkingStationMaster psData = parkingStationMasterRepo.findByIsTwoWheelerPsAndBuildingMaster(true, buildingMaster);
		ParkingStationMaster psmData = new ParkingStationMaster();
		if (psData != null) {
			if (psData.getAvailablilityForUnReserved() != 0) {
				Integer id = psData.getId();
				Integer avlTwoWheelsPs = psData.getAvailablilityForUnReserved() - 1;
				parkingStationMasterRepo.updateUnreservedParking(avlTwoWheelsPs, id,buildingMaster);
				Optional<ParkingStationMaster> psData1 = parkingStationMasterRepo.findById(id);
				psmData = psData1.get();
				return psmData;
			} else {
				throw new DataNotFoundException("Two Wheeler Parking is Full");
			}
		} else {
			throw new DataNotFoundException("Two Wheeler Parking is not Available");
		}
	}

	public ParkingStationMaster bookParkingForReservedSlots(BaseLocation bsl,BuildingMaster buildingMaster) throws DataNotFoundException {
		log.info("inside Reserved Parking Booking service");
		//Iterable<ParkingStationMaster> psData = parkingStationMasterRepo.findAll();
		List<ParkingStationMaster> psData = parkingStationMasterRepo.findByBaseLocationIdAndBuildingMaster(bsl, buildingMaster);
		Optional<ParkingStationMaster> getPSData1, getPSData2 = null;
		ParkingStationMaster psmData = new ParkingStationMaster();
		for (ParkingStationMaster psm : psData) {
			if (psm.getIsTwoWheelerPs() == false) {
				Integer id = psm.getId();
				Integer maxId = parkingStationMasterRepo.findMaxId(buildingMaster);
				System.out.println("maxId" + maxId+" ps name "+psm.getPsName());
				System.out.println("round 1 --");
				if (psm.getAvailablilityForReserved() == 0 && id == maxId) {
					System.out.println("if");
					throw new DataNotFoundException("parking is full");
				} /*
					 * else if (psm.getAvailablilityForReserved() == 0) {
					 * parkingStationMasterRepo.updateTrueIFFullReserved(id);
					 * parkingStationMasterRepo.updateFalseIFFullReserved(id + 1); getPSData1 =
					 * parkingStationMasterRepo.findById(id + 1); Integer avlResPs =
					 * getPSData1.get().getAvailablilityForReserved() - 1; //
					 * parkingStationMasterRepo.updatereservedParking(avlResPs, id + 1);
					 * parkingStationMasterRepo.updateReservedParking(avlResPs, id +
					 * 1,buildingMaster); System.out.println("else if");
					 * Optional<ParkingStationMaster> newPSData =
					 * parkingStationMasterRepo.findById(id + 1); psmData = newPSData.get(); return
					 * psmData; }
					 */ 
				else if(psm.getAvailablilityForReserved() != 0){
					getPSData2 = parkingStationMasterRepo.findById(id);
					Integer avlResPs = getPSData2.get().getAvailablilityForReserved() - 1;
					parkingStationMasterRepo.updateReservedParking(avlResPs, id,buildingMaster);
					Optional<ParkingStationMaster> newPSData = parkingStationMasterRepo.findById(psm.getId());
					psmData = newPSData.get();
					System.out.println("1 " + psmData.getPsName());
					return psmData;
				}
			}
		}
		return psmData;
	}

	@Override
	public VisitorVehicleDetails visitorExit(VisitorVehicleExitDTO visitorVehicleExitDTO) throws Exception {
		log.info("Inside visitorExit service");
		log.debug("Inside visitorExit service");
		String vehicleRegNo = visitorVehicleExitDTO.getVehicleRegNo();
		VisitorVehicleDetails exit = new VisitorVehicleDetails();
		Optional<VisitorVehicleDetails> visitorDataByRegNo = visitorVehicleDetailsRepo
				.findByVehicleRegNoAndIsExitFalse(vehicleRegNo);
		if (!visitorDataByRegNo.isPresent()) {
			throw new DataNotFoundException(vehicleRegNo + " This vehicle has not yet entered.");
		} else {
			log.info("Inside else for release parking for Visitor vehicle");
			VisitorVehicleDetails visitorVehicleExit = new VisitorVehicleDetails();
			BeanUtils.copyProperties(visitorDataByRegNo.get(), visitorVehicleExit);
			visitorVehicleExit.setExitDateTime(LocalDateTime.now());
			visitorVehicleExit.setExit(true);
			visitorVehicleExit.setUpdatedBy(visitorVehicleExitDTO.getUpdatedBy());
			visitorVehicleExit.setExitGate(visitorVehicleExitDTO.getExitGate());
			visitorVehicleExit.setModified(LocalDate.now());
			visitorVehicleExit.setExitGate(visitorVehicleExitDTO.getExitGate());
            BuildingMaster bm=visitorVehicleExit.getBuildingMaster();
			/*
			 * Parking release for visitor
			 */
			if (visitorVehicleExit.getNoOfWheels().getId() == 1) {
				ParkingStationMaster psData = parkingStationMasterRepo.findByIsTwoWheelerPsAndBuildingMaster(true, bm);
				Integer avlPsTwoWheels = psData.getAvailablilityForUnReserved() + 1;
				Integer id = psData.getId();
				parkingStationMasterRepo.updateUnreservedParking(avlPsTwoWheels, id,bm);
				exit = visitorVehicleDetailsRepo.save(visitorVehicleExit);
				return exit;
			}
			ParkingStationMaster psData = visitorDataByRegNo.get().getReservedParkingStation();
			Integer psId = psData.getId();
			Integer avlResPs = psData.getAvailablilityForReserved() + 1;
			parkingStationMasterRepo.updateReservedParking(avlResPs, psId,bm);
			// save exit data
			exit = visitorVehicleDetailsRepo.save(visitorVehicleExit);
			/*
			 * Optional<ParkingStationMaster> newPSData =
			 * parkingStationMasterRepo.findById(psId); Integer availablilityForReserved =
			 * newPSData.get().getAvailablilityForReserved(); Integer newId =
			 * newPSData.get().getId(); if (availablilityForReserved != 0 && newId == 1) {
			 * parkingStationMasterRepo.updateFalseIFFullReserved(1);
			 * parkingStationMasterRepo.updateTrueAfterSecondIdReserved(); }
			 */
		}
		return exit;
	}

	/*
	 * 
	 */
	@Override
	public VisitorVehicleDetails getVisitorbyVRegNo(EmployeeVehicleRegNoDTO dto) throws Exception {
		log.info("inside getVisitorbyVRegNo service");
		log.debug("inside getVisitorbyVRegNo service");
		List<VisitorVehicleDetails> visitorListByVRegNo = visitorVehicleDetailsRepo
				.findByVehicleRegNo(dto.getVehicleRegNo());
		System.out.println(visitorListByVRegNo.size());
		if (visitorListByVRegNo.size() != 0) {
			VisitorVehicleDetails visitorData = visitorListByVRegNo.get(visitorListByVRegNo.size() - 1);
			return visitorData;
		} else {
			throw new DataNotFoundException(dto.getVehicleRegNo() + "  this vehicle details not found.");
		}
	}

}
