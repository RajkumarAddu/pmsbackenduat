package com.mahindra.pms.serviceImpl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.mahindra.pms.model.VehicleInOut;

public class EmployeeVehicleDetailsExcelExporter {

	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	List<VehicleInOut> vehiclelist;

	public EmployeeVehicleDetailsExcelExporter(List<VehicleInOut> vehiclelist) {
		this.vehiclelist = vehiclelist;
		workbook = new XSSFWorkbook();
	}

	private void writeHeaderLine() {
		sheet = workbook.createSheet("Users");

		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "EmployeeId", style);
		createCell(row, 1, "Name", style);
		createCell(row, 2, "Vehicle Registration Number", style);
		createCell(row, 3, "Entry Gate", style);
		createCell(row, 4, "Entry Date Time", style);
		// createCell(row, 4, "Exit Gate", style);
		// createCell(row, 5, "Exit Date Time", style);
		createCell(row, 5, "Building Name", style);
		createCell(row, 6, "Parking Station Name", style);
		createCell(row, 7, "Baselocation Name", style);

	}

	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		// List<VehicleInOut> vehicleInOutList = report2Response.getVehicleInOut();
		for (VehicleInOut vehicle : vehiclelist) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			createCell(row, columnCount++, vehicle.getEmpId(), style);
			createCell(row, columnCount++, vehicle.getVehicleId().getEmpName(), style);
			createCell(row, columnCount++, vehicle.getVehicleRegNo(), style);
			createCell(row, columnCount++, vehicle.getEntryGate().getGateName(), style);
			createCell(row, columnCount++, vehicle.getEntryDateTime().toString(), style);
			// createCell(row, columnCount++, vehicle.getExitGate().getGateName(), style);
			// createCell(row, columnCount++, vehicle.getExitDateTime().toString(), style);
			createCell(row, columnCount++, vehicle.getBuildingMaster().getBuildingName(), style);
			createCell(row, columnCount++, vehicle.getParkingStaionId().getPsName(), style);
			createCell(row, columnCount++, vehicle.getBaseLocationId().getLocationName(), style);

		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}
