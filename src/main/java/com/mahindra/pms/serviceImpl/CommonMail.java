package com.mahindra.pms.serviceImpl;

import com.mahindra.MailClient.Client.MailClient;
import com.mahindra.MailClient.DTO.Authentication;
import com.mahindra.MailClient.DTO.MailDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleMasterDTO;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CommonMail {

	// please call this
	public void sendDetails(EmployeeVehicleMasterDTO dto, File file) {
		List<File> attachments = new ArrayList<File>();
		attachments.add(file);
		String mailcontent = null;
		mailcontent = "Dear Sir/Madam,<br><br>Your Vehicle is Registered for Parking at <Location> Location."
				+ "  <br><br>The Vehicle Details are as follows-"
				+ " <br><br>Vehicle Registration No - <VehicleRegNo>" 
				+ " <br>Make - <Make>"
				+ " <br>Model - <Model>" 
				+ "<br><br>PFA the QR Code for this registered vehicle."
				+ " <br><br>Thanks & Regards,<br>Admin<br><br>"
				+ "Note:  This is system generated email. Please do not reply.";

		mailcontent=mailcontent.replaceAll("<Location>","'"+dto.getBaseLocation().getLocationName()+"'");
		mailcontent=mailcontent.replaceAll("<VehicleRegNo>",dto.getVehicleRegNo());
		mailcontent=mailcontent.replaceAll("<Make>",dto.getMake());
		mailcontent=mailcontent.replaceAll("<Model>",dto.getModel());

		sendMail(dto.getEmail(), "Parking Management System", mailcontent, "QR Code", attachments);
	}

	public void sendMail(String toEmail, String subject, String message, String mailtitle, List<File> attachments) {
		MailClient mail = new MailClient();
		MailDTO mailDTO = new MailDTO("awaras-cont@mahindrasamvaad.com", toEmail, subject, message, "SM", mailtitle,
				"text/html", attachments, false, null, null);
		try {
			String str = mail.SendMail(new Authentication("ashish", "pass,123"), mailDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
