package com.mahindra.pms.exception;

@SuppressWarnings("serial")
public class DataNotFoundException extends RuntimeException{

	public DataNotFoundException(String msg) {
		super(msg);
	}
}
