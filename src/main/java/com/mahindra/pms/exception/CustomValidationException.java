package com.mahindra.pms.exception;

@SuppressWarnings("serial")
public class CustomValidationException extends Exception{

	public CustomValidationException(String msg) {
		super(msg);
	}
}
