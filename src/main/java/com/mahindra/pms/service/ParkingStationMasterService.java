package com.mahindra.pms.service;

import java.util.List;

import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.ParkingStationMaster;
import com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO;
import com.mahindra.pms.model.dto.ChangeParkingSlotDTO;
import com.mahindra.pms.model.dto.ParkingStationMasterResponseDTO;

public interface ParkingStationMasterService {
	ParkingStationMasterResponseDTO fetchParkingStationAndSlots(BaseLocationFetchResponseDTO dto)throws Exception;
	List<ParkingStationMaster> fetchAllParkingStationAndSlots(BaseLocationFetchResponseDTO dto)throws Exception;
	String changePsSlot(ChangeParkingSlotDTO dto)throws Exception;
	
	
}
