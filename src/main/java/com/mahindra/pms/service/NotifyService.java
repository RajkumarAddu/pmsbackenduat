package com.mahindra.pms.service;

import com.mahindra.pms.model.Notify;
import com.mahindra.pms.model.dto.NotifyDTO;

public interface NotifyService {

	Notify observedIssue(NotifyDTO dto)throws Exception;
}
