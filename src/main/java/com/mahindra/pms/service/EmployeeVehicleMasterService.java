package com.mahindra.pms.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;
import com.mahindra.pms.exception.DataNotFoundException;
import com.mahindra.pms.model.EmployeeVehicleMaster;
import com.mahindra.pms.model.FuelType;
import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.VehicleTypeMaster;
import com.mahindra.pms.model.dto.EmployeeFetchDetailsDTO;
import com.mahindra.pms.model.dto.EmployeeVRegNoFetchDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleListDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleMasterDTO;
import com.mahindra.pms.model.dto.ResponseEmployeeVRegNoFetchDTO;
import com.mahindra.pms.model.dto.ResponseOfEmpVRegistration;


public interface EmployeeVehicleMasterService {

	ResponseOfEmpVRegistration saveEmpVM(EmployeeVehicleMasterDTO empVehicleMasterDTO) throws Exception;

	EmployeeVehicleMaster getEmpVMById(EmployeeFetchDetailsDTO dto) throws Exception;

	EmployeeVehicleMaster updateEmpVM(EmployeeVehicleMasterDTO employeeVehicleMasterDTO) throws Exception;

	void deleteEmpVM(EmployeeFetchDetailsDTO dto) throws Exception;

	EmployeeVehicleMaster getEmpVDetailsByVehicleRegNo(String regNo) throws DataNotFoundException;

	List<EmployeeVehicleListDTO> getEmpVehiclePresentToday(EmployeeFetchDetailsDTO dto) throws Exception;

	List<FuelType> getAllFuelTypeData() throws Exception;

	List<VehicleTypeMaster> getAllVehicleTypeMaster() throws Exception;

	List<ResponseEmployeeVRegNoFetchDTO> getAllVehicleRegNo(EmployeeVRegNoFetchDTO dto) throws Exception;

	EmployeeVehicleMaster givenReservedParking(EmployeeFetchDetailsDTO dto) throws Exception;

	ResponseOfEmpVRegistration generateQRCodeImage(EmployeeVehicleMasterDTO dto) throws Exception;
	
	ResponseOfEmpVRegistration generateOnlyQRCodeImage(EmployeeVehicleMasterDTO dto) throws Exception;

	EmployeeVehicleMaster previewImage(EmployeeFetchDetailsDTO dto)throws Exception;
}
