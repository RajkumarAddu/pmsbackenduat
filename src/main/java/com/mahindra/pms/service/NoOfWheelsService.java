package com.mahindra.pms.service;

import java.util.List;

import com.mahindra.pms.model.NoOfWheelsMaster;

public interface NoOfWheelsService {
	
	List<NoOfWheelsMaster> getAllWheelsData() throws Exception;

}
