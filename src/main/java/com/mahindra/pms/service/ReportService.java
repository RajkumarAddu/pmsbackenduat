package com.mahindra.pms.service;

import java.util.List;

import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.dto.Report1DTO;
import com.mahindra.pms.model.dto.Report2DTO;
import com.mahindra.pms.model.dto.Report2ResponseDTO;
import com.mahindra.pms.model.dto.Report3DTO;

public interface ReportService {

	List<VehicleInOut> getDateWiseVehicleInOutData(Report1DTO dto) throws Exception;

	Report2ResponseDTO getEmpAndVisitorData(Report2DTO dto) throws Exception;

	Report2ResponseDTO getEmpAndVisitorDataByPsId(Report3DTO dto) throws Exception;
}
