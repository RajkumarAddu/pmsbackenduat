package com.mahindra.pms.service;

import java.util.List;

import com.mahindra.pms.model.GateMaster;
import com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO;

public interface GateMasterService {

	List<GateMaster> getAllGates()throws Exception;
	List<GateMaster> getGatesByLocationId(BaseLocationFetchResponseDTO baseLocationIdDTO)throws Exception;
}
