package com.mahindra.pms.service;

import com.mahindra.pms.model.SecurityGaurdMaster;
import com.mahindra.pms.model.dto.CheckLocationResponseDTO;
import com.mahindra.pms.model.dto.SecurityGaurdMasterDTO;

public interface SecurityGaurdMasterService {
	
	boolean checkIsLocationAvl(CheckLocationResponseDTO dto) throws Exception;
	SecurityGaurdMaster registerSecurityG(SecurityGaurdMasterDTO sgmDTO)throws Exception;

}
