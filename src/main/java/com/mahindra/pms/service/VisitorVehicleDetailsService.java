package com.mahindra.pms.service;

import java.util.List;

import com.mahindra.pms.model.VisitorVehicleDetails;
import com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO;
import com.mahindra.pms.model.dto.EmployeeVehicleRegNoDTO;
import com.mahindra.pms.model.dto.VisitorVehicleEntryDTO;
import com.mahindra.pms.model.dto.VisitorVehicleExitDTO;

public interface VisitorVehicleDetailsService {
	
	List<VisitorVehicleDetails> getVisitorsList()throws Exception;
	List<VisitorVehicleDetails> visitorsListIsExitFalse(BaseLocationFetchResponseDTO dto)throws Exception;
	VisitorVehicleDetails visitorEntry(VisitorVehicleEntryDTO visitorVehicleDetailsDTO) throws Exception;
	VisitorVehicleDetails visitorExit(VisitorVehicleExitDTO visitorVehicleExitDTO) throws Exception;
	VisitorVehicleDetails getVisitorbyVRegNo(EmployeeVehicleRegNoDTO dto)throws Exception;
}
