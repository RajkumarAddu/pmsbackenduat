package com.mahindra.pms.service;

import com.mahindra.pms.model.VehicleInOut;
import com.mahindra.pms.model.dto.VehicleEntryDTO;
import com.mahindra.pms.model.dto.VehicleExitDTO;

public interface VehicleInOutService {

	VehicleInOut vehicleInEntry(VehicleEntryDTO vehicleEntryDTO)throws Exception;
	VehicleInOut vehicleExit(VehicleExitDTO vehicleExitDTO)throws Exception;
}
