package com.mahindra.pms.service;

import java.util.List;

import com.mahindra.pms.model.BaseLocation;
import com.mahindra.pms.model.dto.ResponseBaseLocationAndBuildingMaster;

public interface BaseLocationService {
	List<BaseLocation> getAllBaseLocation()throws Exception;
	List<ResponseBaseLocationAndBuildingMaster> getAllBuildingsAndBaseLocations()throws Exception;
}
