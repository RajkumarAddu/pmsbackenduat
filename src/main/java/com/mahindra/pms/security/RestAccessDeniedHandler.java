/*
 * package com.mahindra.pms.security;
 * 
 * import com.fasterxml.jackson.databind.ObjectMapper; import
 * com.mahindra.pms.exception.ErrorDetails;
 * 
 * import org.slf4j.Logger; import org.slf4j.LoggerFactory; import
 * org.springframework.security.access.AccessDeniedException; import
 * org.springframework.security.web.access.AccessDeniedHandler; import
 * org.springframework.stereotype.Component;
 * 
 * import javax.servlet.ServletException; import
 * javax.servlet.http.HttpServletRequest; import
 * javax.servlet.http.HttpServletResponse; import java.io.IOException; import
 * java.io.OutputStream; import java.util.Date;
 * 
 * @Component public class RestAccessDeniedHandler implements
 * AccessDeniedHandler {
 * 
 * Logger log = LoggerFactory.getLogger(RestAccessDeniedHandler.class);
 * 
 * @Override public void handle(HttpServletRequest httpServletRequest,
 * HttpServletResponse httpServletResponse, AccessDeniedException e) throws
 * IOException, ServletException {
 * log.info(" strating handle method aith argument  ");
 * log.info("inside access denied exception handle service ");
 * log.debug("inside access denied exception handle service "); OutputStream out
 * = null; ObjectMapper MAPPER = new ObjectMapper(); try { ErrorDetails
 * errDetails = new ErrorDetails(new Date(), e.getMessage(), "Access denied",
 * null, 404, false); // httpServletResponse.setStatus(401); out =
 * httpServletResponse.getOutputStream();
 * 
 * MAPPER.writerWithDefaultPrettyPrinter().writeValue(out, errDetails);
 * 
 * } catch (Exception ex) {
 * log.error("Excepotion occured in RestAccessDeniedHandler ::", ex);
 * ex.printStackTrace(); } finally { out.close(); }
 * 
 * log.info(" ending handle method aith argument  "); } }
 */