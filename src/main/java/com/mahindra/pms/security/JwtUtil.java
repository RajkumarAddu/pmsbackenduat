package com.mahindra.pms.security;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtil {

	Logger log = LoggerFactory.getLogger(JwtUtil.class);

	@Value("${app.secret}")
	private String secret;

	// 1 generate Token...
	public String generateToken(String sgId) {
		log.info("inside  generateToken service");
		log.debug("inside  generateToken service");
		return Jwts.builder().setSubject(sgId).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(100000)))
				.signWith(SignatureAlgorithm.HS256, secret.getBytes()).compact();
	}

	// 2 get claims...
	public Claims getClaims(String token) {
		return Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
	}

	// 3 Read Expiry date
	public Date getExpDate(String token) {
		return getClaims(token).getExpiration();
	}

	// 4 Read Subject..
	public String getUserName(String token) {
		return getClaims(token).getSubject();
	}

	// 5 validate exp date...
	public boolean isTokenExpired(String token) {
		Date expDate = getExpDate(token);
		return expDate.before(new Date(System.currentTimeMillis()));
	}

	// 6 validate userName in Token and database, expDate

	public boolean validateToken(String token, String userName) {
		String tokenUsrName = getUserName(token);
		return (userName.equals(tokenUsrName) && !isTokenExpired(token));
	}

}
