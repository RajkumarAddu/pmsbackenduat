/*
 * package com.mahindra.pms.security;
 * 
 * import java.util.Arrays;
 * 
 * import org.slf4j.Logger; import org.slf4j.LoggerFactory; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.context.annotation.Bean; import
 * org.springframework.context.annotation.Configuration; import
 * org.springframework.http.HttpMethod; import
 * org.springframework.security.authentication.AuthenticationManager; import
 * org.springframework.security.config.annotation.authentication.builders.
 * AuthenticationManagerBuilder; import
 * org.springframework.security.config.annotation.method.configuration.
 * EnableGlobalMethodSecurity; import
 * org.springframework.security.config.annotation.web.builders.HttpSecurity;
 * import org.springframework.security.config.annotation.web.configuration.
 * EnableWebSecurity; import
 * org.springframework.security.config.annotation.web.configuration.
 * WebSecurityConfigurerAdapter; import
 * org.springframework.security.config.http.SessionCreationPolicy; import
 * org.springframework.security.core.userdetails.UserDetailsService; import
 * org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder; import
 * org.springframework.security.web.authentication.
 * UsernamePasswordAuthenticationFilter; import
 * org.springframework.web.cors.CorsConfiguration; import
 * org.springframework.web.cors.CorsConfigurationSource; import
 * org.springframework.web.cors.UrlBasedCorsConfigurationSource;
 * 
 * @Configuration
 * 
 * @EnableWebSecurity
 * 
 * @EnableGlobalMethodSecurity(prePostEnabled = true) public class
 * WebSecurityConfig extends WebSecurityConfigurerAdapter {
 * 
 * Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);
 * 
 * @Autowired private UserDetailsService userDetailsSevrvice;
 * 
 * @Autowired private BCryptPasswordEncoder passwordEncoder;
 * 
 * @Autowired private InvalidUserAuthenticationEntryPoint
 * authenticationEntryPoint;
 * 
 * @Autowired private SecurityFilter filter;
 * 
 * @Override
 * 
 * @Bean public AuthenticationManager authenticationManagerBean() throws
 * Exception { return super.authenticationManagerBean(); }
 * 
 * @Override public void configure(AuthenticationManagerBuilder auth) throws
 * Exception {
 * auth.userDetailsService(userDetailsSevrvice).passwordEncoder(passwordEncoder)
 * ;
 * 
 * }
 * 
 * 
 * @Bean CorsConfigurationSource corsConfigurationSource() {
 * System.out.println("inside cors new"); CorsConfiguration configuration = new
 * CorsConfiguration(); configuration.setAllowedOrigins(Arrays.asList("/**"));
 * configuration.addAllowedHeader("*");
 * configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE",
 * "OPTIONS")); UrlBasedCorsConfigurationSource source = new
 * UrlBasedCorsConfigurationSource(); source.registerCorsConfiguration("/**",
 * configuration); return source; }
 * 
 * 
 * @Override public void configure(HttpSecurity http) throws Exception {
 * log.info("inside WebSecurityConfig service");
 * log.debug("inside WebSecurityConfig service");
 * System.out.println("cors in http security");
 * 
 * http.cors(c -> { CorsConfigurationSource cs = resource -> { CorsConfiguration
 * cc = new CorsConfiguration(); cc.setAllowedOrigins(Arrays.asList("*"));
 * cc.setAllowedHeaders(Arrays.asList("*"));
 * cc.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE",
 * "OPTIONS")); return cc; }; });
 * 
 * http.csrf().disable().sessionManagement().sessionCreationPolicy(
 * SessionCreationPolicy.STATELESS)
 * .and().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
 * .and() .addFilterBefore(filter,
 * UsernamePasswordAuthenticationFilter.class).antMatcher(
 * "/generateAccessToken")
 * .antMatcher("/getAllGates").antMatcher("/getAllBaseLocation").antMatcher(
 * "/checkIsLocationAvl")
 * .authorizeRequests().anyRequest().authenticated().and().exceptionHandling()
 * .authenticationEntryPoint(authenticationEntryPoint);
 * 
 * }
 * 
 * }
 */