/*
 * package com.mahindra.pms.security;
 * 
 * import java.util.ArrayList; import java.util.List; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.security.core.userdetails.UserDetails; import
 * org.springframework.security.core.userdetails.UserDetailsService; import
 * org.springframework.security.core.userdetails.UsernameNotFoundException;
 * import org.springframework.stereotype.Service;
 * 
 * import com.mahindra.pms.dao.SecurityGaurdMasterRepo; import
 * com.mahindra.pms.exception.DataNotFoundException; import
 * com.mahindra.pms.model.SecurityGaurdMaster;
 * 
 * @Service public class UserDetailsServiceImpl implements UserDetailsService {
 * 
 * @Autowired SecurityGaurdMasterRepo sgRepo;
 * 
 * public SecurityGaurdMaster findByUserName(String name) throws
 * DataNotFoundException {
 * 
 * List<SecurityGaurdMaster> list = sgRepo.findByEmpId(name); if (list.size() !=
 * 0) { return list.get(0); } else { throw new
 * DataNotFoundException("Data not found for security gaurd id:" + name); } }
 * 
 * @Override public UserDetails loadUserByUsername(String username) throws
 * UsernameNotFoundException { SecurityGaurdMaster user; try { user =
 * findByUserName(username); if (user == null) { throw new
 * UsernameNotFoundException("Data not found for security gaurd "); } return new
 * org.springframework.security.core.userdetails.User(user.getEmpId(),
 * user.getEmpId(), new ArrayList<>());
 * 
 * } catch (Exception e) { throw new
 * UsernameNotFoundException("Data not found for security gaurd"); } } }
 */