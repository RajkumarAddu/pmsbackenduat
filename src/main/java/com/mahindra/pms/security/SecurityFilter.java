/*
 * package com.mahindra.pms.security;
 * 
 * import java.io.IOException; import java.util.ArrayList; import
 * java.util.Enumeration; import java.util.List; import
 * java.util.concurrent.atomic.AtomicBoolean;
 * 
 * import javax.servlet.FilterChain; import javax.servlet.ServletException;
 * import javax.servlet.http.HttpServletRequest; import
 * javax.servlet.http.HttpServletResponse;
 * 
 * import org.slf4j.Logger; import org.slf4j.LoggerFactory; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.security.access.AccessDeniedException; import
 * org.springframework.security.authentication.
 * UsernamePasswordAuthenticationToken; import
 * org.springframework.security.core.context.SecurityContextHolder; import
 * org.springframework.security.core.userdetails.UserDetails; import
 * org.springframework.security.core.userdetails.UserDetailsService; import
 * org.springframework.security.web.authentication.
 * WebAuthenticationDetailsSource; import
 * org.springframework.stereotype.Component; import
 * org.springframework.web.filter.OncePerRequestFilter;
 * 
 * @Component public class SecurityFilter extends OncePerRequestFilter {
 * 
 * Logger log = LoggerFactory.getLogger(SecurityFilter.class);
 * 
 * @Autowired private JwtUtil jwtUtil;
 * 
 * @Autowired private UserDetailsService userDetailsService;
 * 
 * @Autowired private RestAccessDeniedHandler handler;
 * 
 * @Override public void doFilterInternal(HttpServletRequest request,
 * HttpServletResponse response, FilterChain filterChain) throws
 * ServletException, IOException { log.info("inside doFilterInternal service");
 * log.debug("inside doFilterInternal service");
 * 
 * response.setHeader("Access-Control-Allow-Origin", "*");
 * response.setHeader("Access-Control-Allow-Methods",
 * "GET,POST,DELETE,PUT,OPTIONS");
 * response.setHeader("Access-Control-Allow-Headers", "*");
 * 
 * String requestUrl = request.getRequestURL().toString().toLowerCase(); //
 * System.out.println("req " + requestUrl + " " + //
 * request.getHeader("Authorization")); AtomicBoolean ignoreUrl = new
 * AtomicBoolean(false); request.getHeader("Authorization"); List<String>
 * urlExclusionList = new ArrayList();
 * urlExclusionList.add("/generateAccessToken");
 * urlExclusionList.add("/getAllGates");
 * urlExclusionList.add("/getAllBaseLocation");
 * urlExclusionList.add("/checkIsLocationAvl");
 * 
 * System.out.println(urlExclusionList + "ignore url : " + ignoreUrl);
 * urlExclusionList.forEach((url) -> { if
 * (requestUrl.contains(url.toLowerCase())) {
 * log.info("inside doFilterInternal ignoreUrl block service");
 * ignoreUrl.set(true); System.out.println("waa " + ignoreUrl); } });
 * 
 * if (!ignoreUrl.get()) { //String token = request.getHeader("Authorization");
 * String token=
 * "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyMDM0NDIiLCJpYXQiOjE2MTU1MjE2MDYsImV4cCI6MTYyMTUyMTYwNn0.h-IPls987b4lwqWNEycsq3yxADibRk6XgnTnGie27Y4";
 * Enumeration<String> headerNames = request.getHeaderNames();
 * System.out.println(headerNames.nextElement() + " token " + token); // token=
 * "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyMDM0NDIiLCJpYXQiOjE2MTUxNzcwMzAsImV4cCI6MTYyMTE3NzAzMH0.v6FOZ3PLpmzmEXwD1HLIaPlOUV5cMNyaAcI21zyXF6s";
 * if (token != null) { try {
 * log.info("inside doFilterInternal token verification try block service");
 * String userName = jwtUtil.getUserName(token); if (userName != null &&
 * SecurityContextHolder.getContext().getAuthentication() == null) { UserDetails
 * usr = userDetailsService.loadUserByUsername(userName); boolean isValid =
 * jwtUtil.validateToken(token, usr.getUsername()); if (isValid) {
 * UsernamePasswordAuthenticationToken authToken = new
 * UsernamePasswordAuthenticationToken( userName, usr.getPassword(),
 * usr.getAuthorities()); authToken.setDetails(new
 * WebAuthenticationDetailsSource().buildDetails(request));
 * SecurityContextHolder.getContext().setAuthentication(authToken); } } } catch
 * (Exception e) {
 * log.info("inside doFilterInternal token verification catch block service");
 * AccessDeniedException e1 = new
 * AccessDeniedException("Token signature is not valid ");
 * handler.handle(request, response, e1); } } else {
 * log.info("inside doFilterInternal token verification else block service"); //
 * throw new DataNotFoundException("please generate token");
 * AccessDeniedException e = new
 * AccessDeniedException("Access denied ! please generate token");
 * handler.handle(request, response, e); } } filterChain.doFilter(request,
 * response); }
 * 
 * }
 */