/*
 * package com.mahindra.pms.serviceImpl;
 * 
 * import static org.junit.jupiter.api.Assertions.assertEquals;
 * 
 * import java.time.LocalDate; import java.util.ArrayList; import
 * java.util.List;
 * 
 * import org.junit.jupiter.api.BeforeEach; import org.junit.jupiter.api.Test;
 * import org.mockito.InjectMocks; import org.mockito.Mock; import
 * org.mockito.Mockito; import
 * org.springframework.boot.test.context.SpringBootTest;
 * 
 * import com.mahindra.pms.dao.VehicleInOutRepo; import
 * com.mahindra.pms.dao.VisitorVehicleDetailsRepo; import
 * com.mahindra.pms.model.BaseLocation; import
 * com.mahindra.pms.model.BuildingMaster; import
 * com.mahindra.pms.model.EmployeeVehicleMaster; import
 * com.mahindra.pms.model.NoOfWheelsMaster; import
 * com.mahindra.pms.model.ParkingStationMaster; import
 * com.mahindra.pms.model.VehicleInOut; import
 * com.mahindra.pms.model.VisitorVehicleDetails; import
 * com.mahindra.pms.model.dto.Report1DTO; import
 * com.mahindra.pms.model.dto.Report2DTO; import
 * com.mahindra.pms.model.dto.Report2ResponseDTO;
 * 
 * @SpringBootTest public class ReportServiceImplTest {
 * 
 * @Mock VehicleInOutRepo vehicleInOutRepo;
 * 
 * @Mock VisitorVehicleDetailsRepo visitorVehicleDetailsRepo;
 * 
 * @InjectMocks ReportServiceImpl reportService;
 * 
 * Report1DTO report1DTO; Report2DTO reportDTO2;
 * 
 * Report2ResponseDTO report2Res;
 * 
 * VehicleInOut vehicleInOut; VisitorVehicleDetails visitorVehicleDetails;
 * BaseLocation baseLocation; BuildingMaster buildingMaster;
 * ParkingStationMaster parkingStationMaster; NoOfWheelsMaster noOfWheels;
 * EmployeeVehicleMaster emp1;
 * 
 * List<VehicleInOut> vehicleList = new ArrayList<>();
 * List<VisitorVehicleDetails> visitorList = new ArrayList<>();
 * 
 * @BeforeEach public void init() {
 * 
 * baseLocation = new BaseLocation(); baseLocation.setId(1);
 * baseLocation.setLocationName("Chakan");
 * 
 * buildingMaster = new BuildingMaster();
 * buildingMaster.setBaseLocation(baseLocation);
 * buildingMaster.setBuildingName("MT"); buildingMaster.setId(1);
 * 
 * noOfWheels = new NoOfWheelsMaster(); noOfWheels.setId(2);
 * noOfWheels.setNoOfWheels(4);
 * 
 * report1DTO = new Report1DTO(); report1DTO.setEmpId("203442");
 * report1DTO.setEndDate(LocalDate.now());
 * report1DTO.setStartDate(LocalDate.now());
 * 
 * parkingStationMaster = new ParkingStationMaster();
 * parkingStationMaster.setId(1); parkingStationMaster.setPsName("Basement");
 * parkingStationMaster.setBaseLocationId(baseLocation);
 * parkingStationMaster.setBuildingMaster(buildingMaster);
 * parkingStationMaster.setAvailablilityForReserved(50);
 * parkingStationMaster.setAvailablilityForUnReserved(100);
 * parkingStationMaster.setIsTwoWheelerPs(false);
 * 
 * reportDTO2 = new Report2DTO(); reportDTO2.setEndDate(LocalDate.now());
 * reportDTO2.setStartDate(LocalDate.now());
 * reportDTO2.setParkingStationMaster(parkingStationMaster);
 * reportDTO2.setNoOfWheelsMaster(noOfWheels);
 * reportDTO2.setPresentInPremises(false);
 * 
 * emp1 = new EmployeeVehicleMaster(); emp1.setId(1); emp1.setEmpId("203442");
 * emp1.setEmail("manoj.birla@gmail.com"); emp1.setVehicleRegNo("MP09QA2333");
 * emp1.setBaseLocation(baseLocation); emp1.setBuildingMaster(buildingMaster);
 * //emp1.setVehicleType(vehicleList); emp1.setNoOfWheels(noOfWheels);
 * emp1.setEmpName("Lalit Birla"); emp1.setMake("Maruti");
 * emp1.setModel("swift Dzire");
 * 
 * vehicleInOut = new VehicleInOut();
 * vehicleInOut.setBaseLocationId(baseLocation);
 * vehicleInOut.setBuildingMaster(buildingMaster);
 * vehicleInOut.setEmpId("202442");
 * vehicleInOut.setParkingStaionId(parkingStationMaster);
 * vehicleInOut.setVehicleId(emp1); vehicleInOut.setCreated(LocalDate.now());
 * vehicleList.add(vehicleInOut);
 * 
 * 
 * visitorVehicleDetails = new VisitorVehicleDetails();
 * visitorVehicleDetails.setBaseLocation(baseLocation);
 * visitorVehicleDetails.setBuildingMaster(buildingMaster);
 * visitorVehicleDetails.setCreated(LocalDate.now());
 * visitorVehicleDetails.setReservedParkingStation(parkingStationMaster);
 * 
 * visitorList.add(visitorVehicleDetails);
 * 
 * report2Res = new Report2ResponseDTO(); report2Res.setCount(2);
 * report2Res.setVehicleInOut(vehicleList);
 * report2Res.setVisitorVehicleDetails(visitorList); }
 * 
 * @Test public void getDateWiseVehicleInOutDataTest() throws Exception {
 * Mockito.when(vehicleInOutRepo.findByEmpIdAndStartDateAndEndDate(report1DTO.
 * getEmpId(), report1DTO.getStartDate(),
 * report1DTO.getEndDate())).thenReturn(vehicleList);
 * 
 * List<VehicleInOut> report1 =
 * reportService.getDateWiseVehicleInOutData(report1DTO);
 * assertEquals(vehicleList.get(0), report1.get(0)); }
 * 
 * 
 * test case for parking station id is equal not zero
 * 
 * @Test public void getEmpAndVisitorDataTestIFPsNotZero() throws Exception {
 * Mockito.when(vehicleInOutRepo.findAllByStartDateAndEndDate(
 * parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(vehicleList);
 * Mockito.when(visitorVehicleDetailsRepo.findAllByStartDateAndEndDate(
 * noOfWheels, parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(visitorList);
 * Mockito.when(visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(reportDTO2
 * .getStartDate(), reportDTO2.getEndDate())).thenReturn(1);
 * Mockito.when(vehicleInOutRepo.CountOfAllVehicleInPremises(reportDTO2.
 * getStartDate(), reportDTO2.getEndDate())) .thenReturn(1);
 * 
 * Report2ResponseDTO res = reportService.getEmpAndVisitorData(reportDTO2);
 * assertEquals(report2Res.getCount(), res.getCount()); }
 * 
 * 
 * test case for parking station id is equal zero
 * 
 * @Test public void getEmpAndVisitorDataTestIFPsZero() throws Exception {
 * parkingStationMaster.setId(0);
 * Mockito.when(vehicleInOutRepo.findAllByStartDateAndEndDate(
 * parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(vehicleList);
 * Mockito.when(visitorVehicleDetailsRepo.findAllByStartDateAndEndDate(
 * noOfWheels, parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(visitorList);
 * Mockito.when(visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(reportDTO2
 * .getStartDate(), reportDTO2.getEndDate())).thenReturn(1);
 * Mockito.when(vehicleInOutRepo.CountOfAllVehicleInPremises(reportDTO2.
 * getStartDate(), reportDTO2.getEndDate())) .thenReturn(1);
 * 
 * Report2ResponseDTO res = reportService.getEmpAndVisitorData(reportDTO2);
 * assertEquals(report2Res.getCount(), res.getCount()); }
 * 
 * 
 * 
 * 
 * @Test public void getEmpAndVisitorDataTestForElseBlock() throws Exception {
 * reportDTO2.setPresentInPremises(true);
 * Mockito.when(vehicleInOutRepo.findAllByStartDateAndEndDate(
 * parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(vehicleList);
 * Mockito.when(visitorVehicleDetailsRepo.findAllByStartDateAndEndDate(
 * noOfWheels, parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(visitorList);
 * Mockito.when(visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(reportDTO2
 * .getStartDate(), reportDTO2.getEndDate())).thenReturn(1);
 * Mockito.when(vehicleInOutRepo.CountOfAllVehicleInPremises(reportDTO2.
 * getStartDate(), reportDTO2.getEndDate())) .thenReturn(1); Report2ResponseDTO
 * res = reportService.getEmpAndVisitorData(reportDTO2);
 * assertEquals(report2Res.getCount(), res.getCount()); }
 * 
 * 
 * 
 * 
 * @Test public void getEmpAndVisitorDataTestForElseBlockForPSZero() throws
 * Exception { parkingStationMaster.setId(0);
 * reportDTO2.setPresentInPremises(true);
 * Mockito.when(vehicleInOutRepo.findAllByStartDateAndEndDate(
 * parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(vehicleList);
 * Mockito.when(visitorVehicleDetailsRepo.findAllByStartDateAndEndDate(
 * noOfWheels, parkingStationMaster, reportDTO2.getStartDate(),
 * reportDTO2.getEndDate())).thenReturn(visitorList);
 * Mockito.when(visitorVehicleDetailsRepo.CountOfAllVisitorInPremises(reportDTO2
 * .getStartDate(), reportDTO2.getEndDate())).thenReturn(1);
 * Mockito.when(vehicleInOutRepo.CountOfAllVehicleInPremises(reportDTO2.
 * getStartDate(), reportDTO2.getEndDate())) .thenReturn(1); Report2ResponseDTO
 * res = reportService.getEmpAndVisitorData(reportDTO2);
 * assertEquals(report2Res.getCount(), res.getCount()); } }
 */