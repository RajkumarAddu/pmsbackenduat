/*
 * package com.mahindra.pms.serviceImpl;
 * 
 * import static org.junit.jupiter.api.Assertions.assertEquals; import static
 * org.mockito.Mockito.doNothing;
 * 
 * import java.util.ArrayList; import java.util.List; import java.util.Optional;
 * 
 * import org.junit.jupiter.api.BeforeEach; import org.junit.jupiter.api.Test;
 * import org.mockito.ArgumentMatchers; import org.mockito.InjectMocks; import
 * org.mockito.Mock; import org.mockito.Mockito; import
 * org.springframework.boot.test.context.SpringBootTest;
 * 
 * import com.mahindra.pms.dao.ParkingStationMasterRepo; import
 * com.mahindra.pms.dao.VisitorVehicleDetailsRepo; import
 * com.mahindra.pms.model.BaseLocation; import
 * com.mahindra.pms.model.BuildingMaster; import
 * com.mahindra.pms.model.NoOfWheelsMaster; import
 * com.mahindra.pms.model.ParkingStationMaster; import
 * com.mahindra.pms.model.VisitorVehicleDetails; import
 * com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO; import
 * com.mahindra.pms.model.dto.VisitorVehicleEntryDTO; import
 * com.mahindra.pms.model.dto.VisitorVehicleExitDTO;
 * 
 * @SpringBootTest public class VisitorVehicleDetailsServiceImplTest {
 * 
 * @Mock VisitorVehicleDetailsRepo visitorVehicleDetailsRepo;
 * 
 * @Mock ParkingStationMasterRepo parkingStationMasterRepo;
 * 
 * @InjectMocks VisitorVehicleDetailsServiceImpl visitorService;
 * 
 * VisitorVehicleDetails visitor; List<VisitorVehicleDetails> visitorList = new
 * ArrayList<>(); BaseLocation baseLocation; BuildingMaster buildingMaster;
 * ParkingStationMaster parkingStationMaster; NoOfWheelsMaster noOfWheels;
 * 
 * BaseLocationFetchResponseDTO baseLocationFetchDTO; VisitorVehicleEntryDTO
 * visitorEntryDTO; VisitorVehicleExitDTO visitorVehicleExitDTO;
 * 
 * @BeforeEach public void init() { baseLocation = new BaseLocation();
 * baseLocation.setId(1); baseLocation.setLocationName("Chakan");
 * 
 * buildingMaster = new BuildingMaster();
 * buildingMaster.setBaseLocation(baseLocation);
 * buildingMaster.setBuildingName("MT"); buildingMaster.setId(1);
 * 
 * noOfWheels = new NoOfWheelsMaster(); noOfWheels.setId(2);
 * noOfWheels.setNoOfWheels(4);
 * 
 * parkingStationMaster = new ParkingStationMaster();
 * parkingStationMaster.setId(1); parkingStationMaster.setPsName("Basement");
 * parkingStationMaster.setBaseLocationId(baseLocation);
 * parkingStationMaster.setBuildingMaster(buildingMaster);
 * parkingStationMaster.setAvailablilityForReserved(50);
 * parkingStationMaster.setAvailablilityForUnReserved(100);
 * parkingStationMaster.setIsTwoWheelerPs(false);
 * 
 * visitor = new VisitorVehicleDetails(); visitor.setId(1);
 * visitor.setVisitorName("Lalit"); visitor.setVisitPurpose("For interview");
 * visitor.setBaseLocation(baseLocation);
 * visitor.setBuildingMaster(buildingMaster);
 * visitor.setReservedParkingStation(parkingStationMaster);
 * visitor.setDropPoint("MT"); visitor.setNoOfWheels(noOfWheels);
 * visitor.setVehicleRegNo("MP09QA2709");
 * 
 * visitorList.add(visitor);
 * 
 * baseLocationFetchDTO = new BaseLocationFetchResponseDTO();
 * baseLocationFetchDTO.setBaseLocation(baseLocation);
 * baseLocationFetchDTO.setBuildingMaster(buildingMaster);
 * 
 * visitorEntryDTO = new VisitorVehicleEntryDTO();
 * visitorEntryDTO.setBaseLocation(baseLocation);
 * visitorEntryDTO.setBuildingMaster(buildingMaster);
 * visitorEntryDTO.setVisitorName("Lalit");
 * visitorEntryDTO.setNoOfWheels(noOfWheels);
 * visitorEntryDTO.setToBeParked(true); visitorEntryDTO.setMobile(9090909090L);
 * visitorEntryDTO.setVehicleRegNo("MP09QA2710");
 * 
 * visitorVehicleExitDTO = new VisitorVehicleExitDTO();
 * visitorVehicleExitDTO.setUpdatedBy("Arjun Singh");
 * visitorVehicleExitDTO.setVehicleRegNo("MP09QA2709"); }
 * 
 * @Test public void getVisitorsListTest() throws Exception {
 * Mockito.when(visitorVehicleDetailsRepo.findAll()).thenReturn(visitorList);
 * List<VisitorVehicleDetails> visitorsList1 = visitorService.getVisitorsList();
 * assertEquals(visitorList, visitorsList1); }
 * 
 * @Test public void visitorsListIsExitFalseTest() throws Exception {
 * visitor.setExit(false); Mockito.when(visitorVehicleDetailsRepo.
 * findByBaseLocationAndBuildingMasterAndIsExit(baseLocation, buildingMaster,
 * false)).thenReturn(visitorList); List<VisitorVehicleDetails>
 * visitorsListIsExitFalse = visitorService
 * .visitorsListIsExitFalse(baseLocationFetchDTO); assertEquals(visitorList,
 * visitorsListIsExitFalse); }
 * 
 * @Test public void visitorEntryTest() throws Exception { //
 * visitor.setExit(true);
 * Mockito.when(visitorVehicleDetailsRepo.findByVehicleRegNoAndIsExitFalse(
 * visitor.getVehicleRegNo())) .thenReturn(Mockito.any());
 * Mockito.when(visitorVehicleDetailsRepo.save(visitor)).thenReturn(visitor);
 * VisitorVehicleDetails visitorEntry =
 * visitorService.visitorEntry(visitorEntryDTO); assertEquals(visitor,
 * visitorEntry); }
 * 
 * @Test public void visitorExitTest() throws Exception {
 * visitor.setExit(false); Mockito.when(
 * visitorVehicleDetailsRepo.findByVehicleRegNoAndIsExitFalse(visitor.
 * getVehicleRegNo())) .thenReturn(Optional.of(visitor)); visitor.setExit(true);
 * Mockito.when(visitorVehicleDetailsRepo.save(visitor)).thenReturn(visitor);
 * VisitorVehicleDetails visitorExit =
 * visitorService.visitorExit(visitorVehicleExitDTO); assertEquals(visitor,
 * visitorExit); }
 * 
 * }
 */