/*
 * package com.mahindra.pms.serviceImpl;
 * 
 * import static org.junit.jupiter.api.Assertions.assertEquals;
 * 
 * import java.util.ArrayList; import java.util.List; import java.util.Optional;
 * 
 * import org.junit.jupiter.api.BeforeEach; import org.junit.jupiter.api.Test;
 * import org.mockito.InjectMocks; import org.mockito.Mock; import
 * org.mockito.Mockito; import
 * org.springframework.boot.test.context.SpringBootTest;
 * 
 * import com.mahindra.pms.dao.ParkingStationMasterRepo; import
 * com.mahindra.pms.dao.VehicleInOutRepo; import
 * com.mahindra.pms.model.BaseLocation; import
 * com.mahindra.pms.model.BuildingMaster; import
 * com.mahindra.pms.model.EmployeeVehicleMaster; import
 * com.mahindra.pms.model.ParkingStationMaster; import
 * com.mahindra.pms.model.VehicleInOut; import
 * com.mahindra.pms.model.VehicleTypeMaster; import
 * com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO; import
 * com.mahindra.pms.model.dto.ChangeParkingSlotDTO; import
 * com.mahindra.pms.model.dto.ParkingStationMasterResponseDTO;
 * 
 * @SpringBootTest public class ParkingStationMasterServiceImplTest {
 * 
 * @Mock ParkingStationMasterRepo parkingStationMasterRepo;
 * 
 * @Mock VehicleInOutRepo vehicleInOutRepo;
 * 
 * @InjectMocks ParkingStationMasterServiceImpl parkingSMasterService;
 * 
 * ParkingStationMaster parkingStationMaster; ParkingStationMasterResponseDTO
 * parkingStationMasterResponseDTO; BaseLocation baseLocation; BuildingMaster
 * buildingMaster; List<ParkingStationMaster> list = new ArrayList<>();
 * BaseLocationFetchResponseDTO baseLocationFetchResponse;
 * ParkingStationMasterResponseDTO psResponse; ChangeParkingSlotDTO
 * changeSlotDTO; EmployeeVehicleMaster emp1; VehicleInOut vehicleInOut;
 * VehicleTypeMaster vMaster; List<VehicleTypeMaster> vehicleList=new
 * ArrayList<>();
 * 
 * @BeforeEach public void init() { baseLocation = new BaseLocation();
 * baseLocation.setId(1); baseLocation.setLocationName("Chakan");
 * 
 * buildingMaster = new BuildingMaster();
 * buildingMaster.setBaseLocation(baseLocation);
 * buildingMaster.setBuildingName("MT"); buildingMaster.setId(1);
 * 
 * parkingStationMaster = new ParkingStationMaster();
 * parkingStationMaster.setId(1); parkingStationMaster.setPsName("Basement");
 * parkingStationMaster.setBaseLocationId(baseLocation);
 * parkingStationMaster.setBuildingMaster(buildingMaster);
 * parkingStationMaster.setAvailablilityForReserved(50);
 * parkingStationMaster.setAvailablilityForUnReserved(100);
 * parkingStationMaster.setIsTwoWheelerPs(false);
 * 
 * list.add(parkingStationMaster);
 * 
 * baseLocationFetchResponse=new BaseLocationFetchResponseDTO();
 * baseLocationFetchResponse.setBaseLocation(baseLocation);
 * baseLocationFetchResponse.setBuildingMaster(buildingMaster);
 * 
 * psResponse=new ParkingStationMasterResponseDTO();
 * psResponse.setNoReservedParkingSlots(50);
 * psResponse.setNoUnReservedParkingSlots(100);
 * psResponse.setReservedParkingStaitonName("Basement");
 * psResponse.setUnReservedParkingStaitonName("Basement");
 * 
 * vMaster = new VehicleTypeMaster(); vMaster.setId(1);
 * vMaster.setVehicleType("Admin");
 * 
 * vehicleList.add(vMaster);
 * 
 * emp1 = new EmployeeVehicleMaster(); emp1.setId(1); emp1.setEmpId("203442");
 * emp1.setEmail("manoj.birla@gmail.com"); emp1.setVehicleRegNo("MP09QA2333");
 * emp1.setBaseLocation(baseLocation); emp1.setBuildingMaster(buildingMaster);
 * emp1.setVehicleType(vehicleList); emp1.setEmpName("Lalit Birla");
 * emp1.setMake("Maruti"); emp1.setModel("swift Dzire");
 * 
 * changeSlotDTO=new ChangeParkingSlotDTO();
 * changeSlotDTO.setBuildingMaster(buildingMaster);
 * changeSlotDTO.setBaseLocation(baseLocation);
 * changeSlotDTO.setEmployeeVehicleMaster(emp1);
 * changeSlotDTO.setNewParkingStation(parkingStationMaster);
 * changeSlotDTO.setOldParkingStation(parkingStationMaster);
 * 
 * vehicleInOut=new VehicleInOut();
 * vehicleInOut.setBaseLocationId(baseLocation);
 * vehicleInOut.setBuildingMaster(buildingMaster);
 * vehicleInOut.setEmpId("202442");
 * vehicleInOut.setParkingStaionId(parkingStationMaster); }
 * 
 * @Test public void fetchAllParkingStationAndSlotsTest() throws Exception {
 * Mockito.when(parkingStationMasterRepo.findByBaseLocationIdAndBuildingMaster(
 * baseLocation,buildingMaster)) .thenReturn(list); List<ParkingStationMaster>
 * resList = parkingSMasterService.fetchAllParkingStationAndSlots(
 * baseLocationFetchResponse); assertEquals(list, resList); }
 * 
 * @Test public void fetchParkingStationAndSlotsTest() throws Exception{
 * Mockito.when(parkingStationMasterRepo.findByBaseLocationIdAndBuildingMaster(
 * baseLocation,buildingMaster)) .thenReturn(list);
 * ParkingStationMasterResponseDTO res =
 * parkingSMasterService.fetchParkingStationAndSlots(baseLocationFetchResponse);
 * assertEquals(psResponse.getReservedParkingStaitonName(),
 * res.getReservedParkingStaitonName()); }
 * 
 * @Test public void changePsSlotTest() throws Exception{
 * Mockito.when(vehicleInOutRepo.findByEmpIdAndVehicleRegNoAndIsExitFalse(emp1.
 * getEmpId(),emp1.getVehicleRegNo())) .thenReturn(Optional.of(vehicleInOut));
 * Mockito.when(parkingStationMasterRepo.findById(1))
 * .thenReturn(Optional.of(parkingStationMaster)); String changePsSlot =
 * parkingSMasterService.changePsSlot(changeSlotDTO);
 * assertEquals("Parking Slot changed ", changePsSlot);
 * 
 * 
 * 
 * }
 * 
 * }
 */