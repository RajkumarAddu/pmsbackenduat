/*
 * package com.mahindra.pms.serviceImpl;
 * 
 * import static org.junit.jupiter.api.Assertions.assertEquals;
 * 
 * import java.time.LocalDate; import java.util.ArrayList; import
 * java.util.List; import java.util.Optional;
 * 
 * import org.apache.poi.hpsf.Array; import org.junit.jupiter.api.BeforeEach;
 * import org.junit.jupiter.api.Test; import org.mockito.ArgumentMatchers;
 * import org.mockito.InjectMocks; import org.mockito.Mock; import
 * org.mockito.Mockito; import
 * org.springframework.boot.test.context.SpringBootTest; import
 * com.mahindra.pms.dao.BaseLocationRepo; import
 * com.mahindra.pms.dao.EmployeeVehicleMasterRepo; import
 * com.mahindra.pms.dao.ParkingStationMasterRepo; import
 * com.mahindra.pms.dao.VehicleInOutRepo; import
 * com.mahindra.pms.model.BaseLocation; import
 * com.mahindra.pms.model.BuildingMaster; import
 * com.mahindra.pms.model.EmployeeVehicleMaster; import
 * com.mahindra.pms.model.NoOfWheelsMaster; import
 * com.mahindra.pms.model.ParkingStationMaster; import
 * com.mahindra.pms.model.VehicleInOut; import
 * com.mahindra.pms.model.VehicleTypeMaster; import
 * com.mahindra.pms.model.dto.Report1DTO; import
 * com.mahindra.pms.model.dto.VehicleEntryDTO;
 * 
 * @SpringBootTest public class VehicleInOutServiceImplTest {
 * 
 * @Mock EmployeeVehicleMasterRepo employeeVehicleMasterRepo;
 * 
 * @Mock VehicleInOutRepo vehicleInOutRepo;
 * 
 * @Mock ParkingStationMasterRepo parkingStationMasterRepo;
 * 
 * @Mock BaseLocationRepo baseLocationRepo;
 * 
 * @InjectMocks VehicleInOutServiceImpl vehicleInOutService;
 * 
 * List<VehicleTypeMaster> vehicleList = new ArrayList<>(); VehicleInOut
 * vehicleInOut; BaseLocation baseLocation; BuildingMaster buildingMaster;
 * ParkingStationMaster parkingStationMaster; NoOfWheelsMaster noOfWheels;
 * EmployeeVehicleMaster emp1; VehicleEntryDTO vehicleEntryDTO;
 * VehicleTypeMaster vehicleTypeMaster;
 * 
 * @BeforeEach public void init() { vehicleTypeMaster = new VehicleTypeMaster();
 * vehicleTypeMaster.setId(2); vehicleTypeMaster.setVehicleType("Admin");
 * vehicleList.add(vehicleTypeMaster);
 * 
 * baseLocation = new BaseLocation(); baseLocation.setId(1);
 * baseLocation.setLocationName("Chakan");
 * 
 * buildingMaster = new BuildingMaster();
 * buildingMaster.setBaseLocation(baseLocation);
 * buildingMaster.setBuildingName("MT"); buildingMaster.setId(1);
 * 
 * noOfWheels = new NoOfWheelsMaster(); noOfWheels.setId(2);
 * noOfWheels.setNoOfWheels(4);
 * 
 * parkingStationMaster = new ParkingStationMaster();
 * parkingStationMaster.setId(1); parkingStationMaster.setPsName("Basement");
 * parkingStationMaster.setBaseLocationId(baseLocation);
 * parkingStationMaster.setBuildingMaster(buildingMaster);
 * parkingStationMaster.setAvailablilityForReserved(50);
 * parkingStationMaster.setAvailablilityForUnReserved(100);
 * parkingStationMaster.setIsTwoWheelerPs(false);
 * 
 * emp1 = new EmployeeVehicleMaster(); emp1.setId(1); emp1.setEmpId("203442");
 * emp1.setEmail("manoj.birla@gmail.com"); emp1.setVehicleRegNo("MP09QA2333");
 * emp1.setBaseLocation(baseLocation); emp1.setBuildingMaster(buildingMaster);
 * emp1.setVehicleType(vehicleList); emp1.setNoOfWheels(noOfWheels);
 * emp1.setEmpName("Lalit Birla"); emp1.setMake("Maruti");
 * emp1.setModel("swift Dzire"); emp1.setInsuranceValidTo(LocalDate.now());
 * 
 * vehicleInOut = new VehicleInOut(); vehicleInOut.setId(1);
 * vehicleInOut.setBaseLocationId(baseLocation);
 * vehicleInOut.setBuildingMaster(buildingMaster);
 * vehicleInOut.setEmpId("203442");
 * vehicleInOut.setParkingStaionId(parkingStationMaster);
 * vehicleInOut.setVehicleId(emp1); vehicleInOut.setVehicleRegNo("MP09QA2333");
 * vehicleInOut.setCreated(LocalDate.now());
 * 
 * vehicleEntryDTO = new VehicleEntryDTO();
 * vehicleEntryDTO.setBaseLocationId(baseLocation);
 * vehicleEntryDTO.setBuildingMaster(buildingMaster);
 * vehicleEntryDTO.setEmpId("203442");
 * vehicleEntryDTO.setVehicleRegNo("MP09QA2333");
 * vehicleEntryDTO.setCreated(LocalDate.now());
 * 
 * }
 * 
 * @Test public void vehicleInEntryTest() throws Exception {
 * 
 * Mockito.when(employeeVehicleMasterRepo.findByVehicleRegNoAndEmpId(emp1.
 * getVehicleRegNo(), emp1.getEmpId())) .thenReturn(Optional.of(emp1));
 * Mockito.when(vehicleInOutRepo.findByEmpIdAndVehicleRegNoAndIsExitFalse(emp1.
 * getVehicleRegNo(), emp1.getEmpId())) .thenReturn(ArgumentMatchers.isNull());
 * Mockito.when(baseLocationRepo.findById(1)).thenReturn(Optional.of(
 * baseLocation));
 * Mockito.when(vehicleInOutRepo.save(vehicleInOut)).thenReturn(vehicleInOut);
 * VehicleInOut vehicleInEntry =
 * vehicleInOutService.vehicleInEntry(vehicleEntryDTO);
 * System.out.println(vehicleInEntry); assertEquals(vehicleInOut,
 * vehicleInEntry); }
 * 
 * }
 */