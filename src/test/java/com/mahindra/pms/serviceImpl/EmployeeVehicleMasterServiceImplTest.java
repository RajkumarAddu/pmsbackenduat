/*
 * package com.mahindra.pms.serviceImpl;
 * 
 * import static org.junit.jupiter.api.Assertions.assertEquals;
 * 
 * 
 * import java.util.ArrayList; import java.util.List; import java.util.Optional;
 * 
 * import org.junit.jupiter.api.BeforeEach; import org.junit.jupiter.api.Test;
 * import org.mockito.ArgumentMatchers; import org.mockito.InjectMocks; import
 * org.mockito.Mock; import org.mockito.Mockito; import
 * org.springframework.boot.test.context.SpringBootTest; import
 * com.mahindra.pms.dao.BaseLocationRepo; import
 * com.mahindra.pms.dao.EmployeeVehicleMasterRepo; import
 * com.mahindra.pms.dao.FuelTypeRepo; import
 * com.mahindra.pms.dao.VehicleInOutRepo; import
 * com.mahindra.pms.dao.VehicleTypeMasterRepo; import
 * com.mahindra.pms.model.BaseLocation; import
 * com.mahindra.pms.model.BuildingMaster; import
 * com.mahindra.pms.model.EmployeeVehicleMaster; import
 * com.mahindra.pms.model.VehicleInOut; import
 * com.mahindra.pms.model.VehicleTypeMaster; import
 * com.mahindra.pms.model.dto.EmployeeFetchDetailsDTO; import
 * com.mahindra.pms.model.dto.EmployeeVehicleMasterDTO; import
 * com.mahindra.pms.model.dto.ResponseOfEmpVRegistration;
 * 
 * @SpringBootTest public class EmployeeVehicleMasterServiceImplTest {
 * 
 * @Mock EmployeeVehicleMasterRepo employeeVehicleMasterRepo;
 * 
 * @Mock VehicleTypeMasterRepo vehicleTypeMasterRepo;
 * 
 * @Mock VehicleInOutRepo vehicleInOutRepo;
 * 
 * @Mock FuelTypeRepo fuelTypeRepo;
 * 
 * @Mock BaseLocationRepo baseLocationRepo;
 * 
 * CommonMail commonMail = new CommonMail();
 * 
 * @InjectMocks EmployeeVehicleMasterServiceImpl employeeVehicleMasterService;
 * 
 * 
 * List<VehicleTypeMaster> vehicleList; EmployeeVehicleMaster emp1,emp2;
 * EmployeeVehicleMasterDTO empDTO1; BaseLocation baseLocation; BuildingMaster
 * buildingMaster; VehicleTypeMaster vMaster; EmployeeFetchDetailsDTO
 * employeeFethcDetailsDTO; VehicleInOut vehicleInOut;
 * 
 * ResponseOfEmpVRegistration resOfEmpReg;
 * 
 * String qrCodeImageString =
 * "iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6AQAAAACgl2eQAAAAzElEQVR4Xu3Q0Q3EMAgDUDZPRutmOWyT9FTpOsAZt0oCvC9iveeKZ+eRBkoDpYHSQPECM5ix5pjjrvwAC0z13T03kMvBgNNT2YLzmQM8Xhf196BYfXfPDASzQVV+4ETb+YoX4Gq4oJHv2pchWNnIWV58kaF2A6cHwcnk2A2svR5cumHtgBowOBMEp3ZgR5CvkPECWSJYlQZSfoA1zgkwQUXcAAY4MeWTfVvAewk5g8CfJX5LUIzQGATDLS2tqoQX+J0GSgOlgdJAaaBcH6b5y7Tm0OipAAAAAElFTkSuQmCC";
 * 
 * @BeforeEach public void init() { vehicleList = new ArrayList<>();
 * baseLocation = new BaseLocation(); baseLocation.setId(1);
 * baseLocation.setLocationName("Chakan");
 * 
 * buildingMaster = new BuildingMaster();
 * buildingMaster.setBaseLocation(baseLocation);
 * buildingMaster.setBuildingName("MT"); buildingMaster.setId(1);
 * 
 * vMaster = new VehicleTypeMaster(); vMaster.setId(1);
 * vMaster.setVehicleType("Admin");
 * 
 * vehicleList.add(vMaster);
 * 
 * emp1 = new EmployeeVehicleMaster(); emp1.setId(1); emp1.setEmpId("203442");
 * emp1.setEmail("manoj.birla@gmail.com"); emp1.setVehicleRegNo("MP09QA2333");
 * emp1.setBaseLocation(baseLocation); emp1.setBuildingMaster(buildingMaster);
 * emp1.setVehicleType(vehicleList); emp1.setEmpName("Lalit Birla");
 * emp1.setMake("Maruti"); emp1.setModel("swift Dzire");
 * 
 * emp2 = new EmployeeVehicleMaster(); emp2.setId(1); emp2.setEmpId("203442");
 * emp2.setEmail("manoj.birla@gmail.com"); emp2.setVehicleRegNo("MP09QA2333");
 * emp2.setBaseLocation(baseLocation); emp2.setBuildingMaster(buildingMaster);
 * emp2.setVehicleType(vehicleList); emp2.setEmpName("Lalit Birla");
 * emp2.setMake("Maruti"); emp2.setModel("swift Dzire");
 * 
 * empDTO1 = new EmployeeVehicleMasterDTO(); empDTO1.setId(1);
 * empDTO1.setEmpId("203442"); empDTO1.setEmail("manoj.birla@gmail.com");
 * empDTO1.setVehicleRegNo("MP09QA2333"); empDTO1.setBaseLocation(baseLocation);
 * empDTO1.setBuildingMaster(buildingMaster);
 * empDTO1.setVehicleType(vehicleList); empDTO1.setEmpName("Lalit Birla");
 * empDTO1.setMake("Maruti"); empDTO1.setModel("swift Dzire");
 * 
 * resOfEmpReg = new ResponseOfEmpVRegistration();
 * 
 * resOfEmpReg.setQrCodeImageString(qrCodeImageString);
 * 
 * //resOfEmpReg.setQrCodePdfString(qrCodePdfString);
 * 
 * employeeFethcDetailsDTO=new EmployeeFetchDetailsDTO();
 * employeeFethcDetailsDTO.setEmpId("203442");
 * employeeFethcDetailsDTO.setBaseLocation(baseLocation);
 * employeeFethcDetailsDTO.setVehicleRegNo("MP09QA2333");
 * 
 * vehicleInOut=new VehicleInOut(); vehicleInOut.setEmpId("203442");
 * vehicleInOut.setVehicleRegNo("MP09QA2333");
 * 
 * }
 * 
 * @Test public void saveEmpVMTest() throws Exception { //File file=new
 * File("");
 * Mockito.when(employeeVehicleMasterRepo.findByVehicleRegNo("MP09QA2333"))
 * .thenReturn(ArgumentMatchers.isNull());
 * Mockito.when(vehicleTypeMasterRepo.findById(vMaster.getId()))
 * .thenReturn(Optional.of(vMaster));
 * Mockito.when(employeeVehicleMasterRepo.save(emp1)).thenReturn(emp1);
 * Mockito.when(baseLocationRepo.findById(baseLocation.getId()))
 * .thenReturn(Optional.of(baseLocation)); ResponseOfEmpVRegistration
 * generateQRCodeImage =
 * employeeVehicleMasterService.generateQRCodeImage(empDTO1);
 * //ResponseOfEmpVRegistration res =
 * employeeVehicleMasterService.saveEmpVM(empDTO1);
 * System.out.println(generateQRCodeImage.getQrCodeImageString().equals(
 * resOfEmpReg.getQrCodeImageString())); System.out.println("res :-" +
 * generateQRCodeImage.getQrCodeImageString());
 * assertEquals(resOfEmpReg.getQrCodeImageString(),generateQRCodeImage.
 * getQrCodeImageString()); }
 * 
 * 
 * @Test public void getEmpVMByIdTest() throws Exception {
 * Mockito.when(employeeVehicleMasterRepo.findByVehicleRegNoAndEmpId(emp1.
 * getVehicleRegNo(),emp1.getEmpId())) .thenReturn(Optional.of(emp1));
 * Mockito.when(baseLocationRepo.findById(baseLocation.getId()))
 * .thenReturn(Optional.of(baseLocation)); EmployeeVehicleMaster empVMById =
 * employeeVehicleMasterService.getEmpVMById(employeeFethcDetailsDTO);
 * assertEquals(emp1, empVMById); }
 * 
 * @Test public void updateEmpVMTest() throws Exception {
 * Mockito.when(employeeVehicleMasterRepo.findByVehicleRegNoAndEmpId(emp1.
 * getVehicleRegNo(),emp1.getEmpId())) .thenReturn(Optional.of(emp1));
 * Mockito.when(employeeVehicleMasterRepo.save(emp1)).thenReturn(emp1);
 * 
 * EmployeeVehicleMaster updateEmpVM =
 * employeeVehicleMasterService.updateEmpVM(empDTO1);
 * System.out.println("Employee "+updateEmpVM); //
 * assertEquals(emp1,updateEmpVM);
 * 
 * }
 * 
 * 
 * @Test public void deleteEmpVMTest(){
 * Mockito.when(employeeVehicleMasterRepo.findByVehicleRegNoAndEmpId(emp1.
 * getVehicleRegNo(),emp1.getEmpId())) .thenReturn(Optional.of(emp1));
 * 
 * Mockito.when(vehicleInOutRepo.findByEmpIdAndVehicleRegNoAndIsExitFalse(emp1.
 * getEmpId(),emp1.getVehicleRegNo())) .thenReturn(Optional.of(vehicleInOut));
 * //Mockito.doNothing().when(employeeVehicleMasterRepo.softDelete(emp1.
 * getVehicleRegNo(),emp1.getEmpId())).methodToMock();
 * 
 * } }
 */