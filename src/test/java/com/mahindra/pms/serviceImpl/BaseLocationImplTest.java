/*
 * package com.mahindra.pms.serviceImpl;
 * 
 * 
 * import static org.junit.jupiter.api.Assertions.assertEquals;
 * 
 * import java.util.ArrayList; import java.util.List;
 * 
 * import org.junit.jupiter.api.BeforeEach; import org.junit.jupiter.api.Test;
 * import org.mockito.InjectMocks; import org.mockito.Mock; import
 * org.mockito.Mockito; import
 * org.springframework.boot.test.context.SpringBootTest;
 * 
 * import com.mahindra.pms.dao.BaseLocationRepo; import
 * com.mahindra.pms.dao.BuildingMasterRepo; import
 * com.mahindra.pms.model.BaseLocation; import
 * com.mahindra.pms.model.BuildingMaster; import
 * com.mahindra.pms.model.dto.ResponseBaseLocationAndBuildingMaster;
 * 
 * @SpringBootTest public class BaseLocationImplTest {
 * 
 * @Mock BaseLocationRepo baseLocationRepo;
 * 
 * @Mock BuildingMasterRepo buildingMasterRepo;
 * 
 * @InjectMocks BaseLocationImpl baseLocationService;
 * 
 * List<ResponseBaseLocationAndBuildingMaster> resList; List<BaseLocation> list;
 * List<BuildingMaster> buildingList; BaseLocation baseLocation; BuildingMaster
 * buildingMaster; Iterable<BaseLocation> locationsData;
 * ResponseBaseLocationAndBuildingMaster resB;
 * 
 * @BeforeEach public void init() { list = new ArrayList(); buildingList = new
 * ArrayList(); resList = new ArrayList();
 * 
 * baseLocation = new BaseLocation(); baseLocation.setId(1);
 * baseLocation.setLocationName("Chakan");
 * 
 * buildingMaster = new BuildingMaster();
 * buildingMaster.setBaseLocation(baseLocation);
 * buildingMaster.setBuildingName("MT"); buildingMaster.setId(1);
 * 
 * list.add(baseLocation); locationsData = list;
 * 
 * buildingList.add(buildingMaster);
 * 
 * resB = new ResponseBaseLocationAndBuildingMaster();
 * resB.setBaseLocation(baseLocation); resB.setBuildingMaster(buildingList);
 * 
 * resList.add(resB);
 * 
 * }
 * 
 * @Test void getAllBaseLocationTest() throws Exception {
 * Mockito.when(baseLocationRepo.findAll()).thenReturn(locationsData);
 * List<BaseLocation> allBaseLocation =
 * baseLocationService.getAllBaseLocation(); assertEquals(list,
 * allBaseLocation); }
 * 
 * @Test void getAllBuildingsAndBaseLocationTest() throws Exception {
 * Mockito.when(baseLocationRepo.findAll()).thenReturn(locationsData);
 * Mockito.when(buildingMasterRepo.findAll()).thenReturn(buildingList);
 * 
 * List<ResponseBaseLocationAndBuildingMaster> res =
 * baseLocationService.getAllBuildingsAndBaseLocations();
 * assertEquals(resList.get(0).getBaseLocation(), res.get(0).getBaseLocation());
 * }
 * 
 * }
 */