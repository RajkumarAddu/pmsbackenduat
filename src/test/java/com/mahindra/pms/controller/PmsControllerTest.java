/*
 * package com.mahindra.pms.controller;
 * 
 * import static
 * org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
 * import static
 * org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
 * import static
 * org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
 * import static
 * org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
 * 
 * import java.time.LocalDate; import java.util.ArrayList; import
 * java.util.List;
 * 
 * import javax.servlet.Filter;
 * 
 * import org.junit.Before; import org.junit.jupiter.api.BeforeEach; import
 * org.junit.jupiter.api.DisplayName; import org.junit.jupiter.api.Test; import
 * org.mockito.Mockito; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
 * import org.springframework.boot.test.context.SpringBootTest; import
 * org.springframework.boot.test.mock.mockito.MockBean; import
 * org.springframework.http.MediaType; import
 * org.springframework.test.web.servlet.MockMvc; import
 * org.springframework.test.web.servlet.setup.MockMvcBuilders; import
 * org.springframework.web.context.WebApplicationContext;
 * 
 * import com.mahindra.pms.model.BaseLocation; import
 * com.mahindra.pms.model.BuildingMaster; import
 * com.mahindra.pms.model.EmployeeVehicleMaster; import
 * com.mahindra.pms.model.GateMaster; import
 * com.mahindra.pms.model.VehicleInOut; import
 * com.mahindra.pms.model.VehicleTypeMaster; import
 * com.mahindra.pms.model.dto.BaseLocationFetchResponseDTO; import
 * com.mahindra.pms.model.dto.EmployeeFetchDetailsDTO; import
 * com.mahindra.pms.model.dto.EmployeeVehicleMasterDTO; import
 * com.mahindra.pms.model.dto.ResponseDTO; import
 * com.mahindra.pms.model.dto.ResponseOfEmpVRegistration; import
 * com.mahindra.pms.security.JwtUtil; import
 * com.mahindra.pms.service.BaseLocationService; import
 * com.mahindra.pms.service.EmployeeVehicleMasterService; import
 * com.mahindra.pms.service.GateMasterService; import
 * com.mahindra.pms.service.NoOfWheelsService; import
 * com.mahindra.pms.service.NotifyService; import
 * com.mahindra.pms.service.ParkingStationMasterService; import
 * com.mahindra.pms.service.ReportService; import
 * com.mahindra.pms.service.SecurityGaurdMasterService; import
 * com.mahindra.pms.service.VehicleInOutService; import
 * com.mahindra.pms.service.VisitorVehicleDetailsService; import
 * com.mahindra.pms.validation.EmployeeVehicleMasterValidation;
 * 
 * @SpringBootTest(classes = PmsController.class)
 * 
 * @AutoConfigureMockMvc public class PmsControllerTest {
 * 
 * @Autowired private MockMvc mockMvc;
 * 
 * @Autowired private WebApplicationContext context;
 * 
 * @MockBean EmployeeVehicleMasterService employeeVehicleMasterServiceImpl;
 * 
 * @MockBean ParkingStationMasterService parkingStationMasterService;
 * 
 * @MockBean GateMasterService gateMasteService;
 * 
 * @MockBean VehicleInOutService vehicleInOutService;
 * 
 * @MockBean VisitorVehicleDetailsService visitorVehicleDetailsService;
 * 
 * @MockBean BaseLocationService baseLocationService;
 * 
 * @MockBean NotifyService notifyService;
 * 
 * @MockBean NoOfWheelsService noOfWheelsService;
 * 
 * @MockBean SecurityGaurdMasterService securityGaurdMasterService;
 * 
 * @MockBean JwtUtil jwtUtil;
 * 
 * @MockBean ReportService reportService;
 * 
 * @Autowired private Filter springSecurityFilterChain;
 * 
 * EmployeeVehicleMaster emp1, emp2; ResponseDTO responseDTO;
 * ResponseOfEmpVRegistration responseOfEmpVRegistration;
 * BaseLocationFetchResponseDTO baseLocationFetchResDTO;
 * 
 * List<GateMaster> listOfGates; GateMaster gateMaster1;
 * 
 * List<VehicleTypeMaster> vehicleList; EmployeeVehicleMasterDTO empDTO1;
 * BaseLocation baseLocation; BuildingMaster buildingMaster; VehicleTypeMaster
 * vMaster; EmployeeFetchDetailsDTO employeeFethcDetailsDTO; VehicleInOut
 * vehicleInOut;
 * 
 * @BeforeEach public void init() { vehicleList = new ArrayList<>();
 * baseLocation = new BaseLocation(); baseLocation.setId(1);
 * baseLocation.setLocationName("Chakan");
 * 
 * buildingMaster = new BuildingMaster();
 * buildingMaster.setBaseLocation(baseLocation);
 * buildingMaster.setBuildingName("MT"); buildingMaster.setId(1);
 * 
 * vMaster = new VehicleTypeMaster(); vMaster.setId(1);
 * vMaster.setVehicleType("Admin");
 * 
 * vehicleList.add(vMaster);
 * 
 * emp1 = new EmployeeVehicleMaster(); emp1.setId(1); emp1.setEmpId("203442");
 * emp1.setEmail("manoj.birla@gmail.com"); emp1.setVehicleRegNo("MP09QA2333");
 * emp1.setBaseLocation(baseLocation); emp1.setBuildingMaster(buildingMaster);
 * emp1.setVehicleType(vehicleList); emp1.setEmpName("Lalit Birla");
 * emp1.setMake("Maruti"); emp1.setModel("swift Dzire");
 * 
 * emp2 = new EmployeeVehicleMaster(); emp2.setId(1); emp2.setEmpId("203442");
 * emp2.setEmail("manoj.birla@gmail.com"); emp2.setVehicleRegNo("MP09QA2333");
 * emp2.setBaseLocation(baseLocation); emp2.setBuildingMaster(buildingMaster);
 * emp2.setVehicleType(vehicleList); emp2.setEmpName("Lalit Birla");
 * emp2.setMake("Maruti"); emp2.setModel("swift Dzire");
 * 
 * empDTO1 = new EmployeeVehicleMasterDTO(); empDTO1.setId(1);
 * empDTO1.setEmpId("203442"); empDTO1.setEmail("manoj.birla@gmail.com");
 * empDTO1.setVehicleRegNo("MP09QA2333"); empDTO1.setBaseLocation(baseLocation);
 * empDTO1.setBuildingMaster(buildingMaster);
 * empDTO1.setVehicleType(vehicleList); empDTO1.setEmpName("Lalit Birla");
 * empDTO1.setMake("Maruti"); empDTO1.setModel("swift Dzire");
 * 
 * // resOfEmpReg.setQrCodePdfString(qrCodePdfString);
 * 
 * employeeFethcDetailsDTO = new EmployeeFetchDetailsDTO();
 * employeeFethcDetailsDTO.setEmpId("203442");
 * employeeFethcDetailsDTO.setBaseLocation(baseLocation);
 * employeeFethcDetailsDTO.setVehicleRegNo("MP09QA2333");
 * 
 * vehicleInOut = new VehicleInOut(); vehicleInOut.setEmpId("203442");
 * vehicleInOut.setVehicleRegNo("MP09QA2333");
 * 
 * responseDTO = new ResponseDTO(); responseDTO.setData(Object.class);
 * responseDTO.setStatus(200); responseDTO.setSuccess(true);
 * responseDTO.setMessage("registered successfully");
 * 
 * responseOfEmpVRegistration = new ResponseOfEmpVRegistration();
 * responseOfEmpVRegistration.setQrCodeImageString("");
 * responseOfEmpVRegistration.setQrCodePdfString("");
 * 
 * empDTO1 = new EmployeeVehicleMasterDTO(); empDTO1.setId(1);
 * empDTO1.setCreated(LocalDate.now());
 * 
 * listOfGates = new ArrayList<GateMaster>(); gateMaster1 = new GateMaster();
 * gateMaster1.setGateName("MT"); gateMaster1.setBaseLocationId(new
 * BaseLocation()); gateMaster1.setId(1); listOfGates.add(gateMaster1);
 * 
 * baseLocationFetchResDTO = new BaseLocationFetchResponseDTO();
 * baseLocationFetchResDTO.setBaseLocation(baseLocation);
 * baseLocationFetchResDTO.setBuildingMaster(buildingMaster); }
 * 
 * @Before public void setup() { mockMvc =
 * MockMvcBuilders.webAppContextSetup(context).build(); }
 * 
 * @Test
 * 
 * @DisplayName("Positive Test registerEmpVeDetailsTest") public void
 * registerEmpVeDetailsTest() throws Exception {
 * Mockito.when(employeeVehicleMasterServiceImpl.saveEmpVM(Mockito.any(
 * EmployeeVehicleMasterDTO.class))) .thenReturn(responseOfEmpVRegistration);
 * mockMvc.perform(post("/pms/register/EmpVehicleMaster").accept(MediaType.
 * APPLICATION_JSON).header( "Authorization",
 * "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyMDM0NDIiLCJpYXQiOjE2MTUyMDA2MTksImV4cCI6MTYyMTIwMDYxOX0.WSRVaffN-LboI_1IcuPfvbQqMor2Etd8Tac_dhhkcmk"
 * )) .andDo(print()).andExpect(status().isOk()); }
 * 
 * @Test
 * 
 * @DisplayName("Positive Test getAllGates") public void getAllGatesTest()
 * throws Exception {
 * Mockito.when(gateMasteService.getAllGates()).thenReturn(listOfGates);
 * mockMvc.perform(get("/pms/getAllGates")).andDo(print()).andExpect(status().
 * isOk()); }
 * 
 * @Test public void getGatesByLocationIdTest() throws Exception {
 * Mockito.when(gateMasteService.getGatesByLocationId(baseLocationFetchResDTO)).
 * thenReturn(listOfGates);
 * mockMvc.perform(post("/pms/getAllGates")).andDo(print()).andExpect(status().
 * isOk()); }
 * 
 * @Test public void getEmpVeDetailsByIdTest()throws Exception {
 * Mockito.when(employeeVehicleMasterServiceImpl.getEmpVMById(
 * employeeFethcDetailsDTO)) .thenReturn(emp1);
 * mockMvc.perform(post("/pms/getEmpVehicleDetails")).andDo(print()).andExpect(
 * status().isOk()); } }
 */